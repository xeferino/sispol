unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,
  DB, DBTables,
  ADODB,
  Grids, DBGrids;

type
  TBiometrico = class(TForm)
    Button1: TButton;
    Panel1: TPanel;
    ComboBox1: TComboBox;
    Button2: TButton;
    Button3: TButton;
    Image1: TImage;
    ADOConnection1: TADOConnection;
    ADOQuery1: TADOQuery;
    ADOQuery2: TADOQuery;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
   
  private
    { Private declarations }
     Procedure ParaRefresh;
     Procedure EnabledCapturar;
  public
    { Public declarations }
  end;

var
  //variables de llamado de formulario y funciones del biometrico
  Biometrico: TBiometrico;
  AvzOpen:Integer;
  AvzProces:Integer;
  Hwnd:THandle;
  AvzFind:SmallInt;
  AppPath:String;
  tmpS:String;
  AvzFindValid: Integer;
  FingerOn:SmallInt;

implementation
  uses AvzScanner;
{$R *.dfm}

Procedure TBiometrico.ParaRefresh;
var m_brightness,m_contrast,m_hue,m_saturation,m_sharpness,m_gama,m_wbalance,m_exposure:Word;
begin
  m_brightness := 0;
	m_contrast := 50;
	m_hue := 0;
	m_saturation := 0;
	m_sharpness := 0;
	m_gama := 30;
	m_wbalance := 15;
	m_exposure := 30;

	AvzSetParm(ComboBox1.ItemIndex,
					m_brightness, m_contrast,
					m_hue, m_saturation,
					m_sharpness, m_gama,
					m_wbalance, m_exposure);
end;

Procedure TBiometrico.EnabledCapturar;

begin
  Button3.Enabled:=True;
end;

procedure TBiometrico.Button1Click(Sender: TObject);
var P:ScannerName;
    I,J:Integer;
begin
    
    ComboBox1.Items.Clear;
    FillChar(P,Sizeof(P),Char(0));
    AvzFind:=AvzFindDevice(P);
    AvzFindValid:=0;
    for I:=0 to AvzFind-1 do
    begin
        tmpS:='';
        for J:=0 to 127 do
        begin
            if P[I,J]=Char(0) then Break
            else tmpS:=tmpS+P[I,J];
        end;
    end;
    if tmpS='' then
       begin
        AvzFindValid:=AvzFindValid+1;
        Showmessage('No se detecto el biometrico');
      end
    else
       begin
        ComboBox1.Items.Add(tmpS);
        Showmessage('Biometrico: '+tmpS);
      end;
end;

procedure TBiometrico.Button2Click(Sender: TObject);
begin
      if (ComboBox1.ItemIndex<>-1) then
       begin
          Hwnd:=Panel1.Handle;
          AvzOpen:=AvzOpenDevice(ComboBox1.ItemIndex,Hwnd);
          if AvzOpen=-1 then Showmessage('No se detecto el biometrico')
          else begin
            ComboBox1.Enabled:=False;
            Button1.Enabled:=False;
            gbValidScanner:=True;
            AvzGetImage(ComboBox1.ItemIndex,ImageProc,FingerOn);
              if FingerOn=0 then
                begin
                  Showmessage('Biometrico iniciado correctamente, presione capturar huellas');
                  Button2.Enabled:=False;
                  Button3.Enabled:=True;
              end;
              //else
                //begin
                  //Showmessage('Huellas detectada, presione capturar huellas');
              //end;
          end;
      end
     else
       begin
       Showmessage('Seleccione el biometrico detectedo');
      end;
end;
procedure TBiometrico.Button3Click(Sender: TObject);
var FileName:String;
    ret:Integer;
    validar:Integer;
    Fs:TFileStream;
    FileNameAvn:String;
begin
    AvzGetImage(ComboBox1.ItemIndex,ImageProc,FingerOn);
    if FingerOn=0 then
    begin
      Showmessage('No hay huellas en el biometrico');
      Button2.Enabled:=False;
    end
    else
      begin
        AvzProces := AvzProcess(ImageProc,pFeature,ImageBin,TRUE,TRUE);
        if AvzProces=0 then
          begin
            ADOQuery1.Close();
            ADOQuery1.SQL.Clear();
            ADOQuery1.SQL.Add('SELECT pb.huella, CONCAT(p.documento,p.cedula) AS dni');
            ADOQuery1.SQL.Add('FROM sp_biometrico_temp bt');
            ADOQuery1.SQL.Add('INNER JOIN sp_personal_biometrico pb ON bt.idpersonal = pb.idpersonal');
            ADOQuery1.SQL.Add('INNER JOIN sp_personal p ON p.idpersonal = pb.idpersonal');
            ADOQuery1.SQL.Add('WHERE pb.huella=bt.huella');
            ADOQuery1.Open();
            FileNameAvn :=ADOQuery1.FieldByName('dni').AsString;

            if ADOQuery1.isEmpty then
              begin
                 Showmessage('Captura de huella negada verifique');
              end
              else
                begin
                 if not AvzLoadFeature(Pchar('C:\xampp\htdocs\sispol\bhcf\Huellas\'+FileNameAvn+'.anv'),pFeatureA,pFeatureB) then
                begin
                  Exit;
              end;
                  ret:=AvzMatch(pFeature,pFeatureA,3);
                    if ret=0 then
                      begin
                        Showmessage('Captura de huella, exitosamente. Espere mientras el sistema procesa');
                        Button3.Enabled:=False;
                        self.Close;
                      end
                    else
                      begin
                        Showmessage('Captura de huella negada verifique');
                    end;
                end;
          end
          else
            Showmessage('Verifique, el sensor del biometrico. Vuelva intentar');
          end
end;


end.
