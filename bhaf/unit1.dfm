object Biometrico: TBiometrico
  Left = 210
  Top = 192
  Width = 353
  Height = 247
  Caption = 'Biometrico Capturar Huellas'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 16
    Top = 0
    Width = 145
    Height = 20
    Caption = 'Detectar Biometrico'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Panel1: TPanel
    Left = 16
    Top = 56
    Width = 305
    Height = 145
    TabOrder = 1
    object Image1: TImage
      Left = 72
      Top = 0
      Width = 153
      Height = 137
    end
  end
  object ComboBox1: TComboBox
    Left = 176
    Top = 0
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 2
  end
  object Button2: TButton
    Left = 16
    Top = 24
    Width = 145
    Height = 25
    Caption = 'Abrir Biometrico'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 176
    Top = 24
    Width = 145
    Height = 25
    Caption = 'Capturar Huella'
    Enabled = False
    TabOrder = 4
    OnClick = Button3Click
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data' +
      ' Source=sispol;Initial Catalog=sispol'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 48
    Top = 64
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sp_personal')
    Left = 272
    Top = 64
  end
  object ADOQuery2: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sp_personal')
    Left = 272
    Top = 104
  end
end
