DELIMITER $$
DROP TRIGGER IF EXISTS `sispol`.`sp_personal_armamentos_insert`$$
#log de insertados(Nuevos registros)
DELIMITER $$
CREATE
TRIGGER `sispol`.`sp_personal_armamentos_insert`
AFTER INSERT ON `sispol`.`sp_personal_armamentos`
FOR EACH ROW
BEGIN
    SET @newq = CONCAT (
						' ID :',ifnull(new.idpersonalarmamento,''),',',
						' ID Personal: ',ifnull(new.idpersonal,''),',',
						' ID Armas: ',ifnull(new.idarmas,''),',',
						' Cantidad: ',ifnull(new.cantarmas,''),',',
						' Fecha Asig.: ',ifnull(new.fechaasignacion,''),',',
						' Hora Asig.: ',ifnull(new.horaasignacion,''),',',
						' Fecha Dev.: ',ifnull(new.fechadevolucion,''),',',
						' Hora Dev.: ',ifnull(new.horadevolucion,''),',',
						' Resp. Asig.: ',ifnull(new.responsableasignacion,''),',',
						' Resp. Dev.: ',ifnull(new.responsabledevolucion,''),',',
						' Estatus Arma: ',ifnull(new.estatusarma,''),',',
						' Estatus: ',ifnull(new.estatusasignacion,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, fecha)                
    VALUES (NOW(),"INSERT","sp_personal_armamentos",new.usuario_creacion,@newq, NOW());
END$$

DELIMITER $$
DROP TRIGGER IF EXISTS `sispol`.`sp_personal_armamentos_update`$$
#log de insertados(Nuevos registros sp_personal)
DELIMITER $$
USE `sispol`$$
CREATE
TRIGGER `sispol`.`sp_personal_armamentos_update`
BEFORE UPDATE ON `sispol`.`sp_personal_armamentos`
FOR EACH ROW
BEGIN
    SET @oldq = CONCAT (
    					' ID :',ifnull(old.idpersonalarmamento,''),',',
						' ID Personal: ',ifnull(old.idpersonal,''),',',
						' ID Armas: ',ifnull(old.idarmas,''),',',
						' Cantidad: ',ifnull(old.cantarmas,''),',',
						' Fecha Asig.: ',ifnull(old.fechaasignacion,''),',',
						' Hora Asig.: ',ifnull(old.horaasignacion,''),',',
						' Fecha Dev.: ',ifnull(old.fechadevolucion,''),',',
						' Hora Dev.: ',ifnull(old.horadevolucion,''),',',
						' Resp. Asig.: ',ifnull(old.responsableasignacion,''),',',
						' Resp. Dev.: ',ifnull(old.responsabledevolucion,''),',',
						' Estatus Arma: ',ifnull(old.estatusarma,''),',',
						' Estatus: ',ifnull(old.estatusasignacion,'')
						);
		SET @newq = CONCAT (
						' ID :',ifnull(new.idpersonalarmamento,''),',',
						' ID Personal: ',ifnull(new.idpersonal,''),',',
						' ID Armas: ',ifnull(new.idarmas,''),',',
						' Cantidad: ',ifnull(new.cantarmas,''),',',
						' Fecha Asig.: ',ifnull(new.fechaasignacion,''),',',
						' Hora Asig.: ',ifnull(new.horaasignacion,''),',',
						' Fecha Dev.: ',ifnull(new.fechadevolucion,''),',',
						' Hora Dev.: ',ifnull(new.horadevolucion,''),',',
						' Resp. Asig.: ',ifnull(new.responsableasignacion,''),',',
						' Resp. Dev.: ',ifnull(new.responsabledevolucion,''),',',
						' Estatus Arma: ',ifnull(new.estatusarma,''),',',
						' Estatus: ',ifnull(new.estatusasignacion,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, informacion_old, fecha)                
    VALUES (NOW(),"UPDATE","sp_personal_armamentos",new.usuario_modificacion,@newq, @oldq, NOW());
END$$
