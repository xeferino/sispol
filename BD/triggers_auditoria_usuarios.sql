DELIMITER $$
DROP TRIGGER IF EXISTS `sispol`.`sp_usuarios_insert`$$
DELIMITER $$
CREATE
TRIGGER `sispol`.`sp_usuarios_insert`
AFTER INSERT ON `sispol`.`sp_usuarios`
FOR EACH ROW
BEGIN
    SET @newq = CONCAT (
						' ID: ',ifnull(new.idusuario,''),',',
						' Nombres: ',ifnull(new.nombres,''),',',
						' Apellidos: ',ifnull(new.apellidos,''),',',
						' Documento: ',ifnull(new.documento,''),',',
						' Cedula: ',ifnull(new.cedula,''),',',
						' Email: ',PASSWORD(ifnull(new.email,'')),',',
						' Usuario: ',PASSWORD(ifnull(new.nombreusuario,'')),',',
						' Clave: ',ifnull(new.claveusuario,''),',',
						' Tipo: ',ifnull(new.tipo,''),',',
						' Estatus: ',ifnull(new.estatususuario,''),',',
						' Pregunta: ',PASSWORD(ifnull(new.pregunta,'')),',',
						' Respuesta: ',PASSWORD(ifnull(new.respuesta,'')),',',
						' Login: ',ifnull(new.login,''),',',
						' Last Login: ',ifnull(new.last_login,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, fecha)                
    VALUES (NOW(),"INSERT","sp_usuarios",new.usuario_creacion,@newq, NOW());
END$$

DELIMITER $$
DROP TRIGGER IF EXISTS `sispol`.`sp_usuarios_update`$$
#log de insertados(Nuevos registros sp_usuarios)
DELIMITER $$
CREATE
TRIGGER `sispol`.`sp_usuarios_update`
BEFORE UPDATE ON `sispol`.`sp_usuarios`
FOR EACH ROW
BEGIN
    SET @newq = CONCAT (
						' ID: ',ifnull(new.idusuario,''),',',
						' Nombres: ',ifnull(new.nombres,''),',',
						' Apellidos: ',ifnull(new.apellidos,''),',',
						' Documento: ',ifnull(new.documento,''),',',
						' Cedula: ',ifnull(new.cedula,''),',',
						' Email: ',PASSWORD(ifnull(new.email,'')),',',
						' Usuario: ',PASSWORD(ifnull(new.nombreusuario,'')),',',
						' Clave.: ',ifnull(new.claveusuario,''),',',
						' Tipo: ',ifnull(new.tipo,''),',',
						' Estatus: ',ifnull(new.estatususuario,''),',',
						' Pregunta: ',PASSWORD(ifnull(new.pregunta,'')),',',
						' Respuesta: ',PASSWORD(ifnull(new.respuesta,'')),',',
						' Login: ',ifnull(new.login,''),',',
						' Last Login: ',ifnull(new.last_login,'')
						);
		SET @oldq = CONCAT (
						' ID :',ifnull(old.idusuario,''),',',
						' Nombres :',ifnull(old.nombres,''),',',
						' Apellidos :',ifnull(old.apellidos,''),',',
						' Documento :',ifnull(old.documento,''),',',
						' Cedula :',ifnull(old.cedula,''),',',
						' Email :',PASSWORD(ifnull(old.email,'')),',',
						' Usuario :',PASSWORD(ifnull(old.nombreusuario,'')),',',
						' Clave. :',ifnull(old.claveusuario,''),',',
						' Tipo :',ifnull(old.tipo,''),',',
						' Estatus :',ifnull(old.estatususuario,''),',',
						' Pregunta :',PASSWORD(ifnull(old.pregunta,'')),',',
						' Respuesta :',PASSWORD(ifnull(old.respuesta,'')),',',
						' Login: ',ifnull(old.login,''),',',
						' Last Login: ',ifnull(old.last_login,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, informacion_old, fecha)                
    VALUES (NOW(),"UPDATE","sp_usuarios",new.usuario_creacion,@newq, @oldq, NOW());
END$$