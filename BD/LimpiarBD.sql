TRUNCATE TABLE sp_armamentos;
TRUNCATE TABLE sp_auditoria;
TRUNCATE TABLE sp_armamentos;
TRUNCATE TABLE sp_biometrico_temp;
TRUNCATE TABLE sp_personal;
TRUNCATE TABLE sp_personal_armamentos;
TRUNCATE TABLE sp_personal_biometrico;

DELETE
sp_usuarios,
sp_modulos_usuarios
FROM
	sp_usuarios
INNER JOIN sp_modulos_usuarios ON sp_usuarios.idusuario = sp_modulos_usuarios.idusuario
WHERE
	sp_usuarios.tipo NOT IN ('analista sistema');
