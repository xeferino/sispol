DELIMITER $$
DROP TRIGGER IF EXISTS `sispol`.`sp_armamentos_insert`$$
#log de insertados(Nuevos registros)
DELIMITER $$
CREATE
TRIGGER `sispol`.`sp_armamentos_insert`
AFTER INSERT ON `sispol`.`sp_armamentos`
FOR EACH ROW
BEGIN
    SET @newq = CONCAT (
						' ID :',ifnull(new.idarma,''),',',
						' Codigo :',ifnull(new.codigo,''),',',
						' Tipo :',ifnull(new.tipo,''),',',
						' Calibre :',ifnull(new.calibre,''),',',
						' Descripcion :',ifnull(new.descripcion,''),',',
						' Cantidad :',ifnull(new.cantidad,''),',',
						' CantidadAsignada :',ifnull(new.cantidad_asignada,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, fecha)                
    VALUES (NOW(),"INSERT","sp_armamentos",new.usuario_creacion,@newq, NOW());
END$$

DELIMITER $$
DROP TRIGGER IF EXISTS `sispol`.`sp_armamentos_update`$$
#log de insertados(Nuevos registros sp_personal)
DELIMITER $$
USE `sispol`$$
CREATE
TRIGGER `sispol`.`sp_armamentos_update`
BEFORE UPDATE ON `sispol`.`sp_armamentos`
FOR EACH ROW
BEGIN
    SET @oldq = CONCAT (
    					' ID :',ifnull(old.idarma,''),',',
						' Codigo :',ifnull(old.codigo,''),',',
						' Tipo :',ifnull(old.tipo,''),',',
						' Calibre :',ifnull(old.calibre,''),',',
						' Descripcion :',ifnull(old.descripcion,''),',',
						' Cantidad :',ifnull(old.cantidad,''),',',
						' CantidadAsignada :',ifnull(old.cantidad_asignada,'')
						);
		SET @newq = CONCAT (
						' ID :',ifnull(new.idarma,''),',',
						' Codigo :',ifnull(new.codigo,''),',',
						' Tipo :',ifnull(new.tipo,''),',',
						' Calibre :',ifnull(new.calibre,''),',',
						' Descripcion :',ifnull(new.descripcion,''),',',
						' Cantidad :',ifnull(new.cantidad,''),',',
						' CantidadAsignada :',ifnull(new.cantidad_asignada,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, informacion_old, fecha)                
    VALUES (NOW(),"UPDATE","sp_armamentos",new.usuario_modificacion,@newq, @oldq, NOW());
END$$
