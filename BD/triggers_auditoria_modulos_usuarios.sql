DELIMITER $$
DROP TRIGGER IF EXISTS `sispol`.`sp_modulos_usuarios_insert`$$
DELIMITER $$
CREATE
TRIGGER `sispol`.`sp_modulos_usuarios_insert`
AFTER INSERT ON `sispol`.`sp_modulos_usuarios`
FOR EACH ROW
BEGIN
	#SET @modulos = (SELECT nombremodulo FROM sp_view_modulos_usuarios WHERE idusuario = new.idusuario);
    SET @newq = CONCAT (
						' ID: ',ifnull(new.idmodulo_usuario,''),',',
						' ID Usuario: ',ifnull(new.idusuario,''),',',
						' ID Modulo: ',ifnull(new.idmodulo,''),',',
						' Tipo: ',ifnull(new.tipo,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, fecha)                
    VALUES (NOW(),"INSERT","sp_modulos_usuarios",new.usuario_creacion,@newq, NOW());
END$$

