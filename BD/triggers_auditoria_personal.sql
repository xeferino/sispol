DELIMITER $$
DROP TRIGGER IF EXISTS `sispol`.`sp_personal_insert`$$
#log de insertados(Nuevos registros sp_personal)
DELIMITER $$
USE `sispol`$$
CREATE
TRIGGER `sispol`.`sp_personal_insert`
AFTER INSERT ON `sispol`.`sp_personal`
FOR EACH ROW
BEGIN
    SET @newq = CONCAT (
    					' ID :',ifnull(new.idpersonal,''),',',
						' Nombres :',ifnull(new.nombres,''),',',
						' Apellidos :',ifnull(new.apellidos,''),',',
						' Documento :',ifnull(new.documento,''),',',
						' Cedula :',ifnull(new.cedula,''),',',
						' Placa :',ifnull(new.placa,''),',',
						' Direccion :',ifnull(new.direccion,''),',',
						' Telefono Hab. :',ifnull(new.telefono_habitacion,''),',',
						' Telefono Cel. :',ifnull(new.telefono_celular,''),',',
						' Rango :',ifnull(new.rango,''),',',
						' Estatus :',ifnull(new.estatus,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, fecha)                
    VALUES (NOW(),"INSERT","sp_personal",new.usuario_creacion,@newq, NOW());
END$$

DELIMITER $$
DROP TRIGGER IF EXISTS `sispol`.`sp_personal_update`$$
#log de insertados(Nuevos registros sp_personal)
DELIMITER $$
USE `sispol`$$
CREATE
TRIGGER `sispol`.`sp_personal_update`
BEFORE UPDATE ON `sispol`.`sp_personal`
FOR EACH ROW
BEGIN
    SET @oldq = CONCAT (
    					' ID :',ifnull(old.idpersonal,''),',',
						' Nombres :',ifnull(old.nombres,''),',',
						' Apellidos :',ifnull(old.apellidos,''),',',
						' Documento :',ifnull(old.documento,''),',',
						' Cedula :',ifnull(old.cedula,''),',',
						' Placa :',ifnull(old.placa,''),',',
						' Direccion :',ifnull(old.direccion,''),',',
						' Telefono Hab. :',ifnull(old.telefono_habitacion,''),',',
						' Telefono Cel. :',ifnull(old.telefono_celular,''),',',
						' Rango :',ifnull(old.rango,''),',',
						' Estatus :',ifnull(old.estatus,'')
						);
		SET @newq = CONCAT (
    					' ID :',ifnull(new.idpersonal,''),',',
						' Nombres :',ifnull(new.nombres,''),',',
						' Apellidos :',ifnull(new.apellidos,''),',',
						' Documento :',ifnull(new.documento,''),',',
						' Cedula :',ifnull(new.cedula,''),',',
						' Placa :',ifnull(new.placa,''),',',
						' Direccion :',ifnull(new.direccion,''),',',
						' Telefono Hab. :',ifnull(new.telefono_habitacion,''),',',
						' Telefono Cel. :',ifnull(new.telefono_celular,''),',',
						' Rango :',ifnull(new.rango,''),',',
						' Estatus :',ifnull(new.estatus,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, informacion_old, fecha)                
    VALUES (NOW(),"UPDATE","sp_personal",new.usuario_modificacion,@newq, @oldq, NOW());
END$$
