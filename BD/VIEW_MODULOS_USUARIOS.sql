-- ----------------------------
-- View structure for sp_view_modulos_usuarios
-- ----------------------------
DROP VIEW IF EXISTS `sp_view_modulos_usuarios`;
CREATE VIEW `sp_view_modulos_usuarios` AS SELECT m.*, u.idusuario FROM sp_usuarios as u
INNER JOIN sp_modulos_usuarios as mu ON mu.idusuario = u.idusuario
INNER JOIN sp_modulos as m ON m.idmodulo = mu.idmodulo ;