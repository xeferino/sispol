/*
Navicat MySQL Data Transfer

Source Server         : Xampp
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : sispol

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-02-17 11:00:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sp_armamentos
-- ----------------------------
DROP TABLE IF EXISTS `sp_armamentos`;
CREATE TABLE `sp_armamentos` (
  `idarma` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(50) NOT NULL,
  `tipo` enum('ESPOSA','CHALECO','MUNICION','ESCOPETA','PISTOLA') NOT NULL,
  `calibre` enum('12 MM','9 MM') DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `cantidad` int(11) NOT NULL,
  `cantidad_asignada` int(11) DEFAULT '0',
  `usuario_creacion` int(11) DEFAULT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_modificacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_creacion` varchar(255) DEFAULT NULL,
  `ip_modificacion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idarma`),
  UNIQUE KEY `codigo` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sp_armamentos
-- ----------------------------

-- ----------------------------
-- Table structure for sp_auditoria
-- ----------------------------
DROP TABLE IF EXISTS `sp_auditoria`;
CREATE TABLE `sp_auditoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fechahora` datetime DEFAULT NULL,
  `accion` varchar(255) DEFAULT NULL,
  `tabla` varchar(255) DEFAULT NULL,
  `idusuario` int(11) DEFAULT NULL,
  `informacion_new` text,
  `informacion_old` text,
  `fecha` timestamp NULL DEFAULT NULL,
  `ip_creacion` varchar(255) DEFAULT NULL,
  `ip_modificacion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sp_auditoria
-- ----------------------------

-- ----------------------------
-- Table structure for sp_biometrico_temp
-- ----------------------------
DROP TABLE IF EXISTS `sp_biometrico_temp`;
CREATE TABLE `sp_biometrico_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpersonal` int(11) NOT NULL,
  `huella` varchar(255) NOT NULL,
  `proceso` varchar(255) DEFAULT NULL,
  `usuario_creacion` int(11) DEFAULT NULL,
  `fecha_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_modificacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_creacion` varchar(255) DEFAULT NULL,
  `ip_modificacion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sp_biometrico_temp
-- ----------------------------

-- ----------------------------
-- Table structure for sp_configuracion
-- ----------------------------
DROP TABLE IF EXISTS `sp_configuracion`;
CREATE TABLE `sp_configuracion` (
  `idconfiguracion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `valor` varchar(255) NOT NULL,
  `usuario_creacion` int(11) DEFAULT NULL,
  `fecha_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_modificacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_creacion` varchar(255) DEFAULT NULL,
  `ip_modificacion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idconfiguracion`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sp_configuracion
-- ----------------------------
INSERT INTO `sp_configuracion` VALUES ('1', 'biometrico', 'no', '1', null, '1', '2020-02-12 20:00:23', null, null);
INSERT INTO `sp_configuracion` VALUES ('2', 'emailEmisor', 'notificaciones.mail.alertas@gmail.com', '1', null, '1', '2019-11-08 09:04:33', null, null);
INSERT INTO `sp_configuracion` VALUES ('3', 'emailReceptor', 'JOSEGREGORIOLOZADAE@YAHOO.COM', '1', null, '1', '2019-11-08 09:04:33', null, null);
INSERT INTO `sp_configuracion` VALUES ('4', 'passEmailEmisor', 'LiUqL25vdGltYWlsYWxlci4lKi8=', null, '2019-11-07 12:47:23', null, '2019-11-07 14:05:58', null, null);

-- ----------------------------
-- Table structure for sp_modulos
-- ----------------------------
DROP TABLE IF EXISTS `sp_modulos`;
CREATE TABLE `sp_modulos` (
  `idmodulo` int(11) NOT NULL AUTO_INCREMENT,
  `nombremodulo` varchar(255) NOT NULL,
  `urlmodulo` varchar(255) NOT NULL,
  `iconomodulo` varchar(50) NOT NULL,
  `estatus` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `segmento` varchar(20) NOT NULL,
  PRIMARY KEY (`idmodulo`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sp_modulos
-- ----------------------------
INSERT INTO `sp_modulos` VALUES ('1', 'Dashboard', 'dashboard', 'dashboard', '0', 'Modulo Dashboard', 'dashboard');
INSERT INTO `sp_modulos` VALUES ('2', 'Usuarios', 'usuario', 'users', '0', 'Modulo  Usuarios', 'usuario');
INSERT INTO `sp_modulos` VALUES ('3', 'Personal', 'personal', 'user', '0', 'Modulo  Personal', 'personal');
INSERT INTO `sp_modulos` VALUES ('4', 'Armanentos', 'armas', 'bomb', '0', 'Modulo  Armanentos', 'armas');
INSERT INTO `sp_modulos` VALUES ('6', 'Auditoria', 'auditoria', 'institution', '0', 'Modulo  Auditoria', 'auditoria');
INSERT INTO `sp_modulos` VALUES ('7', 'Reportes', 'reportes', 'file-pdf-o', '0', 'Modulo  Reportes', 'reportes');
INSERT INTO `sp_modulos` VALUES ('8', 'Capturar Huellas', 'huellas', ' fa-desktop', '1', 'Modulo  Capturar Huellas', 'huellas');
INSERT INTO `sp_modulos` VALUES ('9', 'Configuración', 'configuracion', ' fa-cogs', '0', 'Modulo  Configuración', 'configuracion');

-- ----------------------------
-- Table structure for sp_modulos_usuarios
-- ----------------------------
DROP TABLE IF EXISTS `sp_modulos_usuarios`;
CREATE TABLE `sp_modulos_usuarios` (
  `idmodulo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `idusuario` int(11) NOT NULL,
  `idmodulo` int(11) NOT NULL,
  `tipo` varchar(255) NOT NULL DEFAULT '',
  `usuario_creacion` int(11) DEFAULT NULL,
  `fecha_creacion` timestamp NULL DEFAULT NULL,
  `usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_modificacion` timestamp NULL DEFAULT NULL,
  `ip_creacion` varchar(255) DEFAULT NULL,
  `ip_modificacion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idmodulo_usuario`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sp_modulos_usuarios
-- ----------------------------
INSERT INTO `sp_modulos_usuarios` VALUES ('33', '1', '1', 'sp_modulo', '1', null, null, null, null, null);
INSERT INTO `sp_modulos_usuarios` VALUES ('34', '1', '2', 'sp_modulo', '1', null, null, null, null, null);
INSERT INTO `sp_modulos_usuarios` VALUES ('35', '1', '3', 'sp_modulo', '1', null, null, null, null, null);
INSERT INTO `sp_modulos_usuarios` VALUES ('36', '1', '4', 'sp_modulo', '1', null, null, null, null, null);
INSERT INTO `sp_modulos_usuarios` VALUES ('37', '1', '6', 'sp_modulo', '1', null, null, null, null, null);
INSERT INTO `sp_modulos_usuarios` VALUES ('38', '1', '7', 'sp_modulo', '1', null, null, null, null, null);
INSERT INTO `sp_modulos_usuarios` VALUES ('40', '1', '9', 'sp_modulo', '1', null, null, null, null, null);

-- ----------------------------
-- Table structure for sp_notificaciones
-- ----------------------------
DROP TABLE IF EXISTS `sp_notificaciones`;
CREATE TABLE `sp_notificaciones` (
  `idnotificacion` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `prioridad` varchar(100) NOT NULL DEFAULT 'Normal',
  `fechahora` datetime NOT NULL,
  `estatus` varchar(100) NOT NULL,
  `usuario_creacion` int(11) DEFAULT NULL,
  `fecha_creacion` timestamp NULL DEFAULT NULL,
  `usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_modificacion` timestamp NULL DEFAULT NULL,
  `ip_creacion` varchar(255) DEFAULT NULL,
  `ip_modificacion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idnotificacion`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sp_notificaciones
-- ----------------------------

-- ----------------------------
-- Table structure for sp_personal
-- ----------------------------
DROP TABLE IF EXISTS `sp_personal`;
CREATE TABLE `sp_personal` (
  `idpersonal` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `documento` enum('V','E') NOT NULL DEFAULT 'V',
  `cedula` int(11) NOT NULL,
  `placa` int(11) NOT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `telefono_habitacion` varchar(20) DEFAULT NULL,
  `telefono_celular` varchar(20) DEFAULT NULL,
  `rango` enum('comisionado jefe','comisionado agregado','comisionado','supervisor jefe','supervisor agregado','supervisor','oficial jefe','oficial agregado','oficial','personal motorizado','jefe de operaciones','comandante segundo','comandante') DEFAULT NULL,
  `estatus` int(11) NOT NULL DEFAULT '0',
  `sexo` enum('F','M') DEFAULT NULL,
  `usuario_creacion` int(11) DEFAULT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_modificacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_creacion` varchar(255) DEFAULT NULL,
  `ip_modificacion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idpersonal`),
  UNIQUE KEY `placa` (`placa`),
  UNIQUE KEY `cedula` (`documento`,`cedula`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sp_personal
-- ----------------------------

-- ----------------------------
-- Table structure for sp_personal_armamentos
-- ----------------------------
DROP TABLE IF EXISTS `sp_personal_armamentos`;
CREATE TABLE `sp_personal_armamentos` (
  `idpersonalarmamento` int(11) NOT NULL AUTO_INCREMENT,
  `idpersonal` int(11) NOT NULL,
  `idarmas` varchar(255) NOT NULL,
  `cantarmas` varchar(50) NOT NULL,
  `fechaasignacion` date NOT NULL,
  `horaasignacion` time NOT NULL,
  `fechadevolucion` date NOT NULL,
  `horadevolucion` time NOT NULL,
  `tiempoAsignacion` varchar(255) NOT NULL,
  `tiempoRetraso` time DEFAULT NULL,
  `responsableasignacion` varchar(255) DEFAULT NULL,
  `responsabledevolucion` varchar(255) NOT NULL,
  `estatusarma` varchar(20) NOT NULL,
  `estatusasignacion` varchar(50) DEFAULT NULL,
  `usuario_creacion` int(11) DEFAULT NULL,
  `fecha_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_modificacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_creacion` varchar(255) DEFAULT NULL,
  `ip_modificacion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idpersonalarmamento`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sp_personal_armamentos
-- ----------------------------

-- ----------------------------
-- Table structure for sp_personal_biometrico
-- ----------------------------
DROP TABLE IF EXISTS `sp_personal_biometrico`;
CREATE TABLE `sp_personal_biometrico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpersonal` int(11) NOT NULL,
  `huella` varchar(255) NOT NULL,
  `usuario_creacion` int(11) DEFAULT NULL,
  `fecha_creacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_modificacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_creacion` varchar(255) DEFAULT NULL,
  `ip_modificacion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sp_personal_biometrico
-- ----------------------------

-- ----------------------------
-- Table structure for sp_usuarios
-- ----------------------------
DROP TABLE IF EXISTS `sp_usuarios`;
CREATE TABLE `sp_usuarios` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `documento` enum('V','E') NOT NULL DEFAULT 'V',
  `cedula` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nombreusuario` varchar(255) NOT NULL,
  `claveusuario` varchar(255) NOT NULL,
  `tipo` enum('analista sistema','jefe','parquero','comandante') NOT NULL,
  `estatususuario` int(11) NOT NULL DEFAULT '0',
  `pregunta` varchar(300) NOT NULL,
  `respuesta` varchar(300) NOT NULL,
  `usuario_creacion` int(11) DEFAULT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_modificacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_creacion` varchar(255) DEFAULT NULL,
  `ip_modificacion` varchar(255) DEFAULT NULL,
  `login` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`idusuario`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `nombreusuario` (`nombreusuario`),
  UNIQUE KEY `cedula` (`documento`,`cedula`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sp_usuarios
-- ----------------------------
INSERT INTO `sp_usuarios` VALUES ('1', 'YUGDELIS', 'GARATE', 'V', '20022992', 'yugdelis@gmail.com', 'YUGDE', 'ebb91664381714e036954ec94834521a', 'analista sistema', '0', 'color', 'rojo', '1', '2019-10-27 17:37:23', '1', '2020-02-17 10:58:09', null, null, '2020-02-17 10:58:09', '2020-02-17 10:57:51');

-- ----------------------------
-- View structure for sp_view_modulos_usuarios
-- ----------------------------
DROP VIEW IF EXISTS `sp_view_modulos_usuarios`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `sp_view_modulos_usuarios` AS SELECT m.*, u.idusuario FROM sp_usuarios as u
INNER JOIN sp_modulos_usuarios as mu ON mu.idusuario = u.idusuario
INNER JOIN sp_modulos as m ON m.idmodulo = mu.idmodulo ;
DROP TRIGGER IF EXISTS `sp_armamentos_insert`;
DELIMITER ;;
CREATE TRIGGER `sp_armamentos_insert` AFTER INSERT ON `sp_armamentos` FOR EACH ROW BEGIN
    SET @newq = CONCAT (
						' ID :',ifnull(new.idarma,''),',',
						' Codigo :',ifnull(new.codigo,''),',',
						' Tipo :',ifnull(new.tipo,''),',',
						' Calibre :',ifnull(new.calibre,''),',',
						' Descripcion :',ifnull(new.descripcion,''),',',
						' Cantidad :',ifnull(new.cantidad,''),',',
						' CantidadAsignada :',ifnull(new.cantidad_asignada,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, fecha)                
    VALUES (NOW(),"INSERT","sp_armamentos",new.usuario_creacion,@newq, NOW());
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `sp_armamentos_update`;
DELIMITER ;;
CREATE TRIGGER `sp_armamentos_update` BEFORE UPDATE ON `sp_armamentos` FOR EACH ROW BEGIN
    SET @oldq = CONCAT (
    					' ID :',ifnull(old.idarma,''),',',
						' Codigo :',ifnull(old.codigo,''),',',
						' Tipo :',ifnull(old.tipo,''),',',
						' Calibre :',ifnull(old.calibre,''),',',
						' Descripcion :',ifnull(old.descripcion,''),',',
						' Cantidad :',ifnull(old.cantidad,''),',',
						' CantidadAsignada :',ifnull(old.cantidad_asignada,'')
						);
		SET @newq = CONCAT (
						' ID :',ifnull(new.idarma,''),',',
						' Codigo :',ifnull(new.codigo,''),',',
						' Tipo :',ifnull(new.tipo,''),',',
						' Calibre :',ifnull(new.calibre,''),',',
						' Descripcion :',ifnull(new.descripcion,''),',',
						' Cantidad :',ifnull(new.cantidad,''),',',
						' CantidadAsignada :',ifnull(new.cantidad_asignada,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, informacion_old, fecha)                
    VALUES (NOW(),"UPDATE","sp_armamentos",new.usuario_modificacion,@newq, @oldq, NOW());
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `sp_configuracion_insert`;
DELIMITER ;;
CREATE TRIGGER `sp_configuracion_insert` AFTER INSERT ON `sp_configuracion` FOR EACH ROW BEGIN
    SET @newq = CONCAT (
						' ID :',ifnull(new.idconfiguracion,''),',',
						' Nombre :',ifnull(new.nombre,''),',',
						' Valor :',ifnull(new.valor,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, fecha)                
    VALUES (NOW(),"INSERT","sp_configuracion",new.usuario_creacion,@newq, NOW());
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `sp_configuracion_update`;
DELIMITER ;;
CREATE TRIGGER `sp_configuracion_update` BEFORE UPDATE ON `sp_configuracion` FOR EACH ROW BEGIN
    SET @oldq = CONCAT (
    					' ID :',ifnull(old.idconfiguracion,''),',',
						' Nombre :',ifnull(old.nombre,''),',',
						' Valor :',ifnull(old.valor,'')
						);
		SET @newq = CONCAT (
						' ID :',ifnull(new.idconfiguracion,''),',',
						' Nombre :',ifnull(new.nombre,''),',',
						' Valor :',ifnull(new.valor,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, informacion_old, fecha)                
    VALUES (NOW(),"UPDATE","sp_configuracion",new.usuario_modificacion,@newq, @oldq, NOW());
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `sp_modulos_usuarios_insert`;
DELIMITER ;;
CREATE TRIGGER `sp_modulos_usuarios_insert` AFTER INSERT ON `sp_modulos_usuarios` FOR EACH ROW BEGIN
	#SET @modulos = (SELECT nombremodulo FROM sp_view_modulos_usuarios WHERE idusuario = new.idusuario);
    SET @newq = CONCAT (
						' ID: ',ifnull(new.idmodulo_usuario,''),',',
						' ID Usuario: ',ifnull(new.idusuario,''),',',
						' ID Modulo: ',ifnull(new.idmodulo,''),',',
						' Tipo: ',ifnull(new.tipo,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, fecha)                
    VALUES (NOW(),"INSERT","sp_modulos_usuarios",new.usuario_creacion,@newq, NOW());
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `sp_personal_insert`;
DELIMITER ;;
CREATE TRIGGER `sp_personal_insert` AFTER INSERT ON `sp_personal` FOR EACH ROW BEGIN
    SET @newq = CONCAT (
    					' ID :',ifnull(new.idpersonal,''),',',
						' Nombres :',ifnull(new.nombres,''),',',
						' Apellidos :',ifnull(new.apellidos,''),',',
						' Documento :',ifnull(new.documento,''),',',
						' Cedula :',ifnull(new.cedula,''),',',
						' Placa :',ifnull(new.placa,''),',',
						' Direccion :',ifnull(new.direccion,''),',',
						' Telefono Hab. :',ifnull(new.telefono_habitacion,''),',',
						' Telefono Cel. :',ifnull(new.telefono_celular,''),',',
						' Rango :',ifnull(new.rango,''),',',
						' Estatus :',ifnull(new.estatus,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, fecha)                
    VALUES (NOW(),"INSERT","sp_personal",new.usuario_creacion,@newq, NOW());
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `sp_personal_update`;
DELIMITER ;;
CREATE TRIGGER `sp_personal_update` BEFORE UPDATE ON `sp_personal` FOR EACH ROW BEGIN
    SET @oldq = CONCAT (
    					' ID :',ifnull(old.idpersonal,''),',',
						' Nombres :',ifnull(old.nombres,''),',',
						' Apellidos :',ifnull(old.apellidos,''),',',
						' Documento :',ifnull(old.documento,''),',',
						' Cedula :',ifnull(old.cedula,''),',',
						' Placa :',ifnull(old.placa,''),',',
						' Direccion :',ifnull(old.direccion,''),',',
						' Telefono Hab. :',ifnull(old.telefono_habitacion,''),',',
						' Telefono Cel. :',ifnull(old.telefono_celular,''),',',
						' Rango :',ifnull(old.rango,''),',',
						' Estatus :',ifnull(old.estatus,'')
						);
		SET @newq = CONCAT (
    					' ID :',ifnull(new.idpersonal,''),',',
						' Nombres :',ifnull(new.nombres,''),',',
						' Apellidos :',ifnull(new.apellidos,''),',',
						' Documento :',ifnull(new.documento,''),',',
						' Cedula :',ifnull(new.cedula,''),',',
						' Placa :',ifnull(new.placa,''),',',
						' Direccion :',ifnull(new.direccion,''),',',
						' Telefono Hab. :',ifnull(new.telefono_habitacion,''),',',
						' Telefono Cel. :',ifnull(new.telefono_celular,''),',',
						' Rango :',ifnull(new.rango,''),',',
						' Estatus :',ifnull(new.estatus,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, informacion_old, fecha)                
    VALUES (NOW(),"UPDATE","sp_personal",new.usuario_modificacion,@newq, @oldq, NOW());
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `sp_personal_armamentos_insert`;
DELIMITER ;;
CREATE TRIGGER `sp_personal_armamentos_insert` AFTER INSERT ON `sp_personal_armamentos` FOR EACH ROW BEGIN
    SET @newq = CONCAT (
						' ID :',ifnull(new.idpersonalarmamento,''),',',
						' ID Personal: ',ifnull(new.idpersonal,''),',',
						' ID Armas: ',ifnull(new.idarmas,''),',',
						' Cantidad: ',ifnull(new.cantarmas,''),',',
						' Fecha Asig.: ',ifnull(new.fechaasignacion,''),',',
						' Hora Asig.: ',ifnull(new.horaasignacion,''),',',
						' Fecha Dev.: ',ifnull(new.fechadevolucion,''),',',
						' Hora Dev.: ',ifnull(new.horadevolucion,''),',',
						' Resp. Asig.: ',ifnull(new.responsableasignacion,''),',',
						' Resp. Dev.: ',ifnull(new.responsabledevolucion,''),',',
						' Estatus Arma: ',ifnull(new.estatusarma,''),',',
						' Estatus: ',ifnull(new.estatusasignacion,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, fecha)                
    VALUES (NOW(),"INSERT","sp_personal_armamentos",new.usuario_creacion,@newq, NOW());
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `sp_personal_armamentos_update`;
DELIMITER ;;
CREATE TRIGGER `sp_personal_armamentos_update` BEFORE UPDATE ON `sp_personal_armamentos` FOR EACH ROW BEGIN
    SET @oldq = CONCAT (
    					' ID :',ifnull(old.idpersonalarmamento,''),',',
						' ID Personal: ',ifnull(old.idpersonal,''),',',
						' ID Armas: ',ifnull(old.idarmas,''),',',
						' Cantidad: ',ifnull(old.cantarmas,''),',',
						' Fecha Asig.: ',ifnull(old.fechaasignacion,''),',',
						' Hora Asig.: ',ifnull(old.horaasignacion,''),',',
						' Fecha Dev.: ',ifnull(old.fechadevolucion,''),',',
						' Hora Dev.: ',ifnull(old.horadevolucion,''),',',
						' Resp. Asig.: ',ifnull(old.responsableasignacion,''),',',
						' Resp. Dev.: ',ifnull(old.responsabledevolucion,''),',',
						' Estatus Arma: ',ifnull(old.estatusarma,''),',',
						' Estatus: ',ifnull(old.estatusasignacion,'')
						);
		SET @newq = CONCAT (
						' ID :',ifnull(new.idpersonalarmamento,''),',',
						' ID Personal: ',ifnull(new.idpersonal,''),',',
						' ID Armas: ',ifnull(new.idarmas,''),',',
						' Cantidad: ',ifnull(new.cantarmas,''),',',
						' Fecha Asig.: ',ifnull(new.fechaasignacion,''),',',
						' Hora Asig.: ',ifnull(new.horaasignacion,''),',',
						' Fecha Dev.: ',ifnull(new.fechadevolucion,''),',',
						' Hora Dev.: ',ifnull(new.horadevolucion,''),',',
						' Resp. Asig.: ',ifnull(new.responsableasignacion,''),',',
						' Resp. Dev.: ',ifnull(new.responsabledevolucion,''),',',
						' Estatus Arma: ',ifnull(new.estatusarma,''),',',
						' Estatus: ',ifnull(new.estatusasignacion,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, informacion_old, fecha)                
    VALUES (NOW(),"UPDATE","sp_personal_armamentos",new.usuario_modificacion,@newq, @oldq, NOW());
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `sp_usuarios_insert`;
DELIMITER ;;
CREATE TRIGGER `sp_usuarios_insert` AFTER INSERT ON `sp_usuarios` FOR EACH ROW BEGIN
    SET @newq = CONCAT (
						' ID: ',ifnull(new.idusuario,''),',',
						' Nombres: ',ifnull(new.nombres,''),',',
						' Apellidos: ',ifnull(new.apellidos,''),',',
						' Documento: ',ifnull(new.documento,''),',',
						' Cedula: ',ifnull(new.cedula,''),',',
						' Email: ',PASSWORD(ifnull(new.email,'')),',',
						' Usuario: ',PASSWORD(ifnull(new.nombreusuario,'')),',',
						' Clave: ',ifnull(new.claveusuario,''),',',
						' Tipo: ',ifnull(new.tipo,''),',',
						' Estatus: ',ifnull(new.estatususuario,''),',',
						' Pregunta: ',PASSWORD(ifnull(new.pregunta,'')),',',
						' Respuesta: ',PASSWORD(ifnull(new.respuesta,'')),',',
						' Login: ',ifnull(new.login,''),',',
						' Last Login: ',ifnull(new.last_login,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, fecha)                
    VALUES (NOW(),"INSERT","sp_usuarios",new.usuario_creacion,@newq, NOW());
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `sp_usuarios_update`;
DELIMITER ;;
CREATE TRIGGER `sp_usuarios_update` BEFORE UPDATE ON `sp_usuarios` FOR EACH ROW BEGIN
    SET @newq = CONCAT (
						' ID: ',ifnull(new.idusuario,''),',',
						' Nombres: ',ifnull(new.nombres,''),',',
						' Apellidos: ',ifnull(new.apellidos,''),',',
						' Documento: ',ifnull(new.documento,''),',',
						' Cedula: ',ifnull(new.cedula,''),',',
						' Email: ',PASSWORD(ifnull(new.email,'')),',',
						' Usuario: ',PASSWORD(ifnull(new.nombreusuario,'')),',',
						' Clave.: ',ifnull(new.claveusuario,''),',',
						' Tipo: ',ifnull(new.tipo,''),',',
						' Estatus: ',ifnull(new.estatususuario,''),',',
						' Pregunta: ',PASSWORD(ifnull(new.pregunta,'')),',',
						' Respuesta: ',PASSWORD(ifnull(new.respuesta,'')),',',
						' Login: ',ifnull(new.login,''),',',
						' Last Login: ',ifnull(new.last_login,'')
						);
		SET @oldq = CONCAT (
						' ID :',ifnull(old.idusuario,''),',',
						' Nombres :',ifnull(old.nombres,''),',',
						' Apellidos :',ifnull(old.apellidos,''),',',
						' Documento :',ifnull(old.documento,''),',',
						' Cedula :',ifnull(old.cedula,''),',',
						' Email :',PASSWORD(ifnull(old.email,'')),',',
						' Usuario :',PASSWORD(ifnull(old.nombreusuario,'')),',',
						' Clave. :',ifnull(old.claveusuario,''),',',
						' Tipo :',ifnull(old.tipo,''),',',
						' Estatus :',ifnull(old.estatususuario,''),',',
						' Pregunta :',PASSWORD(ifnull(old.pregunta,'')),',',
						' Respuesta :',PASSWORD(ifnull(old.respuesta,'')),',',
						' Login: ',ifnull(old.login,''),',',
						' Last Login: ',ifnull(old.last_login,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, informacion_old, fecha)                
    VALUES (NOW(),"UPDATE","sp_usuarios",new.usuario_creacion,@newq, @oldq, NOW());
END
;;
DELIMITER ;
