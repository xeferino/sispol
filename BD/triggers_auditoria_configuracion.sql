DELIMITER $$
DROP TRIGGER IF EXISTS `sispol`.`sp_configuracion_insert`$$
#log de insertados(Nuevos registros)
DELIMITER $$
CREATE
TRIGGER `sispol`.`sp_configuracion_insert`
AFTER INSERT ON `sispol`.`sp_configuracion`
FOR EACH ROW
BEGIN
    SET @newq = CONCAT (
						' ID :',ifnull(new.idconfiguracion,''),',',
						' Nombre :',ifnull(new.nombre,''),',',
						' Valor :',ifnull(new.valor,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, fecha)                
    VALUES (NOW(),"INSERT","sp_configuracion",new.usuario_creacion,@newq, NOW());
END$$

DELIMITER $$
DROP TRIGGER IF EXISTS `sispol`.`sp_configuracion_update`$$
#log de insertados(Nuevos registros sp_personal)
DELIMITER $$
USE `sispol`$$
CREATE
TRIGGER `sispol`.`sp_configuracion_update`
BEFORE UPDATE ON `sispol`.`sp_configuracion`
FOR EACH ROW
BEGIN
    SET @oldq = CONCAT (
    					' ID :',ifnull(old.idconfiguracion,''),',',
						' Nombre :',ifnull(old.nombre,''),',',
						' Valor :',ifnull(old.valor,'')
						);
		SET @newq = CONCAT (
						' ID :',ifnull(new.idconfiguracion,''),',',
						' Nombre :',ifnull(new.nombre,''),',',
						' Valor :',ifnull(new.valor,'')
						);

	INSERT INTO sispol.sp_auditoria (fechahora,accion,tabla,idusuario, informacion_new, informacion_old, fecha)                
    VALUES (NOW(),"UPDATE","sp_configuracion",new.usuario_modificacion,@newq, @oldq, NOW());
END$$
