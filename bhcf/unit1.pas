unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, DBTables, ADODB, Grids, DBGrids, ComCtrls, FileCtrl;

type
  TBiometrico = class(TForm)
    Button1: TButton;
    Panel1: TPanel;
    ComboBox1: TComboBox;
    Button2: TButton;
    Button3: TButton;
    Image1: TImage;
    ADOConnection1: TADOConnection;
    ADOQuery1: TADOQuery;
    ADOQuery2: TADOQuery;
    ADOQuery3: TADOQuery;
    ADOQuery4: TADOQuery;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
   
  private
    { Private declarations }
     Procedure ParaRefresh;
     Procedure EnabledCapturar;
  public
    { Public declarations }
  end;

var
  //variables de llamado de formulario y funciones del biometrico
  Biometrico: TBiometrico;
  AvzOpen:Integer;
  AvzProces:Integer;
  Hwnd:THandle;
  AvzFind:SmallInt;
  AppPath:String;
  tmpS:String;
  AvzFindValid: Integer;
  FingerOn:SmallInt;

implementation
  uses AvzScanner;
{$R *.dfm}

Procedure TBiometrico.ParaRefresh;
var m_brightness,m_contrast,m_hue,m_saturation,m_sharpness,m_gama,m_wbalance,m_exposure:Word;
begin
  m_brightness := 0;
	m_contrast := 50;
	m_hue := 0;
	m_saturation := 0;
	m_sharpness := 0;
	m_gama := 30;
	m_wbalance := 15;
	m_exposure := 30;

	AvzSetParm(ComboBox1.ItemIndex,
					m_brightness, m_contrast,
					m_hue, m_saturation,
					m_sharpness, m_gama,
					m_wbalance, m_exposure);
end;

Procedure TBiometrico.EnabledCapturar;

begin
  Button3.Enabled:=True;
end;

procedure TBiometrico.Button1Click(Sender: TObject);
var P:ScannerName;
    I,J:Integer;
begin
    
    ComboBox1.Items.Clear;
    FillChar(P,Sizeof(P),Char(0));
    AvzFind:=AvzFindDevice(P);
    AvzFindValid:=0;
    for I:=0 to AvzFind-1 do
    begin
        tmpS:='';
        for J:=0 to 127 do
        begin
            if P[I,J]=Char(0) then Break
            else tmpS:=tmpS+P[I,J];
        end;
    end;
    if tmpS='' then
       begin
        AvzFindValid:=AvzFindValid+1;
        Showmessage('No se detecto el biometrico');
      end
    else
       begin
        ComboBox1.Items.Add(tmpS);
        Showmessage('Biometrico: '+tmpS);
      end;
end;

procedure TBiometrico.Button2Click(Sender: TObject);
begin
      if (ComboBox1.ItemIndex<>-1) then
       begin
          Hwnd:=Panel1.Handle;
          AvzOpen:=AvzOpenDevice(ComboBox1.ItemIndex,Hwnd);
          if AvzOpen=-1 then Showmessage('No se detecto el biometrico')
          else begin
            ComboBox1.Enabled:=False;
            Button1.Enabled:=False;
            gbValidScanner:=True;
            AvzGetImage(ComboBox1.ItemIndex,ImageProc,FingerOn);
              if FingerOn=0 then
                begin
                  Showmessage('Biometrico iniciado correctamente, presione capturar huellas');
                  Button2.Enabled:=False;
                  Button3.Enabled:=True;
              end;
          end;
      end
     else
       begin
       Showmessage('Seleccione el biometrico detectedo');
      end;
end;
procedure TBiometrico.Button3Click(Sender: TObject);
var ret:Integer;
    FileName:String;
    FingerOn:SmallInt;
    Fs:TFileStream;
    //nuevo
    FileNameAvn:String;
    Funcionario:Integer;
    Dir:String;
    I:Integer;
    SFile,TempF,TempPath:String;
    FileSearch: TSearchRec;
    FileAttrs: Integer;
    STime,ETime:DWord;
begin
    AvzGetImage(ComboBox1.ItemIndex,ImageProc,FingerOn);
    if FingerOn=0 then
      begin
        Showmessage('No hay huellas en el biometrico');
        Button2.Enabled:=False;
      end
    else
      begin
        AvzProces := AvzProcess(ImageProc,pFeature,ImageBin,TRUE,TRUE);
        if AvzProces=0 then
          begin
           Dir:='C:\xampp\htdocs\sispol\bhcf\Huellas';
            //if not SelectDirectory('Folder:','',Dir) then Exit;
            pFeatureNum:=0;
            FileAttrs:=faArchive;
            TempF:=Dir+'\*.anv';
            TempPath:=Dir+'\';

            if FindFirst(TempF, FileAttrs, FileSearch) = 0 then
              begin
              sFile:=TempPath+Filesearch.Name;
              if AvzLoadFeature(Pchar(sFile),pFeatureA,pFeatureB) then
                begin
                  for I:=0 to 255 do pFeatureLib1[pFeatureNum,I]:=pFeatureA[I];
                  for I:=0 to 255 do pFeatureLib2[pFeatureNum,I]:=pFeatureB[I];

                  PFileName[pFeatureNum]:=Filesearch.Name;
                  Inc(pFeatureNum);
                end;
              while FindNext(FileSearch) = 0 do
                  begin
                    Application.ProcessMessages;
                    Sleep(1);
                    sFile:=TempPath+Filesearch.Name;
                      if AvzLoadFeature(PChar(sFile),pFeatureA,pFeatureB) then
                        begin
                          if pFeatureNum=10000 then
                            begin
                            FindClose(FileSearch);
                            Break;
                          end;
                      for I:=0 to 255 do pFeatureLib1[pFeatureNum,I]:=pFeatureA[I];
                      for I:=0 to 255 do pFeatureLib2[pFeatureNum,I]:=pFeatureB[I];

                      PFileName[pFeatureNum]:=Filesearch.Name;
                      Inc(pFeatureNum);
                end;
            end;
              FindClose(FileSearch);
          end;

          STime:=GetTickCount;
          ret:=AvzMatchN(pFeature,pFeatureLib1,pFeatureNum,3);
          ETime:=GetTickCount;
          if ret<0 then
            begin
              STime:=GetTickCount;
              ret:=AvzMatchN(pFeature,pFeatureLib2,pFeatureNum,3);
              ETime:=GetTickCount;
          end;

              if ret>=0 then
                begin
                  Showmessage('La Huella ya Existe');
                end
              else
                begin
                  FileName:='C:\xampp\htdocs\sispol\biometrico.bmp';
                  AvzSaveHueBMPFile(Pchar(FileName),ImageProc);
                  Image1.Picture.LoadFromFile(FileName);
                  
                  //logica de la BD  y Comparacion para las Huellas
                  ADOQuery2.Close;
                  ADOQuery2.Active := true;
                  ADOQuery2.Close;
                  ADOQuery2.SQL.Clear;
                  ADOQuery2.SQL.Add('SELECT '+
                                'huella, '+
                                'idpersonal '+
                                'FROM sp_biometrico_temp '+
                                'WHERE proceso = "capturar"');
                  ADOQuery2.ExecSQL;
                  ADOQuery2.Open;
                  FileNameAvn :=ADOQuery2.FieldByName('huella').AsString;
                  Funcionario :=ADOQuery2.FieldByName('idpersonal').AsInteger;

                  ret:=AvzPackFeature(pFeature,pFeature,pFeatureBuf);
                  Fs :=TFileStream.Create('C:\xampp\htdocs\sispol\bhcf\Huellas\'+FileNameAvn+'.anv',fmCreate or fmShareDenyNone);
                  Fs.Seek(0,soFromBeginning);
                  Fs.WriteBuffer(pFeatureBuf,ret);
                  Fs.Destroy;
                  //Insert en Personal Biometrico
                  ADOQuery3.Active := true;
                  ADOQuery3.Close;
                  ADOQuery3.SQL.Clear;
                  ADOQuery3.SQL.Add('INSERT INTO sp_personal_biometrico (idpersonal, huella) VALUES ("'+inttostr(Funcionario)+'","'+FileNameAvn+'")');
                  ADOQuery3.ExecSQL;

                  ADOQuery4.Active := true;
                  ADOQuery4.Close;
                  ADOQuery4.SQL.Clear;
                  ADOQuery4.SQL.Add('TRUNCATE TABLE sp_biometrico_temp');
                  ADOQuery4.ExecSQL;

                  Showmessage('Captura de Huella Exitosamente, refresque su navegador');
                  Button3.Enabled:=False;
                  self.Close;
                  
                end;
              
          end
          else
            begin
              Showmessage('Verifique, el sensor del biometrico. Vuelva intentar');
            end;
    end;
end;

end.
