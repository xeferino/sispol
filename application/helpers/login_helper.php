<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

   /* function Verify_inicio(){
        
        $ci =& get_instance();
        $ci->load->database();
        $ci->load->library('session');

        if($ci->session->userdata("usuario") != ""){
            header('Location: '.base_url()."index.php/dashboard");
        }
    }

    function Verify_sistema(){
        
        $ci =& get_instance();
        $ci->load->database();
        $ci->load->library('session');

        if($ci->session->userdata("usuario") == ""){
            header('Location: '.base_url());
        }
    }*/

    function loginCheck($check = NULL){
    	/*$user = $ci->session->userdata("logueado");
    	if ($check == "Auth" && isset($user)) {
    		redirect(base_url()."index.php/dashboard");
    	}*/
    	$ci =& get_instance();
        $ci->load->database();
        $ci->load->library('session');
    	if (!$ci->session->userdata("logueado") && !$ci->session->userdata("usuario") 
            && !$ci->session->userdata("tipo") && !$ci->session->userdata("idusuario")) 
	    {
	       redirect(base_url()); //ruta a la que queremis que nos redireccione
	    }
    }

    function systemCheck(){
    	$ci =& get_instance();
        $ci->load->library('session');
        $url = base_url();
    	if ($url == 'http://localhost/sispol/') 
	    {
	       systemModules(); //ruta a la que queremis que nos redireccione
	    }
    }

    function systemModules()
    {
        
        $ci =& get_instance();
        $ci->load->library('session');
        $ci->load->model(array("usuarios_model", "configuracion_model"));
        $configuracion = $ci->configuracion_model->ObtenerConfiguracion();
        $idusuario = $ci->session->userdata("idusuario");
        if($configuracion["biometrico"]=="si"){
            if (isset($idusuario)) {
                $modules = $ci->usuarios_model->app_get_modulos($idusuario);
                $validar = 0;
                foreach ($modules as $key => $modulo){
                    if($ci->uri->segment(1) == $modulo->urlmodulo or $ci->uri->segment(1) == "notificaciones" or $ci->uri->segment(1) == "profile"  or $ci->uri->segment(1) == "huellas" )
                    {
                        $validar ++;
                    }
                }
                if ($validar==0) {
                    if ($modules[0]->urlmodulo=="dashboard") {
                       redirect(base_url()."dashboard");
                    }else{
                        redirect(base_url().$modules[0]->urlmodulo);
                    }
                }
            }
        }else{
            if (isset($idusuario)) {
                $modules = $ci->usuarios_model->app_get_modulos($idusuario);
                $validar = 0;
                foreach ($modules as $key => $modulo){
                    if($ci->uri->segment(1) == $modulo->urlmodulo or $ci->uri->segment(1) == "notificaciones" or $ci->uri->segment(1) == "profile")
                    {
                        $validar ++;
                    }
                }
                if ($validar==0) {
                    if ($modules[0]->urlmodulo=="dashboard") {
                       redirect(base_url()."dashboard");
                    }else{
                        redirect(base_url().$modules[0]->urlmodulo);
                    }
                }
            }
        }
    }
?>