<?php
    class Cron_model extends CI_Model {

        public function retrasoArmasPersonal()
        {
            $get = $this->db->query("SELECT
                                        idpersonalarmamento AS id,
                                        TIMEDIFF(
                                        NOW(),
                                        concat_ws(
                                            ' ',
                                            fechadevolucion,
                                            horadevolucion
                                        )
                                    ) AS tiempoRetraso
                                    FROM
                                        sp_personal_armamentos
                                    WHERE estatusarma = 'Pendiente' OR estatusarma = 'Vencido'
                                ");
             $restrasos = $get->result();

            foreach ($restrasos as $data) {
                if ($data->tiempoRetraso>="0") {
                    $info['estatusarma'] = 'Vencido';
                    $info['tiempoRetraso'] = $data->tiempoRetraso;
                    $info['usuario_modificacion'] = '1';
                    $this->db->where("idpersonalarmamento", $data->id);
                    $update = $this->db->update("sp_personal_armamentos",  $info);
                }
            }
        }

        public function notificacionArmasPersonal()
        {
            $get = $this->db->query("SELECT * FROM sp_personal_armamentos AS pa
                                    INNER JOIN sp_personal AS p ON p.idpersonal = pa.idpersonal
                                    WHERE pa.estatusarma = 'Pendiente' OR pa.estatusarma = 'Vencido'
                                    ");
            $validar = $get->num_rows();
            if ($validar>0) {
               return $get->result();
            }else{
                return $validar;
            }
        }

        public function topNotifiArmasPendientes()
        {
            $get = $this->db->query("SELECT
                                      *
                                    FROM
                                        sp_personal_armamentos AS pa
                                    INNER JOIN sp_personal AS p ON p.idpersonal = pa.idpersonal
                                    WHERE pa.estatusarma = 'Vencido'
                                    ORDER BY
                                        p.idpersonal
                                    LIMIT 0, 10
                                    ");
            $validar = $get->num_rows();

             if($validar>0){
                return $get->result();
             }else{
                return $validar;
             }
        } 
    }