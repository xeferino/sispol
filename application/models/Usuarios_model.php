<?php
date_default_timezone_set('America/Caracas');
class Usuarios_model extends CI_Model {

    /**
     * Tabla
     *
     * @var string
     */
    protected $tabla = 'sp_usuarios';

	/**
	 * Constructor.
	 *
	 * Cargo las clases necesarias
	 */
    public function __construct()
    {
        
    }

     /**
     * Obtener registros de la tabla usuario login.
     *
     * @param Integer
     */
    public function login($data)
    {
        if ($data)
        {
            $this->db->select("idusuario, nombreusuario, claveusuario, tipo, estatususuario");
            $this->db->from($this->tabla);
            //var_dump(md5($data['usuario'])); die();
            $this->db->where("nombreusuario", $data['usuario']);
            $this->db->where("claveusuario", md5($data['clave']));
            $res = $this->db->get();
            $usuario = $res->row();
            return $usuario;
        }
    }

    public function accessLogin($proccess)
    {
        $id = $this->session->userdata("idusuario"); 
        if ($proccess=="login")
        {      
            $get = $this->db->query("UPDATE sp_usuarios SET usuario_modificacion = ".$id.", login = NOW() 
                                     WHERE idusuario = ".$id."
                                ");

        }elseif($proccess=="logout"){
            $get = $this->db->query("UPDATE sp_usuarios SET usuario_modificacion = ".$id.", last_login = NOW() 
                                     WHERE idusuario = ".$id."
                                ");
        }elseif($proccess=="connection"){
            
            $this->db->select("*, 
                                CONCAT_WS(' ',ABS(TIMESTAMPDIFF(DAY,   NOW(), last_login)),
                                'Dias', ABS(LEFT(TIMESTAMPDIFF(HOUR,   NOW(), last_login),2)),
                                'Horas', ABS(LEFT(TIMESTAMPDIFF(MINUTE,   NOW(), last_login),2)),
                                'Minutos', ABS(LEFT(TIMESTAMPDIFF(SECOND,   NOW(), last_login),2)),
                                'Segundos') AS tiempoAcceso");
            $usuario = $this->db->from($this->tabla)->get()->result();
            return $usuario;
        }
    }

	/**
	 * Obtener registros de la tabla.
	 *
	 * @param Integer
	 */
    public function get($id = NULL)
    {
    	if ($id)
    	{
    		$this->db->where(['idusuario' => $id]);
    	}
        $query = $this->db->get($this->tabla);

    	if ($id)
    	{
    		return $query->row();
    	}
    	else
    	{
    		return $query->result();
    	}
    }


    /**
     * Obtener registros de la tabla JSON.
     *
     * @param Integer
     */
    public function allUsers()
    {
        $this->db->from($this->tabla);
        $res = $this->db->get();
        $usuarios = $res->result();

        $data = array();
        foreach ($usuarios as $usuario) {
            # code...
            //$row[] = $usuarios;
            $row[0] = $usuario->idusuario;
            $row[1] = $usuario->nombres;
            $row[2] = $usuario->apellidos;
            $row[3] = $usuario->cedula;
            $row[4] = $usuario->email;
            $row[5] = $usuario->nombreusuario;
            $row[6] = $usuario->estatususuario;
            $row[7] = $usuario->tipo;
            $row[8] = $usuario->pregunta;
            $row[9] = $usuario->respuesta;

            array_push($data, $row);
        }
            $results = [ "iTotalRecords" => count($data),
                         "iTotalDisplayRecords" => count($data),
                         "aaData"=>$data 
                       ];
        return json_encode($results);
    }

   
    /**
     * Obtener registros de la tabla.
     *
     * @param Integer   Carga los modulos de la aplicacion
     * @param Array     Parámetros de búsqueda
     * @param Integer   Limit de la consulta
     * @return Array
     */
    
    public function app_get_modulos($idusuario)
    {
        $this->db->from("sp_view_modulos_usuarios");
        $this->db->where("idusuario", $idusuario);
        $res = $this->db->get();
        $modulo = $res->result();
        return $modulo;
    }

	/**
	 * Agregar Usuario
	 *
     * @param Array     Parámetros de búsqueda
	 * @return Array
	 */

    public function insertUser() {
        
        $modulosUser = $this->input->post('modulosUser');
        
        if($modulosUser==NULL){
            return "modulo";
        }else{

            $this->db->select('cedula, documento');
            $this->db->where("documento", $this->input->post("documento"));
            $this->db->where("cedula", $this->input->post("cedula"));
            $cedula = $this->db->from($this->tabla)->get()->result();

            $this->db->select('email');
            $this->db->where("email", $this->input->post("email"));
            $email = $this->db->from($this->tabla)->get()->result();

            $this->db->select('nombreusuario');
            $this->db->where("nombreusuario", $this->input->post("usuario"));
            $nombreusuario = $this->db->from($this->tabla)->get()->result();

            if(count($cedula)>0){
                return "cedula::".$this->input->post("cedula")."::".$this->input->post("documento");
            }elseif (count($email)>0){
                return "email::".$this->input->post("email");
            }elseif (count($nombreusuario)>0){
                return "nombreusuario::".$this->input->post("usuario");
            }else{
                
                $dataUser = array(  "nombres"               => $this->input->post("nombre"),
                                    "apellidos"             => $this->input->post("apellido"),
                                    "cedula"                => $this->input->post("cedula"),
                                    "documento"             => $this->input->post("documento"),
                                    "email"                 => $this->input->post("email"),
                                    "nombreusuario"         => $this->input->post("usuario"),
                                    "claveusuario"          => md5($this->input->post("clave")),
                                    "tipo"                  => $this->input->post("tipo"),
                                    "pregunta"              => $this->input->post("pregunta"),
                                    "respuesta"             => $this->input->post("respuesta"),
                                    "usuario_creacion"      => $this->session->userdata("idusuario")
                            );
                    
                $insert = $this->db->insert($this->tabla, $dataUser);
                $id = $this->db->insert_id();
                if($insert) {
                    ##modulos##
                    if (count($modulosUser))
                    {

                        $privilegios = NULL;
                        foreach ($modulosUser as $idmodulo)
                        {
                            $privilegios[] = [
                                'idusuario' => $id,
                                'idmodulo' => $idmodulo,
                                'tipo' => 'sp_modulo',
                                'usuario_creacion' =>  $this->session->userdata("idusuario"),
                            ];
                        }

                        $this->db->insert_batch('sp_modulos_usuarios', $privilegios);
                    }
                    return "exito";    
                }else{
                    return "error";
                }
            }
        }
    }

    /**
     * Agregar Usuario
     *
     * @param Array     Parámetros de búsqueda
     * @return Array
     */

    public function updateUser() {
        
        $modulosUser = $this->input->post('modulosUser');
        $conectado = $this->input->post('conectado');        
        if ($conectado=="analista sistema") {
            if ($modulosUser==null) {
                return "modulo";
            } else {
                $this->db->where("cedula", $this->input->post("cedula"));
                $this->db->where("documento", $this->input->post("documento"));
                $this->db->where("idusuario !=", $this->input->post("idusuario"));
                $cedula = $this->db->from($this->tabla)->get()->result();
    
                $this->db->where("email", $this->input->post("email"));
                $this->db->where("idusuario !=", $this->input->post("idusuario"));
                $email = $this->db->from($this->tabla)->get()->result();
    
                $this->db->where("nombreusuario", $this->input->post("usuario"));
                $this->db->where("idusuario !=", $this->input->post("idusuario"));
                $nombreusuario = $this->db->from($this->tabla)->get()->result();
    
                if (count($cedula)>0) {
                    return "cedula::".$this->input->post("cedula")."::".$this->input->post("documento");
                } elseif (count($email)>0) {
                    return "email::".$this->input->post("email");
                } elseif (count($nombreusuario)>0) {
                    return "nombreusuario::".$this->input->post("usuario");
                } else {
                    $dataUser = array(  "nombres"               => $this->input->post("nombre"),
                                    "apellidos"                 => $this->input->post("apellido"),
                                    "cedula"                    => $this->input->post("cedula"),
                                    "documento"                 => $this->input->post("documento"),
                                    "email"                     => $this->input->post("email"),
                                    "nombreusuario"             => $this->input->post("usuario"),
                                    "claveusuario"              => md5($this->input->post("clave")),
                                    "tipo"                      => $this->input->post("tipo"),
                                    "pregunta"                  => $this->input->post("pregunta"),
                                    "respuesta"                 => $this->input->post("respuesta"),
                                    "usuario_modificacion"      => $this->session->userdata("idusuario")
                            );
                        
                    $this->db->where("idusuario", $this->input->post("idusuario"));
                    $update = $this->db->update($this->tabla, $dataUser);
                    
                    if ($update) {
                        $delete = $this->db->delete('sp_modulos_usuarios', ['idusuario' => $this->input->post("idusuario")]);
                        if ($delete) {
                            ##modulos##
                            if (count($modulosUser)) {
                                $privilegios = null;
                                foreach ($modulosUser as $idmodulo) {
                                    $privilegios[] = [
                                        'idusuario' =>  $this->input->post("idusuario"),
                                        'idmodulo' => $idmodulo,
                                        'tipo' => 'sp_modulo',
                                        'usuario_creacion' =>  $this->session->userdata("idusuario"),
                                    ];
                                }
    
                                $this->db->insert_batch('sp_modulos_usuarios', $privilegios);
                            }
                            return "exito";
                        } else {
                            return "error";
                        }
                    }
                }
            }
        }else{
                
            $this->db->where("cedula", $this->input->post("cedula"));
            $this->db->where("documento", $this->input->post("documento"));
            $this->db->where("idusuario !=", $this->input->post("idusuario"));
            $cedula = $this->db->from($this->tabla)->get()->result();

            $this->db->where("email",$this->input->post("email"));
            $this->db->where("idusuario !=", $this->input->post("idusuario"));
            $email = $this->db->from($this->tabla)->get()->result();

            $this->db->where("nombreusuario", $this->input->post("usuario"));
            $this->db->where("idusuario !=", $this->input->post("idusuario"));
            $nombreusuario = $this->db->from($this->tabla)->get()->result();

            if(count($cedula)>0){
                return "cedula::".$this->input->post("cedula")."::".$this->input->post("documento");
            }elseif (count($email)>0){
                return "email::".$this->input->post("email");
            }elseif (count($nombreusuario)>0){
                return "nombreusuario::".$this->input->post("usuario");
            }else{
                
                $dataUser = array(  "nombres"                   => $this->input->post("nombre"),
                                    "apellidos"                 => $this->input->post("apellido"),
                                    "cedula"                    => $this->input->post("cedula"),
                                    "documento"                 => $this->input->post("documento"),
                                    "email"                     => $this->input->post("email"),
                                    "nombreusuario"             => $this->input->post("usuario"),
                                    "claveusuario"              => md5($this->input->post("clave")),
                                    "tipo"                      => $this->input->post("tipo"),
                                    "pregunta"                  => $this->input->post("pregunta"),
                                    "respuesta"                 => $this->input->post("respuesta"),
                                    "usuario_modificacion"      => $this->session->userdata("idusuario")
                            );
                    
                $this->db->where("idusuario", $this->input->post("idusuario"));
                $update = $this->db->update($this->tabla, $dataUser);
                
                if($update) {
                    return "exito";    
                }else{
                    return "error";
                }
            }
        }
        
    }

     /**
     * Agregar Usuario
     *
     * @param Array     Parámetros de búsqueda
     * @return Array
     */

    public function updatePerfil() {
        if ($this->input->post("clave") == $this->input->post("claver")) {
            if(filter_var($this->input->post("email"), FILTER_VALIDATE_EMAIL)){
                $this->db->where("cedula", $this->input->post("cedula"));
                $this->db->where("documento", $this->input->post("documento"));
                $this->db->where("idusuario !=", $this->input->post("idusuario"));
                $cedula = $this->db->from($this->tabla)->get()->result();
        
                $this->db->where("email", $this->input->post("email"));
                $this->db->where("idusuario !=", $this->input->post("idusuario"));
                $email = $this->db->from($this->tabla)->get()->result();
        
                $this->db->where("nombreusuario", $this->input->post("usuario"));
                $this->db->where("idusuario !=", $this->input->post("idusuario"));
                $nombreusuario = $this->db->from($this->tabla)->get()->result();
        
                if(count($cedula)>0){
                    return "cedula";
                }elseif (count($email)>0){
                    return "email";
                }elseif (count($nombreusuario)>0){
                    return "usuario";
                }else{
                    
                    $dataUser = array(  "nombres"                   => $this->input->post("nombre"),
                                        "apellidos"                 => $this->input->post("apellido"),
                                        "cedula"                    => $this->input->post("cedula"),
                                        "documento"                 => $this->input->post("documento"),
                                        "email"                     => $this->input->post("email"),
                                        "nombreusuario"             => $this->input->post("usuario"),
                                        "claveusuario"              => md5($this->input->post("clave")),
                                        "tipo"                      => $this->input->post("tipo"),
                                        "pregunta"                  => $this->input->post("pregunta"),
                                        "respuesta"                 => $this->input->post("respuesta"),
                                        "usuario_modificacion"      => $this->session->userdata("idusuario")
                                );
                        
                    $this->db->where("idusuario", $this->input->post("idusuario"));
                    $update = $this->db->update($this->tabla, $dataUser);
                    if($update) {
                        return "exito";    
                    }else{
                        return "error";
                    }
                }
            }else {
                return "correo";
            }
        }else {
           return "clave";
        }
    }

    /**
     * Actualizar Estatus de Usuario.
     *
     * @param Integer   Carga los modulos de la aplicacion
     * @param Array     Parámetros de búsqueda
     * @param Integer   Limit de la consulta
     * @return Array
     */
    
    public function updateEstatus($idusuario)
    {
        $usuario = $this->get($idusuario);
        $estatus = ($usuario->estatususuario=="0") ? '1' : '0' ;
        $data['estatususuario'] = $estatus;
        $data['usuario_modificacion'] = $this->session->userdata("idusuario");

        $this->db->where("idusuario", $idusuario);
        $update = $this->db->update($this->tabla, $data);

        if ($update) {
            return "exito";
        }else{
            return "error";
        }
    }

    /**
     * Actualizar Estatus de Usuario.
     *
     * @param Integer   Carga los modulos de la aplicacion
     * @param Array     Parámetros de búsqueda
     * @param Integer   Limit de la consulta
     * @return Array
     */
    
    public function updatepassword($datos)
    {
       
        $this->db->where("email", $datos['email']);
        $this->db->where("pregunta", $datos['pregunta']);
        $this->db->where("respuesta", $datos['respuesta']);
        $informacion = $this->db->from($this->tabla)->get()->result();

        if ($informacion){
            $data['claveusuario'] = md5($datos['clave']);
            $data['usuario_modificacion'] = $this->session->userdata("idusuario");
            $this->db->where("email", $datos['email']);
            $update = $this->db->update($this->tabla, $data);
            if ($update) {
                return "exito";
            }else{
                return "error";
            }
        }else {
            return "info";
        }
    }

         

    /**
     * Nombre: __destruct
     * Descripcion: destructor para garantizar cierre de conexion de bd
     * Autor: Jose Lozada
     * Fecha: 25/09/2019
     */
    public function __destruct(){
        $this->db->close();
    }
}
