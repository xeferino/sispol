<?php
    class Huellas_model extends CI_Model {

        public function getHuellasPersonal($idpersonal = NULL, $proceso)
        {
            if($proceso=="1"){
                $get = $this->db->query("SELECT
                                            idpersonal,
                                            documento,
                                            cedula
                                        FROM
                                            sp_personal
                                        WHERE idpersonal = '".$idpersonal."'
                                    ");
                $validar = $get->num_rows();

                if(count($validar)>0){
                    $funcionario = $get->row();
                    $datos = array( "idpersonal" => $idpersonal,
                                    "huella" => $funcionario->documento."".$funcionario->cedula,
                                    "proceso" => "capturar"
                    );
                    $insert = $this->db->insert("sp_biometrico_temp", $datos);
                    if ($insert) {
                        return "exito";
                    } else {
                        return "error";
                    }
                }else{
                    return $validar;
                }
            }elseif($proceso=="2"){
                $this->db->from("sp_biometrico_temp");
                $res = $this->db->get();
                $counter = $res->num_rows();
                if(count($counter)<1){
                    return "exito";
                }
            }
        }

        public function getHuellasPersonalArmamentos($idpersonal = NULL, $proceso)
        {
            if($proceso=="1"){
                $get = $this->db->query("SELECT
                                      idpersonal,
                                      documento,
                                      cedula
                                    FROM
                                        sp_personal
                                    WHERE idpersonal = '".$idpersonal."'
                                    ");
                $validar = $get->num_rows();

                if($validar>0){
                    $funcionario = $get->row();
                    $datos = array( "idpersonal" => $idpersonal,
                                    "huella" => $funcionario->documento."".$funcionario->cedula, 
                                    "proceso" => 'capturar'
                                );
                    //$datos = array("idpersonal" => $idpersonal);
                    $insert = $this->db->insert("sp_biometrico_temp", $datos);
                    if ($insert) {
                        return "exito";
                    } else {
                        return "error";
                    }
                }else{
                    return $validar;
                }
            }elseif($proceso=="2"){
                $this->db->select("idpersonal, huella");
                $this->db->from("sp_biometrico_temp");
                $this->db->where("id", "1");
                $this->db->where("proceso", "capturar");
                $res = $this->db->get();
                $temp = $res->row();
                if($temp->idpersonal!="" && $temp->huella!="") {
                    return "exito::".$temp->idpersonal;
                }
            }elseif($proceso=="3"){
                $this->db->from('sp_biometrico_temp');
                $this->db->truncate();
            }
        }

        public function getHuellas($idpersonal=NULL){
           
            if($idpersonal){
                $this->db->select("idpersonal");
                $this->db->from("sp_personal_biometrico");
                $this->db->where("idpersonal", $idpersonal);
                $res = $this->db->get();
                $huella = $res->row();
                return $huella; 
            }else{
                $get = $this->db->query("SELECT
                                        p.idpersonal,
                                        p.documento,
                                        p.cedula,
                                        p.nombres,
                                        p.apellidos,
                                        p.placa,
                                        p.rango,
                                        p.direccion,
                                        p.telefono_habitacion,
                                        p.telefono_celular,
                                        pb.huella,
                                        p.estatus
                                    FROM
                                        sp_personal p
                                    LEFT JOIN sp_personal_biometrico pb ON pb.idpersonal = p.idpersonal"
                );
                return $get->result();
            }
        }

        public function getHuellaDelete($idpersonal){

            $get = $this->db->query("SELECT
                                      idpersonal,
                                      documento,
                                      cedula
                                    FROM
                                        sp_personal
                                    WHERE idpersonal = '".$idpersonal."'
                                    ");
            $row = $get->row();
            $ruta = APPPATH."..\bhcf\Huellas\\".$row->documento."".$row->cedula.".anv";
            $delete = unlink($ruta);
            $delete = $this->db->delete('sp_personal_biometrico', ['idpersonal' => $idpersonal]);
            if ($delete) {
                return "exito";
            }else {
                return "error";
            }
        }
    }