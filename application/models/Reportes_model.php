<?php

class Reportes_model extends CI_Model {

	/**
	 * Constructor.
	 *
	 * Cargo las clases necesarias
	 */
    public function __construct()
    {
        
    }

	public function reportUsuarios($desde = NULL, $hasta = NULL, $estatus = NULL)
    {
        $this->db->select("`idusuario`,
                            CONCAT(`documento`, '-', `cedula`) AS cedula,
                            CONCAT(`nombres`, ' ', `apellidos`) AS nombre,
                            `email`,
                            DATE(`fecha_creacion`) AS fecha,
                            UPPER(`tipo`) AS tipo,
                            `nombreusuario` AS usuario,
                            IF(estatususuario=1,'Bloqueado','Activo') AS estatus");
        $this->db->from("sp_usuarios");
        
        if ($desde!="" && $hasta!="")
        {
            $this->db->where("DATE(fecha_creacion)>=", $desde);
            $this->db->where("DATE(fecha_creacion)<=", $hasta);
        }
        if ($estatus!="todos") {
            $this->db->where("estatususuario", $estatus);
        }

        $res = $this->db->get();
        $validar = $res->num_rows();
        if ($validar>0) {
            $usuarios = $res->result();
            return $usuarios;
        }else{
            return $validar;
        }
    }


    public function reportPersonal($desde = NULL, $hasta = NULL, $estatus = NULL, $rango = NULL)
    {
        $this->db->select("`idpersonal`,
                            CONCAT(`documento`, '-', `cedula`) AS cedula,
                            CONCAT(`nombres`, ' ', `apellidos`) AS nombre,
                            `placa`,
                            DATE(`fecha_creacion`) AS fecha,
                            UPPER(`rango`) AS tipo,
                            CONCAT(`telefono_habitacion`, '-', `telefono_celular`) AS tlf,
                            `direccion`,
                            IF(estatus=1,'Bloqueado','Activo') AS estatus");
        $this->db->from("sp_personal");
        
        if ($desde!="" && $hasta!="")
        {
            $this->db->where("DATE(fecha_creacion)>=", $desde);
            $this->db->where("DATE(fecha_creacion)<=", $hasta);
        }
        if ($estatus!="todos") {
            $this->db->where("estatus", $estatus);
        }
        if ($rango!="todos") {
            $this->db->where("rango", $rango);
        }

        $res = $this->db->get();
        $validar = $res->num_rows();
        if ($validar>0) {
            $personal = $res->result();
            return $personal;
        }else{
            return $validar;
        }
    }

    public function reportArmamentos($desde = NULL, $hasta = NULL)
    {
        $this->db->select(" idarma,
                            codigo,
                            UPPER(`tipo`) AS tipo,
                            calibre,
                            descripcion,
                            cantidad,
                            cantidad_asignada,
                            (cantidad-cantidad_asignada) AS disponible,
                            DATE(`fecha_creacion`) AS fecha,
                        ");
        $this->db->from("sp_armamentos");
        
        if ($desde!="" && $hasta!="")
        {
            $this->db->where("DATE(fecha_creacion)>=", $desde);
            $this->db->where("DATE(fecha_creacion)<=", $hasta);
        }
        
        $res = $this->db->get();
        $validar = $res->num_rows();
        if ($validar>0) {
            $armas = $res->result();
            return $armas;
        }else{
            return $validar;
        }
    }

    public function reportMovimientosArmamentos($desde = NULL, $hasta = NULL, $estatus = NULL, $idpersonal = NULL)
    {
        $this->db->from("sp_personal_armamentos AS pa");
        $this->db->join("sp_personal AS p", "p.idpersonal = pa.idpersonal", "LEFT");
        
        if ($desde!="" && $hasta!="")
        {
            $this->db->where("DATE(pa.fechaasignacion)>=", $desde);
            $this->db->where("DATE(pa.fechadevolucion)<=", $hasta);
        }
        if ($estatus!="todos") {
            $this->db->where("estatusarma", $estatus);
        }
        if ($idpersonal!="todos") {
            $this->db->where("pa.idpersonal", $idpersonal);
        }
        $res = $this->db->get();
        $validar = $res->num_rows();
        if ($validar>0) {
            $movimientos = $res->result();
            return $movimientos;
        }else{
            return $validar;
        }
    }


    public function reportArmamentosMovimientos($desde = NULL, $hasta = NULL /*, $estatus = NULL */)
    {
        //var_dump( $this->input->post());die();
        $this->db->from("sp_personal_armamentos AS pa");
        $this->db->join("sp_personal AS p", "p.idpersonal = pa.idpersonal", "LEFT");
        
        if ($desde!="" && $hasta!="")
        {
            $this->db->where("DATE(pa.fechaasignacion)>=", $desde);
            $this->db->where("DATE(pa.fechadevolucion)<=", $hasta);
        }
        /* if ($estatus!="todos") {
            $this->db->where("estatusarma", $estatus);
        } */
        
        $res = $this->db->get();
        $validar = $res->num_rows();
        if ($validar>0) {
            $movimientos = $res->result();
            return $movimientos;
        }else{
            return $validar;
        }
    }

    public function reportEstadisticasMovimientos($desde = NULL, $hasta = NULL, $estatus = NULL, $idpersonal = NULL, $armas = NULL)
    {
        $this->db->from("sp_personal_armamentos AS pa");
        $this->db->join("sp_personal AS p", "p.idpersonal = pa.idpersonal", "INNER");
        $this->db->order_by("pa.idpersonalarmamento", "DESC");
        
        if ($desde!="" && $hasta!="")
        {
            $this->db->where("DATE(pa.fecha_creacion)>=", $desde);
            $this->db->where("DATE(pa.fecha_creacion)<=", $hasta);
        }
        if ($estatus!="todos") {
            $this->db->where("estatusarma", $estatus);
        }
        if ($idpersonal!="todos") {
            $this->db->where("pa.idpersonal", $idpersonal);
        }
        $res = $this->db->get();
        $validar = $res->num_rows();
        if ($validar>0) {
            $movimientos = $res->result();
            return $movimientos;
        }else{
            return $validar;
        }
    }

    /**
     * Nombre: __destruct
     * Descripcion: destructor para garantizar cierre de conexion de bd
     * Autor: Jose Lozada
     * Fecha: 24-09-2019
     */
    public function __destruct(){
        $this->db->close();
    }
}
