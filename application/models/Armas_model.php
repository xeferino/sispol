<?php

class Armas_model extends CI_Model {

    /**
     * Tabla
     *
     * @var string
     */
    protected $tabla = 'sp_armamentos';

	/**
	 * Constructor.
	 *
	 * Cargo las clases necesarias
	 */
    public function __construct()
    {
        
    }

	/**
	 * Obtener registros de la tabla.
	 *
	 * @param Integer
	 */
    public function get($id = NULL)
    {
    	if ($id)
    	{
    		$this->db->where(['idarma' => $id]);
    	}
        $query = $this->db->get($this->tabla);

    	if ($id)
    	{
    		return $query->row();
    	}
    	else
    	{
    		return $query->result();
    	}
    }


    /**
     * Obtener registros de la tabla.
     *
     * @param Integer
     */
    public function getPersonalArmamento($id = NULL)
    {
        if ($id)
        {
            $this->db->where(['idpersonal' => $id]);
        }
        $query = $this->db->get("sp_personal_armamentos");

        if ($id)
        {
            return $query->row();
        }
        else
        {
            return $query->result();
        }
    }


    /**
     * Obtener registros de la tabla.
     *
     * @param Integer
     */
    public function getArmamentosPersonal($estatus = NULL)
    {
        if ($estatus)
        {
            $this->db->where(['estatusarma' => $estatus]);
        }
        $query = $this->db->get("sp_personal_armamentos");
        return $query->result();
    }


	/**
	 * Agregar Arma
	 *
     * @param Array     Parámetros de búsqueda
	 * @return Array
	 */

    public function insertArma() {
        
        $this->db->select('codigo');
        $this->db->where("codigo", $this->input->post("codigo"));
        $codigo = $this->db->from($this->tabla)->get()->result();

        if(count($codigo)>0){
            return "codigo::".$this->input->post("codigo")."::".$this->input->post("codigo");
        }else{
            $datos = array( "codigo" => $this->input->post("codigo"),
                            "tipo" => $this->input->post("tipo"),
                            "calibre" => $this->input->post("calibre"),
                            "descripcion" => $this->input->post("descripcion"),
                            "cantidad" => $this->input->post("cantidad"),
                            "usuario_creacion" => $this->session->userdata("idusuario")
                    );
                
            $insert = $this->db->insert($this->tabla, $datos);
            if($insert) {
                return "exito";    
            }else{
                return "error";
            }
        }
    }

    /**
     * Agregar Arma
     *
     * @param Array     Parámetros de búsqueda
     * @return Array
     */

    public function updateArma() {
                
        $this->db->select('codigo');
        $this->db->where("codigo", $this->input->post("codigo"));
        $this->db->where("idarma !=", $this->input->post("idarma"));
        $codigo = $this->db->from($this->tabla)->get()->result();

        if(count($codigo)>0){
            return "codigo::".$this->input->post("codigo")."::".$this->input->post("codigo");
        }else{
            $datos = array( "codigo" => $this->input->post("codigo"),
                            "tipo" => $this->input->post("tipo"),
                            "calibre" => $this->input->post("calibre"),
                            "descripcion" => $this->input->post("descripcion"),
                            "cantidad" => $this->input->post("cantidad"),
                            "usuario_modificacion" => $this->session->userdata("idusuario")
                    );
                
            $this->db->where("idarma", $this->input->post("idarma"));
            $update = $this->db->update($this->tabla, $datos);
            if($update) {
                return "exito";    
            }else{
                return "error";
            }
        }
    }
    
    /**
     * Actualizar Estatus de Arma.
     *
     * @param Integer   Carga los modulos de la aplicacion
     * @param Array     Parámetros de búsqueda
     * @param Integer   Limit de la consulta
     * @return Array
     */
    
    public function updateEstatus($idpersonal)
    {
        $personal = $this->get($idpersonal);
        $estatus = ($personal->estatus=="0") ? '1' : '0' ;
        $data['estatus'] = $estatus;
        $data['usuario_modificacion'] = $this->session->userdata("idusuario");

        $this->db->where("idpersonal", $idpersonal);
        $update = $this->db->update($this->tabla, $data);

        if ($update) {
            return "exito";
        }else{
            return "error";
        }
    }

    public function getInfoArmasPersonal($idpersonal)
    {
         $get = $this->db->query("SELECT * FROM sp_personal_armamentos AS pa
                                    INNER JOIN sp_personal AS p ON p.idpersonal = pa.idpersonal
                                    WHERE (pa.estatusarma = 'Pendiente' OR pa.estatusarma = 'Vencido')
                                          AND pa.idpersonal = '".$idpersonal."'");
         return $get->row();
    }

    public function addArmasPersonal()
    {
        $validar = 0;
        $campo = 0;
        $disponibilidad = 0;

        foreach ($this->input->post('cantArmas') as $key => $cantArmas) {
            $armas = $this->armas_model->get($key);
            $disponible = $armas->cantidad-$armas->cantidad_asignada;

            if($cantArmas=="0" or $cantArmas=="") {
                $campo++;
            }else
            if ($cantArmas>$disponible) {
                $validar++;
            }else if($cantArmas<$disponible) {
                if ($disponible==1) {
                    $validar++;
                }
            }
        }

        if ($validar>0) {
            return "cantidad";
        }else if($campo>0) {
            return "campo";
        } 
        else{
            foreach ($this->input->post('cantArmas') as $key => $cantArmas) {
                $armas = $this->armas_model->get($key);
                $this->db->where("idarma", $key);
                $data['cantidad_asignada'] = $cantArmas+$armas->cantidad_asignada;
                $data['usuario_modificacion'] = $this->session->userdata("idusuario");
                $update = $this->db->update("sp_armamentos", $data);
            }

            $armamentos = implode(",", $this->input->post('armamentos'));
            $cantArmas =  implode(",", $this->input->post('cantArmas'));
            $tiempo = $this->input->post("diff").":00:00";

            $datos = array( "idpersonal" => $this->input->post("funcionario"),
                            "idarmas" => $armamentos,
                            "cantarmas" => $cantArmas,
                            "fechaasignacion" => $this->input->post("fecha_a"),
                            "horaasignacion" => $this->input->post("hora_a"),
                            "fechadevolucion" => $this->input->post("fecha_d"),
                            "horadevolucion" => $this->input->post("hora_d"),
                            "responsableasignacion" => $this->session->userdata("idusuario"),
                            "estatusarma" => "Pendiente",
                            "estatusasignacion" => "Asignadas",
                            "tiempoAsignacion" => $tiempo,
                            "usuario_creacion" => $this->session->userdata("idusuario")
                    );
            $insert = $this->db->insert("sp_personal_armamentos",  $datos);

            if ($insert) {
                return "exito"; 
            } else {
                return "error"; 
            }
            
        }
    }

    public function devolverArmasPersonal()
    {
        $get = $this->getInfoArmasPersonal($this->input->post('id'));

        $armamentos = explode(',', $get->idarmas);

        foreach ($armamentos as $keya => $idarma) {

            $armas = $this->armas_model->get($idarma);
            $cantidadArmas = explode(',', $get->cantarmas);

            foreach ($cantidadArmas as $keyc => $cantArmas) {

                if ($keya==$keyc) {
                    $this->db->where("idarma", $armas->idarma);
                    $data['cantidad_asignada'] = $armas->cantidad_asignada-$cantArmas;
                    $data['usuario_modificacion'] = $this->session->userdata("idusuario");
                }
                
            }
            $update = $this->db->update("sp_armamentos", $data);
        }

            $datos = array( "responsabledevolucion" => $this->session->userdata("idusuario"),
                            "estatusarma" => "Entregado",
                            "estatusasignacion" => "Devueltas",
                            "usuario_modificacion" => $this->session->userdata("idusuario")
                        );
            $this->db->where("idpersonalarmamento", $get->idpersonalarmamento);
            $update = $this->db->update("sp_personal_armamentos",  $datos);

            if ($update) {
                return "exito"; 
            } else {
                return "error"; 
            }
    }

    /**
     * Nombre: __destruct
     * Descripcion: destructor para garantizar cierre de conexion de bd
     * Autor: Jose Lozada
     * Fecha: 25/09/2019
     */
    public function __destruct(){
        $this->db->close();
    }
}
