<?php

class Dashboard_model extends CI_Model {

 	/**
	 * Constructor.
	 *
	 * Cargo las clases necesarias
	 */
    public function __construct()
    {
        
    }

	/**
	 * Obtener registros de la tabla.
	 *
	 */
    public function personalCount()
    {
        $personal = $this->db->from("sp_personal")->get()->result();
        return count($personal);
    }

    /**
     * Obtener registros de la tabla.
     *
     */
    public function usuariosCount()
    {
        $usuarios = $this->db->from("sp_usuarios")->get()->result();
        return count($usuarios);
    }

    /**
     * Obtener registros de la tabla.
     *
     */
    public function movimientosCount()
    {
        $movimientos = $this->db->from("sp_personal_armamentos")->get()->result();
        return count($movimientos);
    }

    /**
     * Obtener registros de la tabla.
     *
     */
    public function movimientosArmas()
    {
        $this->db->from("sp_personal_armamentos AS pa");
        $this->db->join("sp_personal AS p", "p.idpersonal = pa.idpersonal", "INNER");
        $this->db->order_by("pa.idpersonalarmamento", "DESC");
        $res = $this->db->get();
        $validar = $res->num_rows();
        if ($validar>0) {
            $rows = $res->result();
            return $rows;
        }
    }

    


    /**
     * Obtener registros de la tabla.
     *
     */
    public function armamentosCount()
    {
        $armamentos = $this->db->from("sp_armamentos")->get()->result();
        return count($armamentos);
    }


    /**
     * Obtener registros de la tabla.
     *
     */
    public function personal()
    {
        $personal = $this->db->from("sp_personal")->get()->result();
        return $personal;
    }

    /**
     * Obtener registros de la tabla.
     *
     */
    public function usuarios()
    {
        $usuarios = $this->db->from("sp_usuarios")->get()->result();
        return $usuarios;
    }

    /**
     * Obtener registros de la tabla.
     *
     */
    public function armamentos()
    {
        $armamentos = $this->db->from("sp_armamentos")->get()->result();
        return $armamentos;
    }


     public function movimientos()
    {
        $movimientos = $this->db->from("sp_personal_armamentos")->get()->result();
        return $movimientos;
    }

    /**
     * Obtener registros de la tabla.
     *
     */
    public function armamentosPersonal($estatus)
    {
        $where = "";
        if($estatus=="Pendiente"){
            $where ="WHERE estatusarma = 'Pendiente' OR estatusarma = 'Vencido'";
        }else{
            $where ="WHERE estatusarma = 'Entregado'";
        }
        $get = $this->db->query("SELECT
                                *
                                FROM
                                    sp_personal_armamentos
                                ".$where."
                                ORDER BY idpersonalarmamento DESC
                                LIMIT 0, 10");
                        
       return $get->result();
    }


   
    /**
     * Nombre: __destruct
     * Descripcion: destructor para garantizar cierre de conexion de bd
     * Autor: Jose Lozada
     * Fecha: 24-09-2019
     */
    public function __destruct(){
        $this->db->close();
    }
}
