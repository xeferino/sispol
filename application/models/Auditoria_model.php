<?php

class Auditoria_model extends CI_Model {

    /**
     * Tabla
     *
     * @var string
     */
    protected $tabla = 'sp_auditoria';

	/**
	 * Constructor.
	 *
	 * Cargo las clases necesarias
	 */
    public function __construct()
    {
        
    }

	/**
	 * Obtener registros de la tabla.
	 *
	 * @param Integer
	 */
    public function get($id = NULL)
    {
    	if ($id)
    	{
    		$this->db->where(['id' => $id]);
    	}
        $query = $this->db->get($this->tabla);

    	if ($id)
    	{
    		return $query->row();
    	}
    	else
    	{
    		return $query->result();
    	}
    }

   
    /**
     * Nombre: __destruct
     * Descripcion: destructor para garantizar cierre de conexion de bd
     * Autor: Jose Lozada
     * Fecha: 24-09-2019
     */
    public function __destruct(){
        $this->db->close();
    }
}
