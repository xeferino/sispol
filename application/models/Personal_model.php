<?php

class Personal_model extends CI_Model {

    /**
     * Tabla
     *
     * @var string
     */
    protected $tabla = 'sp_personal';

	/**
	 * Constructor.
	 *
	 * Cargo las clases necesarias
	 */
    public function __construct()
    {
        
    }

	/**
	 * Obtener registros de la tabla.
	 *
	 * @param Integer
	 */
    public function get($id = NULL)
    {
    	if ($id)
    	{
    		$this->db->where(['idpersonal' => $id]);
    	}
        $query = $this->db->get($this->tabla);

    	if ($id)
    	{
    		return $query->row();
    	}
    	else
    	{
    		return $query->result();
    	}
    }


    /**
     * Obtener registros de la tabla JSON.
     *
     * @param Integer
     */
    public function allUsers()
    {
        $this->db->from($this->tabla);
        $res = $this->db->get();
        $usuarios = $res->result();

        $data = array();
        foreach ($usuarios as $usuario) {
            # code...
            //$row[] = $usuarios;
            $row[0] = $usuario->idusuario;
            $row[1] = $usuario->nombres;
            $row[2] = $usuario->apellidos;
            $row[3] = $usuario->cedula;
            $row[4] = $usuario->email;
            $row[5] = $usuario->nombreusuario;
            $row[6] = $usuario->estatususuario;
            $row[7] = $usuario->tipo;
            $row[8] = $usuario->pregunta;
            $row[9] = $usuario->respuesta;

            array_push($data, $row);
        }
            $results = [ "iTotalRecords" => count($data),
                         "iTotalDisplayRecords" => count($data),
                         "aaData"=>$data 
                       ];
        return json_encode($results);
    }

	/**
	 * Agregar Personal
	 *
     * @param Array     Parámetros de búsqueda
	 * @return Array
	 */

    public function insertPersonal() {
                
        $this->db->select('cedula, documento');
        $this->db->where("documento", $this->input->post("documento"));
        $this->db->where("cedula", $this->input->post("cedula"));
        $cedula = $this->db->from($this->tabla)->get()->result();

        $this->db->select('placa');
        $this->db->where("placa", $this->input->post("placa"));
        $placa = $this->db->from($this->tabla)->get()->result();

        if(count($cedula)>0){
            return "cedula::".$this->input->post("documento")."::".$this->input->post("cedula");
        }elseif (count($placa)>0){
            return "placa::".$this->input->post("placa");
        }else{
            
            $datos = array(  "nombres" => $this->input->post("nombre"),
                                "apellidos" => $this->input->post("apellido"),
                                "documento" => $this->input->post("documento"),
                                "cedula" => $this->input->post("cedula"),
                                "placa" => $this->input->post("placa"),
                                "direccion" => $this->input->post("direccion"),
                                "telefono_habitacion" => $this->input->post("telefono_habitacion"),
                                "telefono_celular" => $this->input->post("telefono_celular"),
                                "rango" => $this->input->post("rango"),
                                "sexo" => $this->input->post("sexo"),
                                "usuario_creacion" => $this->session->userdata("idusuario")
                        );
                
            $insert = $this->db->insert($this->tabla, $datos);
            if($insert) {
                return "exito";    
            }else{
                return "error";
            }
        }
    }

    /**
     * Agregar Personal
     *
     * @param Array     Parámetros de búsqueda
     * @return Array
     */

    public function updatePersonal() {
                
        $this->db->select('cedula, documento');
        $this->db->where("documento", $this->input->post("documento"));
        $this->db->where("cedula", $this->input->post("cedula"));
        $this->db->where("idpersonal !=", $this->input->post("idpersonal"));
        $cedula = $this->db->from($this->tabla)->get()->result();

        $this->db->select('placa');
        $this->db->where("placa", $this->input->post("placa"));
        $this->db->where("idpersonal !=", $this->input->post("idpersonal"));

        $placa = $this->db->from($this->tabla)->get()->result();

        if(count($cedula)>0){
            return "cedula::".$this->input->post("documento")."::".$this->input->post("cedula");
        }elseif (count($placa)>0){
            return "placa::".$this->input->post("placa");
        }else{
            
            $datos = array( "nombres" => $this->input->post("nombre"),
                            "apellidos" => $this->input->post("apellido"),
                            "documento" => $this->input->post("documento"),
                            "cedula" => $this->input->post("cedula"),
                            "placa" => $this->input->post("placa"),
                            "direccion" => $this->input->post("direccion"),
                            "telefono_habitacion" => $this->input->post("telefono_habitacion"),
                            "telefono_celular" => $this->input->post("telefono_celular"),
                            "rango" => $this->input->post("rango"),
                            "sexo" => $this->input->post("sexo"),
                            "usuario_modificacion" => $this->session->userdata("idusuario")
                        );
                
            $this->db->where("idpersonal", $this->input->post("idpersonal"));
            $update = $this->db->update($this->tabla, $datos);
            if($update) {
                return "exito";    
            }else{
                return "error";
            }
        }
    }
    
    /**
     * Actualizar Estatus de Personal.
     *
     * @param Integer   Carga los modulos de la aplicacion
     * @param Array     Parámetros de búsqueda
     * @param Integer   Limit de la consulta
     * @return Array
     */
    
    public function updateEstatus($idpersonal)
    {
         $get = $this->db->query("SELECT * FROM sp_personal_armamentos AS pa
                                    INNER JOIN sp_personal AS p ON p.idpersonal = pa.idpersonal
                                    WHERE (pa.estatusarma = 'Pendiente' OR pa.estatusarma = 'Vencido')
                                          AND pa.idpersonal = '".$idpersonal."'");

            $validar = $get->num_rows();
            //var_dump($validar);die();

             if($validar>0){
                return "asignadas";
             }else{
                $personal = $this->get($idpersonal);
                $estatus = ($personal->estatus=="0") ? '1' : '0' ;
                $data['estatus'] = $estatus;
                $data['usuario_modificacion'] = $this->session->userdata("idusuario");

                $this->db->where("idpersonal", $idpersonal);
                $update = $this->db->update($this->tabla, $data);

                if ($update) {
                    return "exito";
                }else{
                    return "error";
                }
             }
    }

    public function getPersonalDelete($idpersonal){

        $get = $this->db->query("SELECT
                                    COUNT(*) AS validar
                                FROM
                                    sp_personal AS p
                                INNER JOIN sp_personal_armamentos AS pa ON pa.idpersonal = p.idpersonal
                                AND (pa.estatusarma ='Pendiente' OR pa.estatusarma ='Vencido')
                                WHERE p.idpersonal='".$idpersonal."'");
        $row = $get->row();
        if ($row->validar=="1"){
            return "validar";
        }else {
            $delete = $this->db->delete('sp_personal', ['idpersonal' => $idpersonal]);
            $delete = $this->db->delete('sp_personal_armamentos', ['idpersonal' => $idpersonal]);
            if ($delete) {
                return "exito";
            }else {
                return "error";
            }
        }
    }

    /**
     * Nombre: __destruct
     * Descripcion: destructor para garantizar cierre de conexion de bd
     * Autor: Jose Lozada
     * Fecha: 25/09/2019
     */
    public function __destruct(){
        $this->db->close();
    }
}
