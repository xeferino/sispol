<?php

class Configuracion_model extends CI_Model
{

    public function ObtenerConfiguracion(){
		
		$sql = $this->db->from("sp_configuracion")->get();
		$res = $sql->result();
		$configuracion = array();
			
		foreach($res as $data){
			$configuracion[$data->nombre] =  $data->valor;	
		}
		return $configuracion;	
	}
	
	public function guardarConfiguracion(){
		$datos["usuario_creacion"] = $this->session->userdata("idusuario");
		$datos["usuario_modificacion"] = $this->session->userdata("idusuario");
        $datos["valor"] = $this->input->post("emailEmisor");
		$this->db->where("nombre", "emailEmisor");
		$update = $this->db->update("sp_configuracion", $datos);
		
		$datos["valor"] = $this->input->post("emailReceptor");
		$this->db->where("nombre", "emailReceptor");
		$update = $this->db->update("sp_configuracion", $datos);
		
		$data["estatus"] = ($this->input->post("biometrico")=="si") ? 0 : 1;
		$this->db->where("urlmodulo", "huellas");
		$update = $this->db->update("sp_modulos", $data);
		
		if ($data["estatus"]==1) {

			$this->db->select('idmodulo');
            $this->db->where("urlmodulo", "huellas");
            $idmodulo = $this->db->from('sp_modulos')->get()->row()->idmodulo;
			$update = $this->db->delete('sp_modulos_usuarios', ['idmodulo' => $idmodulo]);
		}
		
		$datos["valor"] = $this->input->post("biometrico");
		$this->db->where("nombre", "biometrico");
		$update = $this->db->update("sp_configuracion", $datos);
                
		if($update){
			return "exito";	
		}else{
			return "error";	
		}	
	}
    
    public function __destruct(){
        $this->db->close();
    }
}
