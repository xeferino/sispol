<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Caracas');
class Personal extends CI_Controller {
	
	protected $faicon = "user";

	public function __construct()
	{
		parent::__construct();
		##	modelos
		$this->load->model(array('usuarios_model', 'personal_model', 'cron_model'));
		loginCheck();
		//systemCheck();
		systemModules();
		$this->cron_model->retrasoArmasPersonal();
		$this->usuario = $this->session->userdata("usuario");
		$this->tipo = $this->session->userdata("tipo");
		$this->logueado = $this->session->userdata("logueado");
		$this->idusuario = $this->session->userdata("idusuario");
		$this->modulosUsuarios = $this->usuarios_model->app_get_modulos($this->idusuario);
		$this->personal = $this->personal_model->get();
	}

	/**
     * Cargar index.
     *
     * @return View
     */
	public function index()
	{
		
		$data = array(	'title' 				=> 'Sispol - Personal',
					  	'container' 			=> 'personal/index',
					  	'titleModulo' 			=> 'Personal',
					  	'conectado' 			=> $this->usuario,
					  	'idusuario' 			=> $this->idusuario,
					  	'tipo' 					=> $this->tipo,
					  	'modulosUsuarios'		=> $this->modulosUsuarios,
					  	'faicon'				=> $this->faicon,
					  	'personal'				=> $this->personal,
					  	'button'				=> 'personal',
					  	'idmodal'				=> 'modal-form-personal',
					  	'titleicono'			=> 'Agregar Personal',
					  	'iconoboton'			=> 'plus',
					  	'script'				=> 'script_personal.js'
					  	
					);
		$this->load->view('template/template', $data);
	}

	/**
     * Add User.
     */

	public function registerPersonal()
	{
		//var_dump($this->input->post());die();
		echo $this->personal_model->insertPersonal();
	}

	/**
     * update Personal.
     */

	public function updatePersonal()
	{
		echo $this->personal_model->updatePersonal();
	}

	
	/**
	 * Proceso de obtener una lista de usuarios.
	 *
     * @return Array
	 */
	public function listDataTablePersonal()
	{
		$data = array('personal' => $this->personal);
		$this->load->view('personal/list',$data);
	}
	
	/**
	 * Proceso de obtener row user
	 *
     * @param Integer
	 */
	public function getPersonal($idpersonal)
	{
		//var_dump($idpersonal);die();
		$personal    =  $this->personal_model->get($idpersonal);
	    
	    echo $personal->idpersonal."::".
         	 $personal->nombres."::".
         	 $personal->apellidos."::".
         	 $personal->documento."::".
         	 $personal->cedula."::".
         	 $personal->placa."::".
         	 $personal->direccion."::".
         	 $personal->telefono_habitacion."::".
         	 $personal->telefono_celular."::".
         	 $personal->rango."::".
         	 $personal->sexo;
	}

	/**
	 * Proceso de obtener row user y actualizar el estatus
	 *
     * @param Integer
	 */
	public function updatePersonalEstatus($idpersonal)
	{
		echo $modulos = $this->personal_model->updateEstatus($idpersonal);
	}

	/**
	 * Proceso de obtener row user y eliminar el estatus
	 *
     * @param Integer
	 */
	public function getDelete($idpersonal)
	{
		echo $this->personal_model->getPersonalDelete($idpersonal);
	}

	/**
     * Nombre: __destruct
     * Descripcion: destructor para garantizar cierre de conexion de bd
     * Autor: Jose Lozada
     * Fecha: 23/09/2019
     */
    public function __destruct(){
        $this->db->close();
    }
}
