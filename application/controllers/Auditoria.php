<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Caracas');
class Auditoria extends CI_Controller {

	protected $faicon = "institution";

	public function __construct()
	{
		parent::__construct();
		##	modelos
		$this->load->model(array('usuarios_model', 'auditoria_model', 'cron_model'));
		loginCheck();
		//systemCheck();
		systemModules();
		$this->cron_model->retrasoArmasPersonal();
		$this->usuario = $this->session->userdata("usuario");
		$this->tipo = $this->session->userdata("tipo");
		$this->logueado = $this->session->userdata("logueado");
		$this->idusuario = $this->session->userdata("idusuario");
		$this->modulosUsuarios = $this->usuarios_model->app_get_modulos($this->idusuario);
		$this->auditoria = $this->auditoria_model->get();

	}

	/**
     * Cargar index.
     *
     * @return View
     */
	public function index()
	{
			$data = array(	'title' 			=> 'Sispol - Auditoria',
					  		'container' 		=> 'auditoria/auditoria',
					  		'titleModulo' 		=> "Auditoria",
					  		'conectado' 		=> $this->usuario,
					  		'idusuario' 		=> $this->idusuario, 
					  		'tipo' 				=> $this->tipo,
					  		'modulosUsuarios'	=> $this->modulosUsuarios,
					  		'faicon'			=> $this->faicon,
					  		'button'			=> "",
					  		'script'			=> 'script_auditoria.js',
					  		'auditoria' 		=> $this->auditoria					  		
					);
		$this->load->view('template/template', $data);
	}


	/**
     * Cargar index.
     *
     * @return View
     */
	public function getAuditoria()
	{
		$data = array('auditoria' => $this->auditoria);
		$this->load->view('auditoria/auditoria',$data);
	}

	/**
     * Nombre: __destruct
     * Descripcion: destructor para garantizar cierre de conexion de bd
     * Autor: Jose Lozada
     * Fecha: 23/09/2019
     */
    public function __destruct(){
        $this->db->close();
    }
}
