<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Caracas');
class Usuario extends CI_Controller {
	
	protected $faicon = "users";

	public function __construct()
	{
		parent::__construct();
		##	modelos
		$this->load->model(array('usuarios_model', 'modulos_model', 'cron_model'));
		loginCheck();
		//systemCheck();
		systemModules();
		$this->cron_model->retrasoArmasPersonal();
		$this->usuario = $this->session->userdata("usuario");
		$this->tipo = $this->session->userdata("tipo");
		$this->logueado = $this->session->userdata("logueado");
		$this->idusuario = $this->session->userdata("idusuario");
		$this->modulosUsuarios = $this->usuarios_model->app_get_modulos($this->idusuario);
		$this->modulos = $this->modulos_model->getModulos();
		$this->usuarios = $this->usuarios_model->accessLogin('connection');
	}

	/**
     * Cargar index.
     *
     * @return View
     */
	public function index()
	{
		if($this->tipo == "analista sistema") { 
			$usuario ="usuario";
		} else{
			$usuario = "";
		}
		$data = array(	'title' 				=> 'Sispol - Usuarios',
					  	'container' 			=> 'usuario/index',
					  	'titleModulo' 			=> 'Usuarios',
					  	'conectado' 			=> $this->usuario,
					  	'idusuario' 			=> $this->idusuario,
					  	'tipo' 					=> $this->tipo,
					  	'modulosUsuarios'		=> $this->modulosUsuarios,
					  	'modulos'				=> $this->modulos,
					  	'faicon'				=> $this->faicon,
					  	'usuarios'				=> $this->usuarios,
					  	'button'				=> $usuario,
					  	'idmodal'				=> 'modal-form-add',
					  	'titleicono'			=> 'Agregar Usuario',
					  	'iconoboton'			=> 'plus',
					  	'script'				=> 'script_usuario.js'
					  	
					);
		$this->load->view('template/template', $data);
	}

	/**
     * Add User.
     */

	public function registerUser()
	{
		echo $this->usuarios_model->insertUser();
	}

	/**
     * update User.
     */

	public function updateUser()
	{
		echo $this->usuarios_model->updateUser();
	}

	
	/**
	 * Proceso de obtener una lista de usuarios.
	 *
     * @return Array
	 */
	public function listDataTableUser()
	{
		$data = array(
				'usuarios'  => $this->usuarios, 
				'tipo'      => $this->tipo,
				'idusuario' => $this->idusuario,
				'logueado' => $this->logueado
		);
		$this->load->view('usuario/list',$data);
	}

	/**
	 * Proceso de obtener una lista de usuarios.
	 *
     * @return Array
	 */
	public function perfilUser()
	{
		$data = array('usuarios' => $this->usuarios);
		$this->load->view('usuario/perfil',$data);
	}

	/**
	 * Proceso de obtener una lista de usuarios.
	 *
     * @return Array
	 */
	public function updatePerfil()
	{
		echo $perfil = $this->usuarios_model->updatePerfil();
	}
	
	/**
	 * Proceso de obtener row user
	 *
     * @param Integer
	 */
	public function getUser($idusuario)
	{
		//var_dump($idusuario);die();
		$user    =  $this->usuarios_model->get($idusuario);
		$modulos = $this->usuarios_model->app_get_modulos($idusuario);

		$modulosUsuarios = array();
			
		foreach($modulos as $data){
			$modulosUsuarios[$data->idmodulo] =  $data->nombremodulo;	
		}
		$mod = ["dataModulos"=>$modulosUsuarios];
        $modulosUsers = json_encode($mod);

		    echo $user->idusuario."::".
             	 $user->nombres."::".
             	 $user->apellidos."::".
             	 $user->cedula."::".
             	 $user->email."::".
             	 $user->nombreusuario."::".
             	 $user->estatususuario."::".
             	 $user->tipo."::".
             	 $user->pregunta."::".
             	 $user->respuesta."::".
             	 $modulosUsers."::".
             	 $user->documento;
	}

	public function getUserProfile()
	{
		$idusuario = $this->input->post('idusuario');
		echo $idusuario;
		///$user = $this->usuarios_model->get();
		//var_dump($idusuario);die();

		/* //var_dump($idusuario);die();
		
		
		/* $modulos = $this->usuarios_model->app_get_modulos($idusuario);

		$modulosUsuarios = array();
			
		foreach($modulos as $data){
			$modulosUsuarios[$data->idmodulo] =  $data->nombremodulo;	
		}
		$mod = ["dataModulos"=>$modulosUsuarios];
        $modulosUsers = json_encode($mod); */

		   /*  echo $user->idusuario."::".
             	 $user->nombres."::".
             	 $user->apellidos."::".
             	 $user->cedula."::".
             	 $user->email."::".
             	 $user->nombreusuario."::".
             	 $user->estatususuario."::".
             	 $user->tipo."::".
             	 $user->pregunta."::".
             	 $user->respuesta."::".
             	 $user->documento; */ 
	}

	/**
	 * Proceso de obtener row user y actualizar el estatus
	 *
     * @param Integer
	 */
	public function updateUserEstatus($idusuario)
	{
		echo $modulos = $this->usuarios_model->updateEstatus($idusuario);
	}

	/**
     * Nombre: __destruct
     * Descripcion: destructor para garantizar cierre de conexion de bd
     * Autor: Jose Lozada
     * Fecha: 23/09/2019
     */
    public function __destruct(){
        $this->db->close();
    }
}
