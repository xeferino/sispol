<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Caracas');
class Armas extends CI_Controller {
	
	protected $faicon = "bomb";

	public function __construct()
	{
		parent::__construct();
		##	modelos
		$this->load->model(array('usuarios_model', 'huellas_model', 'armas_model', 'personal_model', 'cron_model', 'configuracion_model'));
		loginCheck();
		//systemCheck();
		systemModules();
		$this->cron_model->retrasoArmasPersonal();
		$this->usuario = $this->session->userdata("usuario");
		$this->tipo = $this->session->userdata("tipo");
		$this->logueado = $this->session->userdata("logueado");
		$this->idusuario = $this->session->userdata("idusuario");
		$this->modulosUsuarios = $this->usuarios_model->app_get_modulos($this->idusuario);
		$this->armas = $this->armas_model->get();
		$this->personal = $this->personal_model->get();
		$this->hora = date("H:i");
		$this->configuracion = $this->configuracion_model->ObtenerConfiguracion();
	}

	/**
     * Cargar index.
     *
     * @return View
     */
	public function index()
	{
		
		$data = array(	'title' 				=> 'Sispol - Armas',
					  	'container' 			=> 'armas/index',
					  	'titleModulo' 			=> 'Armas',
					  	'conectado' 			=> $this->usuario,
					  	'idusuario' 			=> $this->idusuario,
					  	'tipo' 					=> $this->tipo,
					  	'modulosUsuarios'		=> $this->modulosUsuarios,
					  	'faicon'				=> $this->faicon,
					  	'armas'					=> $this->armas,
					  	'button'				=> 'armas',
					  	'idmodal'				=> 'modal-form-armas',
					  	'titleicono'			=> 'Agregar Armamento',
					  	'iconoboton'			=> 'plus',
					  	'script'				=> 'script_armas.js'
					  	
					);
		$this->load->view('template/template', $data);
	}

	/**
     * Cargar index.
     *
     * @return View
     */
	public function asignarArmas()
	{
		$data = array(	'title' 				=> 'Sispol - Asignar Armas',
					  	'container' 			=> 'armas/asignararmas',
					  	'titleModulo' 			=> 'Asignar Armas',
					  	'conectado' 			=> $this->usuario,
					  	'idusuario' 			=> $this->idusuario,
					  	'tipo' 					=> $this->tipo,
					  	'modulosUsuarios'		=> $this->modulosUsuarios,
					  	'faicon'				=> $this->faicon,
					  	'armas'					=> $this->armas,
					  	'personal'				=> $this->personal,
					  	'configuracion'     	=> $this->configuracion,
					  	'button'				=> '',
						'script'				=> 'script_armas.js',
					  	'hora'				    => $this->hora
					  	
					  	
					);
		$this->load->view('template/template', $data);
	}

	/**
     * Cargar index.
     *
     * @return View
     */
	public function devolverArmas()
	{
		$movimientos = $this->armas_model->getArmamentosPersonal('Pendiente');
		$data = array(	'title' 				=> 'Sispol - Devolver Armas',
					  	'container' 			=> 'armas/devolverarmas',
					  	'titleModulo' 			=> 'Devolver Armas',
					  	'conectado' 			=> $this->usuario,
					  	'idusuario' 			=> $this->idusuario,
					  	'tipo' 					=> $this->tipo,
					  	'modulosUsuarios'		=> $this->modulosUsuarios,
					  	'faicon'				=> $this->faicon,
					  	'personal'				=> $this->personal,
					  	'configuracion'     	=> $this->configuracion,
					  	'button'				=> '',
					  	'script'				=> 'script_armas.js'
					);
		$this->load->view('template/template', $data);
	}

	public function getPersonalHuellas($proceso=NULL){
		
		$proceso = $this->input->post('proceso');
		$personal = $this->huellas_model->getHuellas();
		$data = array('personal' => $personal,
					  'proceso' => $proceso
					);
		$this->load->view('armas/personal',$data);
	}

	/**
	 * Proceso de obtener una lista de usuarios.
	 *
     * @return Array
	 */
	public function getDevolverArmas()
	{
		echo $this->armas_model->devolverArmasPersonal();
	}

	public function getInfoArmasPersonal()
	{
		
		$info = $this->armas_model->getInfoArmasPersonal($this->input->post('idpersonal'));
		$data = array('info' => $info);
		$this->load->view('armas/confimardevolarmas',$data);
	}


	/**
     * Confirmar Armas Personal.
     */

	public function ConfirmarArmasPersonal()
	{

		$p = $this->personal_model->get($this->input->post('personal'));

		$funcionario = "(".$p->documento."-".$p->cedula.") ".$p->nombres." ".$p->apellidos." Rango: ".strtoupper($p->rango)." Placa ".$p->placa;
		$idpersonal = $p->idpersonal;

		$data = array(	'fecha_a' 			=> $this->input->post('desde'),
					  	'fecha_d' 			=> $this->input->post('hasta'),
					  	'hora_a' 			=> $this->input->post('hora_a'),
					  	'hora_d' 			=> $this->input->post('hora_d'),
					  	'funcionario' 		=> $funcionario,
					  	'armasasignadas' 	=> $this->input->post('armamentos'),
					  	'idpersonal' 		=> $idpersonal,
					  	'armamentos'		=> $this->armas
					);

		$this->load->view('armas/confirmararmas',$data);
	}

	public function movimientosArmamentos()
	{
		$movimientos = $this->armas_model->getPersonalArmamento();
		$data = array(	'title' 				=> 'Sispol - Movimientos de Armas',
					  	'container' 			=> 'armas/movimientos',
					  	'titleModulo' 			=> 'Movimientos de Armas',
					  	'conectado' 			=> $this->usuario,
					  	'idusuario' 			=> $this->idusuario,
					  	'tipo' 					=> $this->tipo,
					  	'modulosUsuarios'		=> $this->modulosUsuarios,
					  	'faicon'				=> $this->faicon,
					  	'movimientos'			=> $movimientos,
					  	'button'				=> '',
					  	'script'				=> 'script_armas.js'
					);	
		$this->load->view('template/template', $data);
	}

	/**
     * Add Armas Personal.
     */

	public function addArmasPersonal()
	{
		echo $this->armas_model->addArmasPersonal();
	}

	/**
     * Add Arma.
     */

	public function registerArma()
	{
		echo $this->armas_model->insertArma();
	}

	/**
     * update Arma.
     */

	public function updateArma()
	{
		echo $this->armas_model->updateArma();
	}

	
	/**
	 * Proceso de obtener una lista de usuarios.
	 *
     * @return Array
	 */
	public function listDataTableArmas()
	{
		$data = array('armas' => $this->armas);
		$this->load->view('armas/list',$data);
	}
	
	/**
	 * Proceso de obtener row user
	 *
     * @param Integer
	 */
	public function getArma($idarma)
	{
		//var_dump($idarma);die();
		$arma    =  $this->armas_model->get($idarma);
	    
	    echo $arma->idarma."::".
         	 $arma->codigo."::".
         	 $arma->tipo."::".
         	 $arma->calibre."::".
         	 $arma->descripcion."::".
         	 $arma->cantidad;
	}

	/**
	 * Proceso de obtener row user y actualizar el estatus
	 *
     * @param Integer
	 */
	public function updateArmaEstatus($idarma)
	{
		echo $armas = $this->armas_model->updateEstatus($idarma);
	}
	
	/**
     * Nombre: __destruct
     * Descripcion: destructor para garantizar cierre de conexion de bd
     * Autor: Jose Lozada
     * Fecha: 23/09/2019
     */
    public function __destruct(){
        $this->db->close();
    }
}
