<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Caracas');
ini_set('memory_limit', '1024M');
set_time_limit(0);
    class Cronjobs extends CI_Controller{

        public function __construct(){
            parent::__construct();
            $this->load->model(array('cron_model', 'configuracion_model'));
        }
        public function index(){

            if($this->input->is_cli_request())
            {            
                $this->cron_model->retrasoArmasPersonal();
            }
            else
            {
                echo "No tiene acceso";
            }
        }

        public function biometrico() {
            //$cmd = exec ('start /B C:\xampp\htdocs\sispol\biometrico\project1.exe');
            //$WshShell = new COM("WScript.Shell");
            //$oExec = $WshShell->Run($cmd, 0, TRUE);
            pclose(popen("start /B ". 'C:\xampp\htdocs\sispol\biometrico\biometrico.exe', "r"));
        }

    /**
     * Proceso de obtener row user y actualizar el estatus
     *
     * @param Integer
     */
    public function enviarNotificacionEmail() {
        /* if($this->input->is_cli_request()){ */ 
           $notificacion = $this->cron_model->notificacionArmasPersonal();
           $configuracion = $this->configuracion_model->ObtenerConfiguracion();
          /*Acceso de aplicaciones poco seguras configurar tu cuenta de Google*/
          if ($notificacion>0) {
              $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'smtp.googlemail.com',
                    'smtp_port' => 587,
                    'smtp_user' => $configuracion['emailEmisor'],
                    'smtp_pass' => base64_decode($configuracion['passEmailEmisor']),
                    'smtp_timeout' => 30,
                    'smtp_crypto' => 'tls',
                    'mailtype'  => 'html', 
                    'charset'   => 'utf-8',
                    'wordwrap' => TRUE
                );
                $html ='<table width="100%">
                            <tr>
                            <td align="center" style="background-color:#F7FFFF">
                                <table width="90%">
                            <tr>
                            </tr>
                            <tr>
                                    <td style="background-color:#F6F6F6">
                                        <br>
                                        <p style="font-family: Arial; text-align: center; font-weight:bold;">Listado de los Funcionarios que tienen Armamentos por Entregar en el Parqueadero
                                        </p>
                                        
                                            
                                        <table style="width: 100% !important; box-shadow: 0px 0px 2em #ccc; border:solid 1px #CCC">
                                            <thead>   
                                            <tr style="font-family: Arial; text-align: center; font-size:14px;">
                                                <th style="border-bottom:solid 1px #CCC">#</th>
                                                <th style="border-bottom:solid 1px #CCC">Nombre y Apellido</th>
                                                <th style="border-bottom:solid 1px #CCC">Cedula</th>
                                                <th style="border-bottom:solid 1px #CCC">Placa</th>
                                                <th style="border-bottom:solid 1px #CCC">Rango</th>
                                                <th style="border-bottom:solid 1px #CCC">Telefonos</th>
                                            </tr>
                                            </thead>
                                            <tbody>';
                                                $i=0;
                                                foreach ($notificacion as $info) {
                                                    $i++;
                                                    if ($info->tiempoRetraso!="" and $info->tiempoRetraso>="0"){
                                                    $retraso = explode(":",$info->tiempoRetraso);
                                                    $tiempo = $retraso[0]." H ".$retraso[1]." M ".$retraso[2]." S"; 
                                                    }else{
                                                        $tiempo = "--------------";
                                                    }
                                                    $estilo = ($info->estatusarma == "Pendiente"  or $info->estatusarma == "Vencido") ? 'style="color:#d43f3a; font-weight:bold;"' : 'style="color:#5cb85c; font-weight:bold;"';
                                                
                                                $html .= '
                                                    <tr style="font-family: Arial; text-align: center; font-size:12px; ">
                                                        <td>'.$i.'</td>
                                                        <td>'.$info->nombres." ".$info->apellidos.'</td>
                                                        <td>'.$info->documento."-".$info->cedula.'</td>
                                                        <td>'.$info->placa.'</td>
                                                        <td>'.$info->rango.'</td>
                                                        <td>'.$info->telefono_celular." ".$info->telefono_habitacion.'</td>
                                                    </tr>
                                                    <tr style="font-family: Arial; text-align: center; font-size:12px; font-weight: bold;">
                                                        <td></td>
                                                        <td>Asignación</td>
                                                        <td>Devolución</td>
                                                        <td>Retraso</td>
                                                        <td>Rango</td>
                                                        <td>Estatus</td>
                                                    </tr>
                                                    <tr style="font-family: Arial; text-align: center; font-size:12px; ">
                                                        <td></td>
                                                        <td>'.$info->fechaasignacion." ".$info->horaasignacion.'</td>
                                                        <td>'. $info->fechadevolucion." ".$info->horadevolucion.'</td>
                                                        <td>'.$tiempo.'</td>
                                                        <td>'.strtoupper($info->rango).'</td>
                                                        <td '.$estilo.'>'.$info->estatusarma.'</td>
                                                    </tr>
                                                    ';
                                                }
                                                $html .= '
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                        <td style="border-top:solid 1px #CCC; font-size:10px; color:#CCC; padding-top:10px;">
                                        No responda a este correo, es una dirección de email no monitoreada<br>
                                        
                                        </td>
                                </tr>
                            </table>    
                            </td>
                        </tr>
                        </table>
                        ';
                        //echo $html;
                
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                $this->email->initialize($config);

                $this->email->from($configuracion['emailEmisor'], 'Sispol');
                $this->email->to($configuracion['emailReceptor']); 

                $this->email->subject("Notificaciones de Retrasos de Armamentos");
                $this->email->message($html); 
                // final del codigo
                if($this->email->send()) {
                    echo 'Your email was sent.';
                } else {
                   show_error($this->email->print_debugger());
                }
          }else{
            echo $notificacion;
          }
        /* }else{
            echo "No tiene acceso";
        }  */   
    }
}