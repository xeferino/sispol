<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Caracas');
class Profile extends CI_Controller {

	protected $faicon = "user";

	public function __construct()
	{
		parent::__construct();
		##	modelos
		$this->load->model(array('usuarios_model', 'auditoria_model', 'cron_model', 'armas_model'));
		loginCheck();
		//systemCheck();
		systemModules();
		$this->cron_model->retrasoArmasPersonal();
		$this->usuario = $this->session->userdata("usuario");
		$this->tipo = $this->session->userdata("tipo");
		$this->logueado = $this->session->userdata("logueado");
		$this->idusuario = $this->session->userdata("idusuario");
		$this->modulosUsuarios = $this->usuarios_model->app_get_modulos($this->idusuario);
		$this->notificaciones = $this->cron_model->topNotifiArmasPendientes();

	}

	/**
     * Cargar index.
     *
     * @return View
     */
	public function index()
	{	
        $user    =  $this->usuarios_model->get($this->idusuario);
        $accion = $this->input->post('accion');

        if ($accion=="actualizar"){
            $perfil = $this->usuarios_model->updatePerfil();
           // var_dump($perfil);die();

            $data = array(	'title' 			=> 'Sispol - Perfil',
                        'container' 		=> 'usuario/profile',
                        'titleModulo' 		=> "Perfil de Usuario",
                        'conectado' 		=> $this->usuario,
                        'idusuario' 		=> $this->idusuario,
                        'tipo' 				=> $this->tipo,
                        'modulosUsuarios'	=> $this->modulosUsuarios,
                        'faicon'			=> $this->faicon,
                        'button'			=> "",
                        'user' 		        => $user,
                        'perfil' 		    => $perfil,
                        'script'			=> 'script_usuarios.js'
                    );
            $this->load->view('template/template', $data);
        }else{
            $data = array(	'title' 			=> 'Sispol - Perfil',
                        'container' 		=> 'usuario/profile',
                        'titleModulo' 		=> "Perfil de Usuario",
                        'conectado' 		=> $this->usuario,
                        'idusuario' 		=> $this->idusuario,
                        'tipo' 				=> $this->tipo,
                        'modulosUsuarios'	=> $this->modulosUsuarios,
                        'faicon'			=> $this->faicon,
                        'button'			=> "",
                        'user' 		        => $user,
                        'perfil' 		    => '',
                        'script'			=> 'script_usuarios.js'
                    );
            $this->load->view('template/template', $data);
        }
	}


	/**
     * Cargar index.
     *
     * @return View
     */
	public function getAuditoria()
	{
		$data = array('auditoria' => $this->auditoria);
		$this->load->view('auditoria/auditoria',$data);
	}

	/**
     * Nombre: __destruct
     * Descripcion: destructor para garantizar cierre de conexion de bd
     * Autor: Jose Lozada
     * Fecha: 23/09/2019
     */
    public function __destruct(){
        $this->db->close();
    }
}
