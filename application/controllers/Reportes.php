<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Caracas');
class Reportes extends CI_Controller {

	protected $faicon = "file-pdf-o";

	public function __construct()
	{
		parent::__construct();
		##	modelos
		$this->load->model(array('usuarios_model', 'reportes_model', 'auditoria_model', 'personal_model', 'armas_model','cron_model'));
		loginCheck();
		//systemCheck();
		systemModules();
		$this->cron_model->retrasoArmasPersonal();
		$this->load->library('Pdf');
		$this->usuario = $this->session->userdata("usuario");
		$this->tipo = $this->session->userdata("tipo");
		$this->logueado = $this->session->userdata("logueado");
		$this->idusuario = $this->session->userdata("idusuario");
		$this->modulosUsuarios = $this->usuarios_model->app_get_modulos($this->idusuario);
		$this->armas = $this->armas_model->get();
		$this->personal = $this->personal_model->get();
	}

	/**
     * Cargar index.
     *
     * @return View
     */
	public function index()
	{
			$data = array(	'title' 			=> 'Sispol - Reportes',
					  		'container' 		=> 'reportes/index',
					  		'titleModulo' 		=> "Reportes",
					  		'conectado' 		=> $this->usuario,
					  		'idusuario' 		=> $this->idusuario, 
					  		'tipo' 				=> $this->tipo,
					  		'modulosUsuarios'	=> $this->modulosUsuarios,
					  		'faicon'			=> $this->faicon,
					  		'button'			=> "",
					  		'script'			=> 'script_reportes.js',
					  		'armas'				=> $this->armas,
					  		'personal'			=> $this->personal
					);
		$this->load->view('template/template', $data);
	}


	/**
     * Cargar Reporte usuarios en el sistema
     *
     * @return View
     */
	public function usuarios()
	{
		$fechadesde = $this->input->post('desde');
		$fechahasta = $this->input->post('hasta');
		$opcion = $this->input->post('opc');
		$estatus = $this->input->post('estatus');
		$this->usuarios = $this->reportes_model->reportUsuarios($fechadesde, $fechahasta, $estatus);

		if ($fechadesde!="" && $fechahasta!="") {
			$validar = "true";
		}else{
			$validar = "";
		}

		$data = array(	'fechadesde' => $fechadesde,
						'fechahasta' => $fechahasta,
						'validar'	 => $validar,
						'usuarios'   => $this->usuarios,
						'estatus'   =>  $estatus		  		
		);

		if ($opcion=="pdf") {
			$this->load->view('reportes/usuarios_pdf', $data);
		}elseif ($opcion=="excel") {
			$this->load->view('reportes/usuarios_excel', $data);
		}elseif ($opcion=="html"){

			$this->load->view('reportes/usuarios', $data);
		}else{
			$this->load->view('reportes/usuarios_pdf', $data);
		}
	}

	/**
     * Cargar Reporte personal en el sistema
     *
     * @return View
     */
	public function personal()
	{
		$fechadesde = $this->input->post('desde');
		$fechahasta = $this->input->post('hasta');
		$opcion = $this->input->post('opc');
		$estatus = $this->input->post('estatus');
		$rango = $this->input->post('rango');
		$personas = $this->reportes_model->reportPersonal($fechadesde, $fechahasta, $estatus, $rango);

		if ($fechadesde!="" && $fechahasta!="") {
			$validar = "true";
		}else{
			$validar = "";
		}

		$data = array(	'fechadesde' => $fechadesde,
						'fechahasta' => $fechahasta,
						'validar'	 => $validar,
						'personas'   => $personas,
						'estatus'   =>  $estatus,
						'rango'   =>  $rango		  		
		);

		if ($opcion=="pdf") {
			$this->load->view('reportes/personal_pdf', $data);
		}elseif ($opcion=="excel") {
			$this->load->view('reportes/personal_excel', $data);
		}elseif ($opcion=="html"){

			$this->load->view('reportes/personal', $data);
		}else{
			$this->load->view('reportes/personal_pdf', $data);
		}
	}

	/**
     * Cargar Reporte armamentos en el sistema
     *
     * @return View
     */
	public function armamentos()
	{
		$fechadesde = $this->input->post('desde');
		$fechahasta = $this->input->post('hasta');
		$opcion = $this->input->post('opc');
		$armamentos = $this->reportes_model->reportArmamentos($fechadesde, $fechahasta);
		if ($fechadesde!="" && $fechahasta!="") {
			$validar = "true";
		}else{
			$validar = "";
		}

		$data = array(	'fechadesde' => $fechadesde,
						'fechahasta' => $fechahasta,
						'validar'	 => $validar,
						'armamentos'   => $armamentos
		);

		if ($opcion=="pdf") {
			$this->load->view('reportes/armas_pdf', $data);
		}elseif ($opcion=="excel") {
			$this->load->view('reportes/armas_excel', $data);
		}elseif ($opcion=="html"){
			$this->load->view('reportes/armamentos', $data);
		}else{
			$this->load->view('reportes/armas_pdf', $data);
		}
	}

	/**
     * Cargar Reporte movimientos en el sistema
     *
     * @return View
     */
	public function movimientos()
	{
		$fechadesde = $this->input->post('desde');
		$fechahasta = $this->input->post('hasta');
		$estatus = $this->input->post('estatus');
		$idpersonal = $this->input->post('idpersonal');
		$opcion = $this->input->post('opc');
		$movimientos = $this->reportes_model->reportMovimientosArmamentos($fechadesde, $fechahasta, $estatus, $idpersonal);
		if ($fechadesde!="" && $fechahasta!="") {
			$validar = "true";
		}else{
			$validar = "";
		}

		$data = array(	'fechadesde' => $fechadesde,
						'fechahasta' => $fechahasta,
						'idpersonal' => $idpersonal,
						'estatus' 	 => $estatus,
						'validar'	 => $validar,
						'movimientos'=> $movimientos
		);

		if ($opcion=="pdf") {
			$this->load->view('reportes/movimientos_pdf', $data);
		}elseif ($opcion=="excel") {
			$this->load->view('reportes/movimientos_excel', $data);
		}elseif ($opcion=="html"){
			$this->load->view('reportes/movimientos', $data);
		}else{
			$this->load->view('reportes/movimientos_pdf', $data);
		}
	}

	/**
     * Cargar Reporte movimientos de armas individuales en el sistema
     *
     * @return View
     */
	public function movimientosArmas()
	{
		//var_dump( $this->input->post());die();
		$fechadesde = $this->input->post('desde');
		$fechahasta = $this->input->post('hasta');
		//$estatus = $this->input->post('estatus');
		$idarmamento = $this->input->post('armamentos');
		$opcion = $this->input->post('opc');
		$movimientos = $this->reportes_model->reportArmamentosMovimientos($fechadesde, $fechahasta);
		if ($fechadesde!="" && $fechahasta!="") {
			$validar = "true";
		}else{
			$validar = "";
		}

		$data = array(	'fechadesde' => $fechadesde,
						'fechahasta' => $fechahasta,
						'idarmamento' => $idarmamento,
						//'estatus' 	 => $estatus,
						'validar'	 => $validar,
						'movimientos'=> $movimientos
		);

		if ($opcion=="pdf") {
			$this->load->view('reportes/movimientos_armas_pdf', $data);
		}elseif ($opcion=="excel") {
			$this->load->view('reportes/movimientos_armas_excel', $data);
		}else{
			$this->load->view('reportes/movimientos_armas_pdf', $data);
		}
	}

	/**
     * Cargar Reporte armamentos en el sistema
     *
     * @return View
     */
	public function auditoria()
	{
		
		$auditoria = $this->auditoria_model->get();
		$usuarios = $this->usuarios_model->get();
		$data['auditoria'] = $auditoria;
		$data['usuarios'] = $usuarios;
		$this->load->view('reportes/auditoria_excel', $data);
	}

	public function estadisticas()
	{
		//var_dump($this->input->post());die();
		$fechadesde = $this->input->post('desde');
		$fechahasta = $this->input->post('hasta');
		$estatus = $this->input->post('estatus');
		$idpersonal = $this->input->post('idpersonal');
		$opcion = $this->input->post('opc');
		$armamentos = $this->input->post('armamentos');
		$movimientos = $this->reportes_model->reportEstadisticasMovimientos($fechadesde, $fechahasta, $estatus, $idpersonal, $armamentos);
		if ($fechadesde!="" && $fechahasta!="") {
			$validar = "true";
		}else{
			$validar = "";
		}

		$data = array(	'fechadesde' => $fechadesde,
						'fechahasta' => $fechahasta,
						'idpersonal' => $idpersonal,
						'estatus' 	 => $estatus,
						'validar'	 => $validar,
						'armamentos'=> $armamentos,
						'movimientos'=> $movimientos
		);

		if ($opcion=="pdf") {
			$this->load->view('reportes/estadisticas_pdf', $data);
		}elseif ($opcion=="excel") {
			$this->load->view('reportes/movimientos_excel', $data);
		}elseif ($opcion=="html"){
			$this->load->view('reportes/movimientos', $data);
		}else{
			$this->load->view('reportes/estadisticas_pdf', $data);
		}
	}

	/**
     * Nombre: __destruct
     * Descripcion: destructor para garantizar cierre de conexion de bd
     * Autor: Jose Lozada
     * Fecha: 23/09/2019
     */
    public function __destruct(){
        $this->db->close();
    }
}
