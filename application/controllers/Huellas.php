<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Caracas');
class Huellas extends CI_Controller {

	protected $faicon = "desktop";

	public function __construct()
	{
		parent::__construct();
		##	modelos
		$this->load->model(array('usuarios_model', 'personal_model','cron_model', 'huellas_model'));
		loginCheck();
		//systemModules();
		systemModules();
		$this->cron_model->retrasoArmasPersonal();
		$this->usuario = $this->session->userdata("usuario");
		$this->tipo = $this->session->userdata("tipo");
		$this->logueado = $this->session->userdata("logueado");
		$this->idusuario = $this->session->userdata("idusuario");
		$this->modulosUsuarios = $this->usuarios_model->app_get_modulos($this->idusuario);
		$this->personal = $this->huellas_model->getHuellas();

	}

	/**
     * Cargar index.
     *
     * @return View
     */
	public function index()
	{
			$data = array(	'title' 			=> 'Sispol - Huellas',
					  		'container' 		=> 'huellas/huellas',
					  		'titleModulo' 		=> "Capturar Huellas",
					  		'conectado' 		=> $this->usuario,
					  		'idusuario' 		=> $this->idusuario, 
					  		'tipo' 				=> $this->tipo,
					  		'modulosUsuarios'	=> $this->modulosUsuarios,
					  		'faicon'			=> $this->faicon,
					  		'button'			=> "",
					  		'script'			=> 'script_huellas.js',
					  		'personal' 			=> $this->personal					  		
					);
		$this->load->view('template/template', $data);
	}


	/**
     * Cargar index.
     *
     * @return View
     */
	public function getPersonalHuellas($idpersonal)
	{
		//var_dump();die();
		echo $proceso = $this->huellas_model->getHuellasPersonal($idpersonal, "1");
		if ($proceso == "exito"){
			pclose(popen("start /B ". 'C:\xampp\htdocs\sispol\bhcf\Project1.exe', "r"));
			//$cmd = exec ('start /B C:\xampp\htdocs\sispol\biometrico\biometrico.exe');
		}
	}

	public function getPersonalHuellasCapturar(){
		echo $proceso = $this->huellas_model->getHuellasPersonal("","2");
	}


	public function getDeleteHuella($idpersonal)
	{
		echo $this->huellas_model->getHuellaDelete($idpersonal);
	}

	public function getHuellasPersonalArmamentos($idpersonal)
	{
		echo $proceso = $this->huellas_model->getHuellasPersonalArmamentos($idpersonal, "1");
		if ($proceso == "exito"){
			pclose(popen("start /B ". 'C:\xampp\htdocs\sispol\bhaf\Project1.exe', "r"));
			//$cmd = exec ('start /B C:\xampp\htdocs\sispol\biometrico\biometrico.exe');
		}
	}

	public function getPersonalHuellasCapturarArmamentos(){
		$proceso = $this->huellas_model->getHuellasPersonalArmamentos("", "2");
		$validar = explode("::", $proceso);
		if($validar[0]=="exito"){
			echo $proceso;
		}
	}

	public function procesoBiometricoTempEliminar(){
		$proceso = $this->huellas_model->getHuellasPersonalArmamentos("", "3");
	}
	

	
	

	/**
     * Nombre: __destruct
     * Descripcion: destructor para garantizar cierre de conexion de bd
     * Autor: Jose Lozada
     * Fecha: 23/09/2019
     */
    public function __destruct(){
        $this->db->close();
    }
}
