<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Caracas');
class Configuracion extends CI_Controller {

    protected $faicon = "cogs";

    public function __construct()
    {
        parent::__construct();
        ##  modelos
        $this->load->model(array('cron_model', 'configuracion_model'));
        loginCheck();
        //systemCheck();
        systemModules();
        $this->cron_model->retrasoArmasPersonal();
        $this->usuario = $this->session->userdata("usuario");
        $this->tipo = $this->session->userdata("tipo");
        $this->logueado = $this->session->userdata("logueado");
        $this->idusuario = $this->session->userdata("idusuario");
        $this->modulosUsuarios = $this->usuarios_model->app_get_modulos($this->idusuario);
        $this->configuracion = $this->configuracion_model->ObtenerConfiguracion();
    }

    /**
     * Cargar index.
     *
     * @return View
     */
    public function index()
    {
            $config = $this->configuracion_model->ObtenerConfiguracion();
            $data = array(  'title'             => 'Sispol - Configuración',
                            'container'         => 'configuracion/index.php',
                            'titleModulo'       => "Configuración",
                            'conectado'         => $this->usuario,
                            'idusuario'         => $this->idusuario, 
                            'tipo'              => $this->tipo,
                            'modulosUsuarios'   => $this->modulosUsuarios,
                            'configuracion'     => $this->configuracion,
                            'faicon'            => $this->faicon,
                            'button'            => "",
                            'script'            => 'script_configuracion.js'
                            
                    );
        $this->load->view('template/template', $data);
    }

    /**
     * Cargar View.
     *
     * @return View
     */
    public function insertConfiguracion()
    {
        echo $this->configuracion_model->guardarConfiguracion();
    }


    /**
     * Nombre: __destruct
     * Descripcion: destructor para garantizar cierre de conexion de bd
     * Autor: Jose Lozada
     * Fecha: 23/09/2019
     */
    public function __destruct(){
        $this->db->close();
    }
}
