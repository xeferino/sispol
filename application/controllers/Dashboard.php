<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Caracas');
class Dashboard extends CI_Controller {

	protected $faicon = "dashboard";

	public function __construct()
	{
		parent::__construct();
		##	modelos
		$this->load->model(array('usuarios_model','dashboard_model','armas_model','cron_model', "personal_model"));
		loginCheck();
		//systemCheck();
		systemModules();
		$this->cron_model->retrasoArmasPersonal();
		$this->usuario = $this->session->userdata("usuario");
		$this->tipo = $this->session->userdata("tipo");
		$this->logueado = $this->session->userdata("logueado");
		$this->idusuario = $this->session->userdata("idusuario");
		$this->modulosUsuarios = $this->usuarios_model->app_get_modulos($this->idusuario);
		$this->personalCount = $this->dashboard_model->personalCount();
		$this->usuariosCount = $this->dashboard_model->usuariosCount();
		$this->armamentosCount = $this->dashboard_model->armamentosCount();
		$this->movimientosCount = $this->dashboard_model->movimientosCount();
		$this->personal = $this->dashboard_model->personal();
		$this->usuarios = $this->dashboard_model->usuarios();
		$this->armamentos = $this->dashboard_model->armamentos();
		$this->movimientos= $this->armas_model->getPersonalArmamento();
		$this->armasEquipos= $this->armas_model->get();
		$this->armasMovimientos =  $this->dashboard_model->movimientosArmas();
		$this->funcionarios  = $this->personal_model->get();
	}

	/**
     * Cargar index.
     *
     * @return View
     */
	public function index()
	{
			$armaspersonaldev = $this->dashboard_model->armamentosPersonal('Pendiente');
			$armaspersonalent = $this->dashboard_model->armamentosPersonal('Entregado');
			$data = array(	'title' 			=> 'Sispol - Dashboard',
					  		'container' 		=> 'dashboard/dashboard',
					  		'titleModulo' 		=> "Dashboard",
					  		'conectado' 		=> $this->usuario,
					  		'idusuario' 		=> $this->idusuario, 
					  		'tipo' 				=> $this->tipo,
					  		'modulosUsuarios'	=> $this->modulosUsuarios,
					  		'faicon'			=> $this->faicon,
					  		'button'			=> "",
					  		'script'			=> 'script_dashboard.js',
					  		'personalCount'		=> $this->personalCount,
					  		'usuariosCount'		=> $this->usuariosCount,
					  		'armamentosCount'	=> $this->armamentosCount,
					  		'movimientosCount'	=> $this->movimientosCount,
					  		'armamentos' 		=> $this->armamentos,
					  		'usuarios' 			=> $this->usuarios,
					  		'personal' 			=> $this->personal,
					  		'armaspersonaldev' 	=> $armaspersonaldev,
							'armaspersonalent' 	=> $armaspersonalent,
							'armasEquipos'      => $this->armasEquipos,
							'armasMovimientos'  => $this->armasMovimientos,
							'funcionarios'      => $this->funcionarios
					);
		$this->load->view('template/template', $data);
	}

	/**
     * Cargar View.
     *
     * @return View
     */
	public function armamentos()
	{
		$data = array('armamentos' => $this->armamentos);
		$this->load->view('dashboard/armamentos', $data);
	}

	/**
     * Cargar View.
     *
     * @return View
     */
	public function personal()
	{
		$data = array('personal' => $this->personal);
		$this->load->view('dashboard/personal', $data);
	}

	/**
     * Cargar View.
     *
     * @return View
     */
	public function usuarios()
	{
		$data = array('usuarios' => $this->usuarios);
		$this->load->view('dashboard/usuarios', $data);
	}

	/**
     * Cargar View.
     *
     * @return View
     */
	public function movimientos()
	{
		$data = array('movimientos' => $this->movimientos);
		$this->load->view('dashboard/movimientos', $data);
	}

	/**
     * Nombre: __destruct
     * Descripcion: destructor para garantizar cierre de conexion de bd
     * Autor: Jose Lozada
     * Fecha: 23/09/2019
     */
    public function __destruct(){
        $this->db->close();
    }
}
