<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Caracas');
class Auth extends CI_Controller {

	protected $check = 'Auth';

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('usuarios_model', 'cron_model'));
		$this->cron_model->retrasoArmasPersonal();
		//systemCheck();
		//loginCheck();
	}

	/**
	 * Nombre: Index
     * Descripcion: Vista del Dashboard
     * Autor: Jose Lozada
     * Fecha: 23/09/2019
	 * @return View
	 */
	
	public function index()
	{
		$data = array('title' => 'Sispol - Login' );
		$this->load->view('login', $data);
	}

	/**
	 * Nombre: Login
     * Descripcion: Iniciar Sesion
     * Autor: Jose Lozada
     * Fecha: 23/09/2019
	 * @return Array
	 */
	
	public function login()
	{
		$data = $this->input->post();
		$usuario = $this->usuarios_model->login($data);

		 if (count($usuario)==0)
            {
                echo "error";
            }elseif (count($usuario)>0 and $usuario->estatususuario=="1") {
                echo "bloqueado";
            }else{

                $sessionData = array(
                    'usuario'       =>  $usuario->nombreusuario,
                    'idusuario'     =>  $usuario->idusuario,
                    'tipo'     		=>  $usuario->tipo,
                    'logueado' 		=> TRUE
				);
				
				$this->session->set_userdata($sessionData);
				$this->usuarios_model->accessLogin('login');

                $default = $this->usuarios_model->app_get_modulos($this->session->userdata("idusuario"))[0]->urlmodulo;
                echo "exito::".$default;
            }
	}

	/**
	 * Nombre: recuperarpassword
     * Descripcion: Vista del recuperarpassword
     * Autor: Jose Lozada
     * Fecha: 23/09/2019
	 * @return View
	 */
	
	public function recuperarpassword()
	{
		$data = array('title' => 'Sispol - Recuperar Contraseña' );
		$this->load->view('loginpassword', $data);
	}

	/**
	 * Nombre: sendpassword
     * Descripcion: Vista del recuperarpassword
     * Autor: Jose Lozada
     * Fecha: 23/09/2019
	 * @return View
	 */
	
	public function sendpassword()
	{
		$datos = $this->input->post();
		
		echo $usuario = $this->usuarios_model->updatepassword($datos);
	}

	/**
	 * Nombre: Logout
     * Descripcion: Cerrar Sesion
     * Autor: Jose Lozada
     * Fecha: 23/09/2019
	 * @return Redirect
	 */
	
	public function logout()
	{
		$this->usuarios_model->accessLogin('logout');
		$destroy = array('logueado' => FALSE);
    	$this->session->set_userdata($destroy);
    	$this->session->sess_destroy();
	}

	/**
     * Nombre: __destruct
     * Descripcion: destructor para garantizar cierre de conexion de bd
     * Autor: Jose Lozada
     * Fecha: 23/09/2019
     */
    public function __destruct(){
        $this->db->close();
    }
}
