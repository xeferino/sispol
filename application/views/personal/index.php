    <!-- Modal add usuario-->
     <div class="modal fade" data-backdrop="false" id="<?=$idmodal?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="panel panel-default">
                <!-- <div class="panel-heading">
                    <i class="fa fa-user"></i> <b>Personal</b>
                </div> -->
                <div class="panel-body">
                    <div class="row">
                         <div class="col-md-12">
                            <div class="well">
                                <h4>Importante!</h4>
                                <p id="texto">Aqu&iacute; puedes agregar la informaci&oacute;n del nuevo personal!</p>
                            </div>
                        </div>
                        <form role="form" method="post" id="form-personal">
                            <input type="hidden" name="idpersonal" id="idpersonal">
                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Nombres</label>
                                        <input class="form-control" onkeypress="return keyLetter(event)" type="text" name="nombre" id="nombre" required="required">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Apellidos</label>
                                        <input class="form-control" onkeypress="return keyLetter(event)" type="text" name="apellido" id="apellido" required="required">
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>Doc</label>
                                        <select class="form-control" name="documento" id="documento" required="required">
                                            <option value="">----</option>
                                            <option value="V">V</option>
                                            <option value="E">E</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>C&eacute;dula</label>
                                        <input class="form-control numeric" type="text" name="cedula" id="cedula" maxlength="8" required="required">
                                    </div>
                                </div>
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Placa</label>
                                        <input class="form-control" type="text" name="placa" id="placa" maxlength="8" required="required">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Rango</label>
                                        <select class="form-control" name="rango" id="rango" required="required">
                                            <option value="">.::Seleccione::.</option>
                                            <option value="comandante">Comandante</option>
                                            <option value="comandante segundo">Comandante Segundo</option>
                                            <option value="jefe de operaciones">Jefe de Operaciones</option>
                                            <option value="personal motorizado">Personal Motorizado</option>
                                            <option value="oficial">Oficial</option>
                                            <option value="oficial agregado">Oficial Agregado</option>
                                            <option value="oficial jefe">Oficial Jefe</option>
                                            <option value="supervisor">Supervisor</option>
                                            <option value="supervisor agregado">Supervisor Agregado</option>
                                            <option value="supervisor jefe">Supervisor Jefe</option>
                                            <option value="comisionado">Comisionado</option>
                                            <option value="comisionado agregado">Comisionado Agregado</option>
                                            <option value="comisionado jefe">Comisionado Jefe</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label>Sexo</label>
                                        <select class="form-control" name="sexo" id="sexo" required="required">
                                            <option value="">.::Seleccione::.</option>
                                            <option value="M">Masculino</option>
                                            <option value="F">Femenino</option>
                                        </select>
                                </div>
                             </div>
                            <div class="col-lg-12">
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Tel&eacute;fono Cel.</label>
                                        <input class="form-control phone" type="text" name="telefono_celular" id="telefono_celular" required="required">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                     <div class="form-group">
                                        <label>Tel&eacute;fono Hab.</label>
                                        <input class="form-control phone" type="text" name="telefono_habitacion" id="telefono_habitacion" required="required">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Dirección</label>
                                        <textarea class="form-control" rows="3" name="direccion" id="direccion" required="required"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 pull-right">
                                <input type="submit" name="btn-inicio" id="btn-inicio-agregar" class="btn btn-md btn-primary btn-block">

                            </div>
                            <div class="col-lg-6 pull-right">
                                <a href="javascript:;" class="btn btn-md btn-default btn-block" onclick="clearForm()"> Cancelar
                                </a>
                            </div>
                        </form>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal add usuario-->

     <!-- Modal usuario estatus-->
    <div class="modal fade" id="modal-form-estatus-personal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><br></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="idpersonal" id="idpersonal">¿Desea, Cambiar el Estatus del Personal?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" onclick="updatePersonalEstatus()">Ok</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal add estatus-->
     <!-- /.row -->
    <div class="row">
        <!-- /.col-lg-6 -->
        <div class="col-lg-12" id="personal"></div>
        <!-- /.col-lg-6 -->
    </div>
    <!-- /.row -->