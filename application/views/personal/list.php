
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list"></i> <b>Listado de Personal</b>
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="dataTables-personal">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombres y Apellidos</th>
                        <th>C&eacute;dula</th>
                        <th>Placa</th>
                        <th>Rango</th>
                        <th>Sexo</th>
                        <th>Direcci&oacute;n</th>
                        <th>Tel&eacute;fonos</th>
                        <th>Estatus</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php  foreach ($personal as $persona): ?>
                                <tr class="odd gradeX">
                                    <td><?=$persona->idpersonal?></td>
                                    <td><?=$persona->nombres." ".$persona->apellidos?></td>
                                    <td><?=$persona->documento."-".$persona->cedula?></td>
                                    <td><?=$persona->placa?></td>
                                    <td><?=strtoupper($persona->rango)?></td>
                                    <td><?=$persona->sexo?></td>
                                    <td><?=$persona->direccion?></td>
                                    <td><?=$persona->telefono_habitacion."<br>".$persona->telefono_celular?></td>
                                    <td align="center">
                                        <?php if ($persona->estatus == "1"):?>
                                            <a href="javascript:;" class="btn btn-danger btn-xs btn-circle" onclick="getPersonalEstatus('<?=$persona->idpersonal?>')" title="Bloqueado">
                                                <i class="fa fa-lock"></i>
                                            </a>
                                        <?php else:?>
                                            <a href="javascript:;" class="btn btn-success btn-xs btn-circle" onclick="getPersonalEstatus('<?=$persona->idpersonal?>')" title="Activo">
                                                <i class="fa fa-unlock"></i>
                                            </a>
                                    <?php endif;?>

                                    </td>
                                    <td>
                                        <a href="javascript:;" title="Editar" class="btn btn-primary btn-xs btn-circle" onclick="getPersonal('<?=$persona->idpersonal?>')">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a 
                                            href="javascript:;" 
                                            title="Eliminar Funcionario" 
                                            class="btn btn-primary btn-xs btn-circle" 
                                            onclick="getPersonalDelete('<?=$persona->idpersonal?>')"
                                        >
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                    <?php  endforeach; ?> 
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->
<!-- Modal-->
<div class="modal fade" id="modal-form-eliminar-personal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><br></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="idpersonal" id="idpersonal">¿Desea, Eliminar el Funcionario Selecionado!?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" id="ok">Ok</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal-->