    <!-- Modal add usuario-->
    <div class="modal fade" data-backdrop="false" id="<?=$idmodal?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-12">
                            <div class="well">
                                <h4>Importante!</h4>
                                <p id="texto">Aqu&iacute; puedes agregar la informaci&oacute;n del nuevo amarmaneto!</p>
                            </div>
                        </div>
                         <ul class="nav nav-tabs">
                            <li class="active" id="equipo_armas"><a href="#armamentos" data-toggle="tab" aria-expanded="true"><b>Armamentos</b></a>
                            </li>
                            <li class="" id="equipo_policiales"><a href="#equipos" data-toggle="tab" aria-expanded="false"><b>Equipos Policiales</b></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                        <div class="tab-pane fade active in" id="armamentos">
                               <div class="row">
                                     <div class="col-lg-12">
                                        <br>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <i class="fa fa-bomb fa-fw"></i> <b>Armamentos (Pistolas y Escopetas)</b>
                                            </div>
                                            <div class="panel-body">
                                                <form role="form" method="post" id="form-add-arma">
                                                    <input type="hidden" name="idarma" id="idarma">
                                                    <div class="col-lg-12">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label>C&oacute;digo</label>
                                                                <input class="form-control" type="text" name="codigo" id="codigo" maxlength="10" onkeypress="return keyletterNumber(event)" required="required">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label>Tipo</label>
                                                                <select class="form-control" name="tipo" id="tipo" required="required">
                                                                    <option value="">----</option>
                                                                    <option value="PISTOLA">Pistola</option>
                                                                    <option value="ESCOPETA">Escopeta</option>
                                                                </select>
                                                            </div> 
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label>Calibre</label>
                                                                <select class="form-control" name="calibre" id="calibre" required="required">
                                                                    <option value="">----</option>
                                                                    <option value="9 MM">9 MM</option>
                                                                    <option value="12 MM">12 MM</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label>Cantidad</label>
                                                                <input class="form-control numeric" type="text" name="cantidad" value="1" id="cantidad" maxlength="4" required="required" readonly="true">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-lg-12">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label>Descripci&oacute;n</label>
                                                                <textarea class="form-control" rows="3" name="descripcion" id="descripcion" onkeypress="return keyletterNumber(event)"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 pull-right">
                                                        <input type="submit" name="btn-inicio" id="btn-inicio-agregar-a" class="btn btn-md btn-primary btn-block">

                                                    </div>
                                                    <div class="col-lg-6 pull-right">
                                                        <a href="javascript:;" class="btn btn-md btn-default btn-block" onclick="clearForm()"> Cancelar
                                                        </a>
                                                    </div>
                                                </form>
                                                <!-- .panel-body -->
                                            </div>
                                            <!-- .panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="equipos">
                               <div class="row">
                                     <div class="col-lg-12">
                                        <br>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <i class="fa fa-bomb fa-fw"></i> <b>Equipos Policiales (Esposas, Chalecos, Municiones)</b>
                                            </div>
                                            <div class="panel-body">
                                                <form role="form" method="post" id="form-add-arma-equipos">
                                                    <input type="hidden" name="idarma" id="idarma_e">
                                                    <div class="col-lg-12">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label>C&oacute;digo</label>
                                                                <input class="form-control" type="text" name="codigo" id="codigo_e" maxlength="10" onkeypress="return keyletterNumber(event)" required="required">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label>Tipo</label>
                                                                <select class="form-control" name="tipo" id="tipo_e" required="required">
                                                                    <option value="">----</option>
                                                                    <option value="ESPOSA">Esposas</option>
                                                                    <option value="CHALECO">Chalecos</option>
                                                                    <option value="MUNICION">Municiones</option>
                                                                </select>
                                                            </div> 
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label>Calibre Municiones</label>
                                                                <select class="form-control" name="calibre" id="calibre_e" disabled="disabled" required="required">
                                                                    <option value="">----</option>
                                                                    <option value="9 MM">9 MM</option>
                                                                    <option value="12 MM">12 MM</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label>Cantidad</label>
                                                                <input class="form-control numeric" type="text" name="cantidad" id="cantidad_e" maxlength="4" required="required">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-lg-12">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label>Descripci&oacute;n</label>
                                                                <textarea class="form-control" rows="3" name="descripcion" id="descripcion_e" onkeypress="return keyletterNumber(event)"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 pull-right">
                                                        <input type="submit" name="btn-inicio" id="btn-inicio-agregar-e" class="btn btn-md btn-primary btn-block">

                                                    </div>
                                                    <div class="col-lg-6 pull-right">
                                                        <a href="javascript:;" class="btn btn-md btn-default btn-block" onclick="clearFormE()"> Cancelar
                                                        </a>
                                                    </div>
                                                </form>
                                                <!-- .panel-body -->
                                            </div>
                                            <!-- .panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal add usuario-->
     <!-- /.row -->
    <div class="row">
        <!-- /.col-lg-6 -->
        <div class="col-lg-12" id="armas"></div>
        <!-- /.col-lg-6 -->
    </div>
    <!-- /.row -->