<div class="table-responsive">
    <table class="table" id="devolver-armas">
        <tbody>
            <?php 
            $ci =& get_instance();
            $ci->load->model(array("armas_model", "personal_model"));
                if ($info->tiempoRetraso!="" and $info->tiempoRetraso>="0"){
                    $retraso = explode(":",$info->tiempoRetraso);
                    $tiempo = $retraso[0]." H ".$retraso[1]." M ".$retraso[2]." S"; 
                }else{
                    $tiempo = "--------------";
                }
            ?>
            <tr>
                <td  class="info" colspan="5" style="font-weight:bold;">
                    <i class="fa fa-user"></i>
                    <?= "(".$info->documento."-".$info->cedula.") ".$info->nombres." ".$info->apellidos." Rango: ".strtoupper($info->rango)." Placa: ".$info->placa ?>
                </td>
            </tr>
            <tr>
                <th>Asignaci&oacute;n</th>
                <th>Devoluci&oacute;n</th>
                <th>T. Asignaci&oacute;n</th>
                <th>T. Retraso</th>
                <th>Estatus</th>
            </tr>

            <tr class="odd gradeX" style="font-weight:bold;">
                <td> <?= $info->fechaasignacion." ".$info->horaasignacion ?></td>
                <td> <?= $info->fechadevolucion." ".$info->horadevolucion ?></td>
                <td> <?= $info->tiempoAsignacion." H" ?></td>
                <td> <?= $tiempo ?></td>
                <td> 
                    <button class="btn btn-xs btn-primary">
                        <?=$info->estatusarma?>
                    </button>
                </td>
            </tr>
            <tr class="info"  style="font-weight:bold;">
                <td colspan="4">
                    <i class="fa fa-list"></i> Armas Asignadas
                </td>
                <td>Cantidad</td>
            </tr>
            <?php
            $armamentos = explode(',', $info->idarmas);
            $i=0;
            foreach ($armamentos as $keya => $asignadas):
                $i++;
                $arma = $ci->armas_model->get($asignadas);
                ?>
                <tr>
                    <td colspan="4" style="font-weight:bold;">
                        <?=$i.") <i class='fa fa-barcode'></i> Código (".$arma->codigo.") Tipo: ".$arma->tipo." Calibre: ".$arma->calibre?>
                    </td>
                    <td style="font-weight:bold;">
                        <?php
                        $cantArmas = explode(',', $info->cantarmas);
                        foreach ($cantArmas as  $keyc => $cantidad):
                            if ($keya==$keyc):
                        ?>
                            <button class="btn btn-xs btn-default">
                                 <b><?=$cantidad?> </b>
                            </button>
                    <?php
                            endif;
                        endforeach;
                     ?>
                 </td>
             </tr>
         <?php endforeach;?>
     </tbody>
 </table>
     <a href="javascript:;" id="btn-devolver" title="Devolver" class="btn btn-primary btn-md pull-right" onclick="finalizarDevolverArmas('<?=$info->idpersonal?>')">
        <i class="fa fa-exchange"></i> Devolver Armas
    </a>
</div>