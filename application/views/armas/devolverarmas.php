    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Movimientos de Entrada de Armamentos</b>
                    <a href="<?=base_url()?>armas" class="btn btn-default btn-md">Armamentos</a>
                    <a href="<?=base_url()?>armas/asignarArmas" class="btn btn-default btn-md"> <i class="fa fa-exchange"></i> Asignar Armas</a>
                    <a href="<?=base_url()?>armas/movimientosArmamentos" class="btn btn-default btn-md pull-right"> <i class="fa fa-exchange" ></i> Movimientos de Armas</a>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="well">
                            <h4>Informaci&oacute;n</h4>
                            <p>Se muestra una lista del personal que tiene armamentos pendientes por entregar, puede elegir uno de ellos para que el sistema le muestre la informaci&oacute;n de la asignaci&oacute;n.</p> 
                        </div>
                        <br>
                            <div class="alert alert-info">
                                <label>Listado del personal que tiene armas pendientes por entregar</label>
                            </div>
                            <?php if($configuracion["biometrico"]=="si"):?>
                                <a href="javascript:;" class="btn btn-md btn-primary pull-right" onclick="getPersonalBiometrico('devolver')" id="biometrico">Devolver v&iacute;a Biom&eacute;trico</a> 
                                <?php endif; ?>
                            <br>
                            <label>Funcionarios</label>
                                <select class="form-control" name="personal" id="personal-armas">
                                    <option value="0">.::Seleccione::.</option>
                                    <?php 
                                        $ci =& get_instance();
                                        $ci->load->model("armas_model");
                                        foreach ($personal as $persona):
                                            $validar = $ci->armas_model->getInfoArmasPersonal($persona->idpersonal);
                                            if ($persona->idpersonal==$validar->idpersonal && $validar->estatusarma=="Pendiente" or $validar->estatusarma=="Vencido"):
                                    ?>
                                                <option class="text-danger" value="<?=$validar->idpersonal?>">
                                                        <?="(".$persona->documento."-".$persona->cedula.") ".$persona->nombres." ".$persona->apellidos." Rango: ".strtoupper($persona->rango)." Placa: ".$persona->placa?>
                                                </option>                                    
                                    <?php
                                            endif; 
                                        endforeach;
                                    ?>
                                </select>
                        <br><br><br>
                        <div id="getInfoArmas"></div>
                    </div>
                </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<div id="getInfoPersonal"></div>
<!-- /.row -->