    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Movimientos de Salida de Armamentos</b>
                     <a href="<?=base_url()?>armas" class="btn btn-default btn-md">Armamentos</a>
                     <a href="<?=base_url()?>armas/devolverArmas" class="btn btn-default btn-md"> <i class="fa fa-exchange"></i> Devolver Armas</a>
                      <a href="<?=base_url()?>armas/movimientosArmamentos" class="btn btn-default btn-md pull-right"> <i class="fa fa-exchange" ></i> Movimientos de Armas</a>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                         <form method="post" name="form-asignar-armamentos" id="form-asignar-armamentos" class="form-horizontal" action="" target="">
                            <br>
                            <div class="col-md-12">
                                <div class="well">
                                    <h4>Informaci&oacute;n</h4>
                                    <p>En el siguiente formulario podr&aacute; asignar las diferentes armas disponible al personal de guardia.</p> 
                                </div>
                            </div>
                            <br>
                            <div class="col-md-12">
                                <?php if($configuracion["biometrico"]=="si"):?>
                                <a href="javascript:;" class="btn btn-md btn-primary pull-right" onclick="getPersonalBiometrico('asignar')" id="biometrico">Asignar v&iacute;a Biom&eacute;trico</a> 
                                <?php endif; ?>
                            </div>
                            <div class="col-md-6">
                                <label>Funcionario</label>
                                <select class="form-control" name="personal" id="personal">
                                    <option value="0">.::Seleccione::.</option>
                                    <?php 
                                        $ci =& get_instance();
                                        $ci->load->model("armas_model");
                                        foreach ($personal as $persona):
                                            if ($persona->estatus!='1'):  
                                            $validar = $ci->armas_model->getInfoArmasPersonal($persona->idpersonal);
                                            if ($persona->idpersonal==$validar->idpersonal && $validar->estatusarma=="Pendiente" or $validar->estatusarma=="Vencido"):
                                    ?>
                                    <?php else: ?>
                                                <option class="text-info" value="<?=$persona->idpersonal?>">
                                                    <?="(".$persona->documento."-".$persona->cedula.") ".$persona->nombres." ".$persona->apellidos." Placa: ".$persona->placa?>   
                                                </option>
                                    <?php
                                            endif; 
                                        endif;
                                        endforeach;?>
                                </select>
                            </div>

                             <div class="col-md-6">
                                <label>Armamentos</label>
                                <select class="form-control" name="armamentos[]" id="armamentos" multiple="multiple" placeholder=".::Seleccione::.">
                                    <?php foreach ($armas as $arma):
                                            $disponible = $arma->cantidad-$arma->cantidad_asignada;
                                            if ($disponible=="0"):   
                                    ?>
                                            <?php else: ?>
                                                <option class="text-info" value="<?=$arma->idarma?>"><?="(".$arma->codigo.") Tipo: ".$arma->tipo." Calibre: ".$arma->calibre." Disponible: (".$disponible.")"?>
                                                </option>
                                    <?php    endif;
                                        endforeach;
                                    ?>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <br>
                                <label>Fecha Asignaci&oacute;n</label>
                                <div class="input-group bootstrap-timepicker">
                                    <input type="text" name="desde" id="desde" class="form-control " placeholder="0000-00-00" value="<?=date('Y-m-d')?>" readonly="readonly">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar bigger-110"></i>
                                    </span>
                                </div>
                            </div>
                      
                            <div class="col-md-3">
                                <br>
                                <label>Hora Asignaci&oacute;n</label>
                                <div class="input-group bootstrap-timepicker">
                                    <input id="h_asignacion" name="hora_a" type="text" class="form-control" readonly="readonly"
                                    value="<?=date('H:i:s')?>" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-clock-o bigger-110"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <br>
                                <label>Fecha Devoluci&oacute;n</label>
                                <div class="input-group bootstrap-timepicker">
                                    <input type="text" name="hasta" id="hasta" class="form-control " placeholder="0000-00-00" value="<?=date('Y-m-d')?>" readonly="readonly">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar bigger-110"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <br>
                                <label>Hora Devoluci&oacute;n</label>
                                <div class="input-group bootstrap-timepicker">
                                    <input id="h_devolucion" name="hora_d" type="text" class="form-control" value="<?=date('H:i:s')?>" readonly="readonly"/>
                                    <span class="input-group-addon">
                                        <i class="fa fa-clock-o bigger-110"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <input type="submit" style="margin-top: 25px;" name="btn-inicio" id="btn-inicio-buscar" value="Asignar Armas" class="btn btn-md btn-primary pull-right">
                            </div>
                            <br>
                        </form>
                    </div>
                    </div>
                    <hr>
                    <!-- /.tablse-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div id="getInfo"></div>
    <div id="getInfoPersonal"></div>