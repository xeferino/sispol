<!--Modal usuario estatus-->
<div class="modal fade" id="modal-confimar-armamentos">
    <div class="modal-dialog" role="document">
    <!-- /.row -->
        <div class="panel panel-default">
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="well">
                        <h4>Importante!</h4>
                        <p>Debe confirmar, la cantidad de armas a asignar al funcionario, cabe resaltar que el sistema por defecto le asigna un armamento automaticamente.</p> 
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <label>Funcionario</label>
                            <div class="input-group bootstrap-timepicker">
                                <input id="personal" name="personal" value="<?=$funcionario?>" type="text" class="form-control" readonly="readonly" />
                                <input id="funcionario" name="funcionario" value="<?=$idpersonal?>" type="hidden">
                                <span class="input-group-addon">
                                    <i class="fa fa-user bigger-110"></i>
                                </span>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                           <br>
                           <div class="alert alert-info">
                                <label>Listado de armamentos seleccionados al funcionario, puede actualizar la cantidad a asignar </label>
                            </div>
                        </div>
                            <?php
                                $asignadasarmas = implode(",", $armasasignadas);
                                $i=0;
                                foreach ($armamentos as $armas):
                                    foreach ($armasasignadas as $arma):
                                        if($arma==$armas->idarma):
                                            $i++;
                                            $disponible = $armas->cantidad-$armas->cantidad_asignada;
                             ?>
                             <div class="col-md-12">  
                                <div class="col-md-10">
                                    <span>
                                        <?="<b>".$i.")</b> (".$armas->codigo.") Tipo: ".$armas->tipo." Calibre: ".$armas->calibre." <b>Diponible: (".$disponible.")</b>"?> 
                                    </span>
                                </div>
                                <div class="col-md-2">
                                    <input style="margin-top: -8px;" type="text" name="cantidad" id="<?=$armas->idarma?>" class="form-control cantidad numeric" value="1" maxlength="2">
                                    <br>
                                </div>
                            </div>
                             <?php
                                        endif;
                                    endforeach;
                                endforeach;
                            ?> 
                        
                        <div class="col-md-6">
                            <br>
                            <label>Fecha Asignaci&oacute;n</label>
                            <div class="input-group bootstrap-timepicker">
                                <input type="text" name="fecha_a" id="fecha_a" class="form-control datepicker" placeholder="0000-00-00" value="<?=$fecha_a?>" readonly="readonly">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar bigger-110"></i>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <br>
                            <label>Hora Asignaci&oacute;n</label>
                            <div class="input-group bootstrap-timepicker">
                                <input id="hora_a" name="hora_a" value="<?=$hora_a?>" type="text" class="form-control" readonly="readonly" />
                                <span class="input-group-addon">
                                    <i class="fa fa-clock-o bigger-110"></i>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <br>
                            <label>Fecha Devoluci&oacute;n</label>
                            <div class="input-group bootstrap-timepicker">
                                <input type="text" name="fecha_d" id="fecha_d" class="form-control datepicker" placeholder="0000-00-00" value="<?=$fecha_d?>" readonly="readonly">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar bigger-110"></i>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <br>
                            <label>Hora Devoluci&oacute;n</label>
                            <div class="input-group bootstrap-timepicker">
                                <input id="hora_d" name="hora_d" value="<?=$hora_d?>" type="text" class="form-control" readonly="readonly"/>
                                <span class="input-group-addon">
                                    <i class="fa fa-clock-o bigger-110"></i>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button style="margin-top: 25px;" name="finalizar-asignacion" id="finalizar-asignacion" class="btn btn-md btn-primary pull-right" onclick="finalizarAsignacionArmas()">Finalizar</button>
                             <button style="margin-top: 25px; margin-right: 15px;" type="button" class="btn btn-md btn-default pull-right" data-dismiss="modal">Cancelar</button>
                        </div>
                        <br>
                    </div>
                </div>
                <hr>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
 </div>
<!-- Modal add estatus