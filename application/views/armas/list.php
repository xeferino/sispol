
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list"></i> <b>Listado de Armas</b> 
        <a href="<?=base_url()?>armas/asignarArmas" class="btn btn-default btn-md"> <i class="fa fa-exchange"></i> Asignar Armas</a>
        <a href="<?=base_url()?>armas/devolverArmas" class="btn btn-default btn-md"> <i class="fa fa-exchange"></i> Devolver Armas</a>
        <a href="<?=base_url()?>armas/movimientosArmamentos" class="btn btn-default btn-md pull-right"> <i class="fa fa-exchange" ></i> Movimientos de Armas</a>
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="dataTables-armas">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>C&oacute;digo</th>
                        <th>Tipo</th>
                        <th>Calibre</th>
                        <th>Descripci&oacute;n</th>
                        <th>Cantidad</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php  foreach ($armas as $arma): ?>
                                <tr class="odd gradeX">
                                    <td><?=$arma->idarma?></td>
                                    <td><?=$arma->codigo?></td>
                                    <td><?=$arma->tipo?></td>
                                    <td><?=($arma->calibre=="") ? "--------" : $arma->calibre;?></td>
                                    <td><?=($arma->descripcion=="") ? "--------" : $arma->descripcion;?></td>
                                    <td>
                                        <a href="javascript:;" class="btn btn-default btn-xs">
                                        <?=$arma->cantidad?>
                                        </a>
                                    <td>
                                        <a href="javascript:;" title="Editar" class="btn btn-primary btn-xs btn-circle" onclick="getArma('<?=$arma->idarma?>')">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                    <?php  endforeach; ?> 
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->