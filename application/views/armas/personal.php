 <!-- Modal usuario estatus-->
    <div class="modal fade" id="modal-personal-huellas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><br></h4>
                </div>
                <div class="modal-body">
                    <div class="well">
                        <h4>Importante!</h4>
                        <p id="texto">Aqui puedes elegir el funcionario para capturar la huella,
                                        para continuar la asignacion del amarmaneto!</p> 
                    </div>
                   
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-personal-huellas">
                            <thead>
                                <tr>
                                    <th><i class="fa fa-user"></i> Funcionario</th>
                                </tr>
                            </thead>
                            <tbody>

                            <?php
                             $ci =& get_instance();
                             $ci->load->model("armas_model");
                            if ($proceso=="asignar") {
                                # code...
                            ?>
                            <?php foreach ($personal as $persona){ ?>
                                    <tr class="odd gradeX">
                                        <td>
                                            <b><?=$persona->idpersonal.") ".$persona->documento."-".$persona->cedula." ".$persona->nombres." ".$persona->apellidos." Placa: ".$persona->placa." Rango:".strtoupper($persona->rango)?></b>
                                            <?php if ($persona->huella != ""){ ?>
                                                <?php if ($persona->estatus != "1"){
                                                    $validar = $ci->armas_model->getInfoArmasPersonal($persona->idpersonal);
                                                    if (($validar) && ($persona->idpersonal==$validar->idpersonal && $validar->estatusarma=="Pendiente" or $validar->estatusarma=="Vencido")){
                                                ?>       
                                                   <!--  <a 
                                                        href="javascript:;" 
                                                        title="Capturar Huellas" 
                                                        class="btn btn-primary btn-xs btn-circle pull-right" 
                                                        onclick="getHuella('<?=$persona->idpersonal?>')"
                                                        
                                                    >
                                                        <i class="fa fa-desktop"></i>
                                                    </a> -->
                                                    <?php } else{?>
                                                        <a 
                                                        href="javascript:;" 
                                                        title="Capturar Huellas" 
                                                        class="btn btn-primary btn-xs btn-circle pull-right" 
                                                        onclick="getHuella('<?=$persona->idpersonal?>', 'asignar')"
                                                        
                                                    >
                                                        <i class="fa fa-desktop"></i>
                                                    </a>   
                                                        <?php }?>
                                                <?php }?>
                                            <?php }?>
                                        </td>
                                    </tr>
                            <?php  } ?> 
                        <?php  }elseif($proceso=="devolver") {?>
                            <?php foreach ($personal as $persona){ ?>
                                    <tr class="odd gradeX">
                                        <td>
                                        
                                            <b><?=$persona->idpersonal.") ".$persona->documento."-".$persona->cedula." ".$persona->nombres." ".$persona->apellidos." Placa: ".$persona->placa." Rango:".strtoupper($persona->rango)?></b>
                                            <?php
                                                    $validar = $ci->armas_model->getInfoArmasPersonal($persona->idpersonal);
                                                    if (($validar) && ($persona->idpersonal==$validar->idpersonal && $validar->estatusarma=="Pendiente" or $validar->estatusarma=="Vencido")){
                                                ?>  <?php if ($persona->huella != ""){ 
                                                    if ($persona->estatus != "1"){ ?>     
                                                    <a 
                                                        href="javascript:;" 
                                                        title="Capturar Huellas" 
                                                        class="btn btn-primary btn-xs btn-circle pull-right" 
                                                        onclick="getHuella('<?=$persona->idpersonal?>','devolver')"
                                                        
                                                    >
                                                        <i class="fa fa-desktop"></i>
                                                    </a>
                                                    <?php } ?>
                                               
                                        </td>
                                    </tr>
                                            <?php  } ?> 
                                    <?php  } ?> 
                            <?php  } ?> 
                            <?php } ?>

                                </tbody>
                        </table>           
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal add estatus-->