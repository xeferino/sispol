<?php 
    $ci =& get_instance();
    $ci->load->model(array("cron_model", "personal_model", "armas_model"));
    $top =$ci->cron_model->topNotifiArmasPendientes();

    $mes= date('m'); $dia= date('d');$anio= date('Y');
    if($mes == 1){ $mes = "Enero";} 
    if($mes == 2){ $mes = "Febrero";} 
    if($mes == 3){ $mes = "Marzo";}
    if($mes == 4){ $mes = "Abril";} 
    if($mes == 5){ $mes = "Mayo";} 
    if($mes == 6){ $mes = "Junio";}
    if($mes == 7){ $mes = "Julio";} 
    if($mes == 8){ $mes = "Agosto";} 
    if($mes == 9){ $mes = "Septiembre";}
    if($mes == 10){ $mes = "Octubre";}
    if($mes == 11){ $mes = "Noviembre";}
    if($mes == 12){ $mes = "Diciembre";}

?>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>

    <ul class="nav navbar-nav navbar-left navbar-top-links">
        <li><!-- <a href="#<?=base_url()?>dashboard"><i class="fa fa-home fa-fw"></i> Sispol</a> -->
            <a class="navbar-brand" style="color: #fff !important" href="#">
                <!-- <i class="fa fa-home fa-fw"></i> Sispol -->
                <img src="<?=base_url('public/img/logo-gbv.png')?>" style="max-height: 65px; margin-top: -25px; margin-left: -14px;">
            </a>
        </li>
    </ul>

    <ul class="nav navbar-right navbar-top-links">
        <li class="dropdown">
            <b style="color: #fff !important">
                 <i class="fa fa-clock-o"></i> Reloj: <span id="hora"></span> |
            </b>
        </li>
        <li class="dropdown">
            <b style="color: #fff !important">
                 <i class="fa fa-calendar"></i> Fecha: <?=$dia." de ".$mes." del ".$anio;?> |
            </b>
        </li>
        <li class="dropdown">
            <b style="color: #fff !important">
                Usuario: <?=strtoupper($conectado)?>
            </b>
        </li>
        <li class="dropdown">
            <b style="color: #fff !important">
                | </i> Tipo: <?=strtoupper($tipo)?>
            </b>
        </li>
        <?php if ($top>0) { ?>

            <li class="dropdown navbar-inverse">
                <a class="dropdown-toggle" title="Funcionarios que estan pendientes por entregar armas" data-toggle="dropdown" href="#" style="color: #fff000 !important">
                    <i class="fa fa-bell fa-fw"></i> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu dropdown-alerts">
                    <?php 
                        foreach ($top as $data):
                        $personal = $ci->personal_model->get($data->idpersonal); 
                    ?>
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-user fa-fw"></i> <?= "(".$personal->documento."-".$personal->cedula.") ".$personal->nombres." ".$personal->apellidos." Rango: ".strtoupper($personal->rango)?>
                                <?php
                                      $armamentos = explode(',', $data->idarmas);
                                      foreach ($armamentos as $keya => $asignadas) {
                                        $arma = $ci->armas_model->get($asignadas);
                                        $cantArmas = explode(',', $data->cantarmas);
                                            foreach ($cantArmas as $keyc => $cantidad) {
                                                if($keya==$keyc){
                                ?>
                                <span class="pull-right text-muted small"><?=$arma->tipo." Calibre: ".$arma->calibre." Cantidad: ".$cantidad?></span>
                             <?php
                                                }
                                            }
                                        }
                             ?>
                            </div>
                        </a>
                    </li>
                    <?php endforeach;?>
                    <li class="divider"></li>
                    <li>
                        <a class="text-center" href="<?=base_url()?>notificaciones">
                            <strong>Ver Todas</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
            </li>
        <?php } ?>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: #fff !important" >
               <b> <i class="fa fa-user fa-fw"></i></b><b class="caret"></b>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li>
                <a href="<?=base_url()?>profile"><i class="fa fa-edit fa-fw"></i> Actualizar Perfil </a>
                   <!-- <a href="javascript:;" onclick="perfil('<?=$idusuario?>')"><i class="fa fa-edit fa-fw"></i> Actualizar Perfil </a> -->
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#" data-backdrop="static" data-keyboard="false"data-toggle="modal" data-target="#logout">
                        <i class="fa fa-sign-out fa-fw"></i> Salir
                    </a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- /.navbar-top-links -->
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <?php  
                    $ci =& get_instance();
                    foreach ($modulosUsuarios as $modulo): 
                        $activo = ($ci->uri->segment(1) == $modulo->urlmodulo) ? 'active' :'';
                ?>
                        <?php if ($modulo->estatus!=1): ?>
                        <li>
                            <a href="<?=base_url().$modulo->urlmodulo?>" style="color: #000; font-weight: bold;" class="<?=$activo?>">
                                <i class="fa fa-<?=$modulo->iconomodulo?> fa-2x" style="color: #e61010;"></i> 
                                <?=$modulo->nombremodulo?>
                            </a>
                        </li>
                    <?php endif;?>
            <?php  endforeach; ?>
            </ul>
        </div>
    </div>
</nav>