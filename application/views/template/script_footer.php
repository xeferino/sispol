        <!-- Bootstrap Core JavaScript -->
        <script src="<?=base_url('public/js/jquery.min.js')?>"></script>
        <script src="<?=base_url('public/js/bootstrap.min.js')?>"></script>

        <!-- App Core JavaScript -->
        <script type="text/javascript" src="<?=base_url('public/js/app.js')?>"></script>

         <!-- Functions Core JavaScript -->
         <script type="text/javascript" src="<?=base_url('public/js/functions/').$script?>"></script>

        <!-- DataTables JavaScript -->
        <script src="<?=base_url('public/js/dataTables/jquery.dataTables.min.js')?>"></script>
        <script src="<?=base_url('public/js/dataTables/dataTables.bootstrap.min.js')?>"></script>

        <!-- datetimepicker JavaScript -->
        <script src="<?=base_url('public/datepicker/jquery-ui.js')?>"></script>
        <script src="<?=base_url('public/datepicker/datepicker-es.js')?>"></script>
        <script src="<?=base_url('public/timepicker/bootstrap-timepicker.min.js')?>"></script>

        <!-- maskedinput JavaScript -->        
        <script src="<?=base_url('public/js/jquery.maskedinput.min.js')?>"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?=base_url('public/js/metisMenu.min.js')?>"></script>

       
        <!-- Custom Theme JavaScript -->
        <script src="<?=base_url('public/js/startmin.js')?>"></script>

        <!-- Select2 Core JavaScript -->
        <script type="text/javascript" src="<?=base_url('public/select/js/select2.js')?>"></script>