<!DOCTYPE html>
<html lang="en">
    <!-- /#head -->
    <?php $this->load->view('template/head');?>
    <body>
        <div id="msj_exito"><div class="alert alert-success"></div></div>
        <div id="msj_error"><div class="alert alert-danger"></div></div>
        <div id="msj_advertencia"><div class="alert alert-warning"></div></div>
        <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url();?>">
        <div class="divCarga" id="div_loading">
            <div class="fontmensaje" style="color: #ffffff; margin-top:300px;" id="mensajetextoCarga"></div>
            <img id="imgtextocarga" style="margin-top:10px;  width: 100px;" src="<?=base_url('public/img/loading_trans.gif')?>">    
        </div>
        <!-- /#wrapper -->
        <div id="wrapper">
            <!-- Navigation -->
            <?php $this->load->view('template/navbar');?>
            <!-- Navigation -->

            <!-- /#page-wrapper -->
            <div id="page-wrapper">
                <!-- /.container-fluid -->
                <div class="container-fluid">
                    <!-- /.row -->
                    <div class="row">
                        <!-- /.col-lg-12 -->
                        <div class="col-lg-12">
                            <h1 class="page-header"><i class="fa fa-<?=$faicon?> fa-fw"></i> <?=$titleModulo?></h1>
                        </div>
                        <!-- /.col-lg-12 -->
                        <!-- /.col-lg-12 -->
                        <?php if ($button!=""){ ?>
                            <div class="col-lg-12">
                                <!-- /.col-lg-12 -->
                                <button type="button" class="btn btn-primary btn-lg pull-right" 
                                        style="margin-top: -80px" data-backdrop="static" 
                                        data-keyboard="false" data-toggle="modal" 
                                        data-target="#<?=$idmodal?>" title="<?=$titleicono?>">
                                    <i class="fa fa-<?=$iconoboton?>"></i>
                                </button>
                            </div>
                        <?php }?>
                    </div>
                    <!-- /.row Container-->
                    <?php $this->load->view($container); ?>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <!-- Modal usuario logout-->
        <div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel"><br></h4>
                    </div>
                    <div class="modal-body">
                        ¿Disculpe, desea salir de la aplicacion?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick="logout()">Ok</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- Modal add logout-->
        <!-- Modal add perfil-->
        <?php $this->load->view('usuario/perfil'); ?>
        <!-- Modal add perfil-->

        <?php $this->load->view('template/script_footer'); ?>
    </body>
</html>
