<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?=base_url('public/img/favicon.ico')?>" type="image/x-icon">

        <title><?=$title?></title>
        <!-- Bootstrap Core CSS -->
        <link href="<?=base_url('public/css/bootstrap.min.css')?>" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?=base_url('public/css/metisMenu.min.css')?>" rel="stylesheet">
        <script src="<?=base_url('public/js/jquery.min.js')?>"></script>

        <!-- Custom CSS -->
        <link href="<?=base_url('public/css/startmin.css')?>" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?=base_url('public/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
        
        <!-- Select2 plugins -->
        <link rel="stylesheet" type="text/css" href="<?=base_url('public/select/css/select2.css')?>">
        <link rel="stylesheet" type="text/css" href="<?=base_url('public/select/css/select2-bootstrap.css')?>">

         <!-- datetimepicker CSS -->
        <link href="<?=base_url('public/datepicker/jquery-ui.css')?>" rel="stylesheet">
        <link rel="stylesheet" href="<?=base_url('public/timepicker/bootstrap-timepicker.min.css')?>" />
         <!-- DataTables CSS -->
        <link href="<?=base_url('public/css/dataTables/dataTables.bootstrap.css')?>" rel="stylesheet">

        <!-- DataTables Responsive CSS -->
        <link href="<?=base_url('public/css/dataTables/dataTables.responsive.css')?>" rel="stylesheet">

        <!-- App style -->
        <link rel="stylesheet" type="text/css" href="<?=base_url('public/css/app.css')?>">

        <!-- Timeline CSS -->
        <link href="<?=base_url('public/css/timeline.css')?>" rel="stylesheet">
    
         <!-- Charts JavaScript -->
         <script src="<?=base_url('public/js/awesomechart.js')?>"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
          <style type="text/css" media="screen">
            .modal-header {
                padding:9px 15px;
                border-bottom:1px solid #e61010;
                background-color: #d20d0d;
                -webkit-border-top-left-radius: 5px;
                -webkit-border-top-right-radius: 5px;
                -moz-border-radius-topleft: 5px;
                -moz-border-radius-topright: 5px;
                 border-top-left-radius: 5px;
                 border-top-right-radius: 5px;
                 color: #fff;
             }
             .datepicker {
              z-index: 1200 !important; /* has to be larger than 1050 */
             }
             .btn-primary {
                color: #fff !important;
                background-color: #e61010 !important;
                border-color: #d20d0d !important;
            }

            .btn-primary:hover {
                color: #fff !important;
                background-color: #e61010 !important;
            }

            .btn-default {
                color: #333;
                background-color: #ddd;
                border-color: #ccc;
            }

            .navbar:hover {
                color: #fff !important;
            }
                    
        </style>

    </head>