
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list"></i> <b>Listado de Usuarios</b>
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="dataTables-usuarios">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombres y Apellidos</th>
                        <th>C&eacute;dula</th>
                        <th>Tipo</th>
                        <th>Ultimo Acceso</th>
                        <th>Estatus</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php  foreach ($usuarios as $usuario):?>
                                <tr class="odd gradeX">
                                    <td><?=$usuario->idusuario?></td>
                                    <td><?=$usuario->nombres." ".$usuario->apellidos?></td>
                                    <td><?=$usuario->documento."-".$usuario->cedula?></td>
                                    <td><?=strtoupper($usuario->tipo)?></td>
                                    <td><?= ($usuario->idusuario == $idusuario) ? $usuario->last_login : $usuario->login;?></td>
                                    <td align="center">
                                        <?php if ($usuario->estatususuario == "1"):?>
                                            <?php if ($tipo == "analista sistema" or $usuario->idusuario == $idusuario):?> 
                                                <a href="javascript:;" class="btn btn-danger btn-xs btn-circle" onclick="getUserEstatus('<?=$usuario->idusuario?>')">
                                                    <i class="fa fa-lock"></i>
                                                </a>
                                                <?php else:?>
                                                    <a href="javascript:;" class="btn btn-danger btn-xs btn-circle" onclick="getUserEstatus('<?=$usuario->idusuario?>')" disabled="disabled">
                                                        <i class="fa fa-lock"></i>
                                                    </a>
                                            <?php endif;?>
                                        <?php else:?>
                                            <?php if ($tipo == "analista sistema" or $usuario->idusuario == $idusuario):?> 
                                                <a href="javascript:;" class="btn btn-success btn-xs btn-circle" <?php if ($tipo == "analista sistema" or $usuario->idusuario == $idusuario):?> onclick="getUserEstatus('<?=$usuario->idusuario?>')" <?php endif;?>>
                                                    <i class="fa fa-unlock"></i>
                                                </a>
                                                <?php else:?>
                                                    <a href="javascript:;" class="btn btn-success btn-xs btn-circle" <?php if ($tipo == "analista sistema" or $usuario->idusuario == $idusuario):?> onclick="getUserEstatus('<?=$usuario->idusuario?>')" <?php endif;?> disabled="disabled">
                                                        <i class="fa fa-unlock"></i>
                                                    </a>
                                            <?php endif;?>
                                            
                                    <?php endif;?>

                                    </td>
                                    <td>
                                    <?php if ($tipo == "analista sistema" or $usuario->idusuario == $idusuario):?> 
                                            <a href="javascript:;" title="Editar" class="btn btn-primary btn-xs btn-circle" onclick="getUser('<?=$usuario->idusuario?>')">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                    <?php else:?>
                                            <a href="javascript:;" title="Editar" class="btn btn-primary btn-xs btn-circle"  onclick="getUser('<?=$usuario->idusuario?>')" disabled="disabled">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                    <?php endif;?>    
                                    </td>
                                </tr>
                    <?php  endforeach; ?> 
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->