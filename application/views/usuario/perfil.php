    <style>
        .eye {
            right: 0;
            position: absolute;
            margin-top: -28px;
            margin-right: -5px;
            font-size: large;
            cursor:pointer;
            color: #ccc;
        }
    </style>
<!-- Modal add usuario-->
     <div class="modal fade" data-backdrop="false" id="perfil" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="panel panel-default">
               <!--  <div class="panel-heading">
                    <i class="fa fa-users"></i> <b>Perfil de Usuario</b>
                </div> -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="well">
                                <h4>Importante!</h4>
                                <p id="texto">Aqui puedes editar la información de tu perfil!</p>
                            </div>
                        </div>
                        <form role="form" method="post" id="form-perfil-user">
                            <input type="hidden" name="idusuario" id="idusuario_perfil">
                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Nombres</label>
                                        <input class="form-control" type="text" name="nombre" id="nombre_perfil" required="required">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Apellidos</label>
                                        <input class="form-control" type="text" name="apellido" id="apellido_perfil" required="required">
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>Doc</label>
                                        <select class="form-control" name="documento" id="documento_perfil" required="required">
                                            <option value="">----</option>
                                            <option value="V">V</option>
                                            <option value="J">J</option>
                                            <option value="E">E</option>
                                            <option value="G">G</option>
                                            <option value="R">R</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Cedula</label>
                                        <input class="form-control numeric" type="text" name="cedula" id="cedula_perfil" required="required">
                                    </div>
                                </div>
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" type="email" name="email" id="email_perfil" required="required">
                                        <span class="eye" id="eyes-click-email"  onclick="eyesPerfil('email')"><i class="fa fa-eye-slash"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Tipo</label>
                                        <select class="form-control" name="tipo" id="tipo_perfil" required="required">
                                            <option value="">.::Seleccione::.</option>
                                            <option value="comandante">Comandante</option>
                                            <option value="parquero">Parquero</option>
                                            <option value="jefe">Jefe</option>
                                            <option value="analista sistema">Analista Sistema</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Usuario</label>
                                        <input class="form-control" type="text" name="usuario" id="usuario_perfil" required="required">
                                        <span class="eye" id="eyes-click-user" onclick="eyesPerfil('user')"><i class="fa fa-eye-slash"></i></span>
                                    </div>
                                </div>
                             </div>
                            <div class="col-lg-12">
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Clave</label>
                                        <input class="form-control" type="password" name="clave" id="clave_perfil" required="required">
                                        <span class="eye" id="eyes-click-clave" onclick="eyesPerfil('clave')"><i class="fa fa-eye-slash"></i></span> 
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                       <label>Repetir Clave</label>
                                       <input class="form-control" type="password" name="claver" id="claver_perfil" required="required">
                                       <span class="eye" id="eyes-click-claver" onclick="eyesPerfil('claver')"><i class="fa fa-eye-slash"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Pregunta Secreta</label>
                                        <input class="form-control" type="password" name="pregunta" id="pregunta_perfil" required="required">
                                        <span class="eye" id="eyes-click-pregunta" onclick="eyesPerfil('pregunta')"><i class="fa fa-eye-slash"></i></span>
                                    </div>
                                    
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Respuesta Secreta</label>
                                        <input class="form-control" type="password" name="respuesta" id="respuesta_perfil" required="required">
                                        <span class="eye" id="eyes-click-respuesta" onclick="eyesPerfil('respuesta')"><i class="fa fa-eye-slash"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 pull-right">
                                <input type="submit" name="btn-inicio" id="btn-inicio-perfil" class="btn btn-md btn-primary btn-block">

                            </div>
                            <div class="col-lg-6 pull-right">
                                <a href="javascript:;" class="btn btn-md btn-default btn-block" onclick="clearFormPerfil()"> Cancelar
                                </a>
                            </div>
                        </form>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal add usuario-->