<style>
    .eye {
        right: 0;
        position: absolute;
        margin-top: -28px;
        margin-right: -5px;
        font-size: large;
        cursor:pointer;
        color: #ccc;
    }
</style>
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list"></i> <b>información Personal</b>
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="well">
                    <h4>Importante!</h4>
                    <p id="texto">Aqui puedes editar la información de tu perfil!</p>
                </div>
            </div>
            <input type="hidden" name="perfilError"  value="<?=$perfil?>" id="perfilError">
            <form role="form" method="post" action="<?=base_url()?>profile">
                <input type="hidden" name="accion"  value="actualizar">
                <input type="hidden" name="idusuario"  value="<?=$user->idusuario?>" id="idusuario_perfil">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nombres</label>
                            <input class="form-control" onkeypress="return keyLetter(event)" type="text" name="nombre"  value="<?=$user->nombres?>" id="nombre_perfil" required="required" style="text-transform: uppercase">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Apellidos</label>
                            <input class="form-control" onkeypress="return keyLetter(event)" type="text" name="apellido"  value="<?=$user->apellidos?>" id="apellido_perfil" required="required" style="text-transform: uppercase">
                        </div> 
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label>Doc</label>
                            <select class="form-control" name="documento"  value="" id="documento_perfil" required="required">
                                <option value="<?=$user->documento?>"><?=$user->documento?></option>
                                <option value="V">V</option>
                                <option value="J">J</option>
                                <option value="E">E</option>
                                <option value="G">G</option>
                                <option value="R">R</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Cedula</label>
                            <input class="form-control numeric" type="text" name="cedula"  value="<?=$user->cedula?>" id="cedula_perfil" required="required">
                        </div>
                    </div>
                        <div class="col-lg-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input class="form-control" type="password" name="email"  value="<?=$user->email?>" id="email" required="required" style="text-transform: uppercase">
                            <span class="eye" id="eyes-click-email"  onclick="eyesPerfil('email')"><i class="fa fa-eye-slash"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Tipo</label>
                            <select class="form-control" name="tipo"  value="" id="tipo_perfil" required="required" style="text-transform: uppercase">
                                <option value="<?=$user->tipo?>"><?=$user->tipo?></option>
                                <option value="comandante">Comandante</option>
                                <option value="parquero">Parquero</option>
                                <option value="jefe">Jefe</option>
                                <option value="analista sistema">Analista Sistema</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Usuario</label>
                            <input class="form-control" type="password" name="usuario"  value="<?=$user->nombreusuario?>" id="usuario" required="required" style="text-transform: uppercase">
                            <span class="eye" id="eyes-click-user" onclick="eyesPerfil('user')"><i class="fa fa-eye-slash"></i></span>
                        </div>
                    </div>
                    </div>
                <div class="col-lg-12">
                        <div class="col-lg-6">
                        <div class="form-group">
                            <label>Clave</label>
                            <input class="form-control" onkeypress="return keyPassword(event)" type="password" name="clave"  value="" id="clave" required="required">
                            <span class="eye" id="eyes-click-clave" onclick="eyesPerfil('clave')"><i class="fa fa-eye-slash"></i></span> 
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Repetir Clave</label>
                            <input class="form-control" onkeypress="return keyPassword(event)" type="password" name="claver"  value="" id="claver" required="required">
                            <span class="eye" id="eyes-click-claver" onclick="eyesPerfil('claver')"><i class="fa fa-eye-slash"></i></span>
                        </div>
                    </div>
                </div>
                <br>
                <div class="col-lg-12">
                <div class="well">
                    <h5>La contraseña debería cumplir con los siguientes requerimientos:</h5>
                        <ul>
                            <li id="letter">Al menos debería tener <strong>una letra</strong></li>
                            <li id="capital">Al menos debería tener <strong>una letra en mayúsculas</strong></li>
                            <li id="number">Al menos debería tener <strong>un número</strong></li>
                            <li id="length">Debería tener <strong>8 carácteres</strong> como mínimo</li>
                            <li id="caracter">Debería tener <strong>1 carácteres (solo se aceptan: .,%)</strong> como mínimo</li>
                        </ul>
                </div>
                </div>

                <div class="col-lg-12">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Pregunta Secreta</label>
                            <input class="form-control" onkeypress="return keyLetter(event)" type="password" name="pregunta"  value="<?=$user->pregunta?>" id="pregunta" required="required">
                            <span class="eye" id="eyes-click-pregunta" onclick="eyesPerfil('pregunta')"><i class="fa fa-eye-slash"></i></span>
                        </div>
                        
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Respuesta Secreta</label>
                            <input class="form-control" onkeypress="return keyLetter(event)" type="password" name="respuesta"  value="<?=$user->respuesta?>" id="respuesta" required="required">
                            <span class="eye" id="eyes-click-respuesta" onclick="eyesPerfil('respuesta')"><i class="fa fa-eye-slash"></i></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 pull-right">
                    <input type="submit" name="btn-inicio" value ="Actualizar" id="btn-inicio-perfil" class="btn btn-md btn-primary btn-block">

                </div>
               <!--  <div class="col-lg-3 pull-right">
                    <a href="<?=base_url()?>dashboard" class="btn btn-md btn-default btn-block"> Cancelar
                    </a>
                </div> -->
            </form>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
    <!-- /.panel-body -->
    </div>
<!-- /.panel -->
</div>
