    <style>
        .eye {
            right: 0;
            position: absolute;
            margin-top: -28px;
            margin-right: -5px;
            font-size: large;
            cursor:pointer;
            color: #ccc;
        }
    </style>
    <!-- Modal add usuario-->
     <div class="modal fade" data-backdrop="false" id="<?=$idmodal?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-12">
                            <div class="well">
                                <h4>Importante!</h4>
                                <p id="texto">Aqu&iacute; puedes agregar la informaci&oacute;n del nuevo usuario!</p>
                            </div>
                        </div>
                         <ul class="nav nav-tabs">
                            <li class="active"><a href="#informacion" data-toggle="tab" aria-expanded="true"><b>Informaci&oacute;n</b></a>
                            </li>
                            <li class=""><a href="#modulos" data-toggle="tab" aria-expanded="false"><b>Modulos</b></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="informacion">
                                <div class="row">
                                    <div class="col-md-12">
                                    <br>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <i class="fa fa-users"></i> <b>Usuario</b>
                                        </div>
                                    <div class="panel-body">
                                       <form role="form" method="post" id="form-add-user">
                                            <input type="hidden" name="idusuario" id="idusuario">
                                            <input type="hidden" name="conectado" id="conectado" value="<?=$tipo?>">
                                            <div class="col-lg-12">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Nombres</label>
                                                        <input class="form-control" onkeypress="return keyLetter(event)" type="text" name="nombre" id="nombre" required="required">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Apellidos</label>
                                                        <input class="form-control" onkeypress="return keyLetter(event)" type="text" name="apellido" id="apellido" required="required">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label>Doc</label>
                                                        <select class="form-control" name="documento" id="documento" required="required">
                                                            <option value="">----</option>
                                                            <option value="V">V</option>
                                                            <option value="E">E</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>C&eacute;dula</label>
                                                        <input class="form-control numeric" type="text" name="cedula" maxlength="8"  id="cedula" required="required">
                                                    </div>
                                                </div>
                                                 <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input class="form-control" type="password" name="email" id="email" required="required">
                                                        <span class="eye" id="eyes-click-email"  onclick="eyes('email')"><i class="fa fa-eye-slash"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Tipo</label>
                                                        <select class="form-control" name="tipo" id="tipo" required="required">
                                                            <option value="">.::Seleccione::.</option>
                                                            <option value="comandante">Comandante</option>
                                                            <option value="parquero">Parquero</option>
                                                            <option value="jefe">Jefe</option>
                                                            <option value="analista sistema">Analista Sistema</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Usuario</label>
                                                        <input class="form-control" type="password" name="usuario" id="usuario" required="required">
                                                        <span class="eye" id="eyes-click-user" onclick="eyes('user')"><i class="fa fa-eye-slash"></i></span>
                                                    </div>
                                                </div>
                                             </div>
                                            <div class="col-lg-12">
                                                 <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="clave">Clave</label>
                                                            <input class="form-control" onkeypress="return keyPassword(event)" type="password" name="clave" id="clave" required="required">
                                                            <span class="eye" id="eyes-click-clave" onclick="eyes('clave')"><i class="fa fa-eye-slash"></i></span> 
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                       <label>Repetir Clave</label>
                                                       <input class="form-control" onkeypress="return keyPassword(event)" type="password" name="claver" id="claver" required="required">
                                                       <span class="eye" id="eyes-click-claver" onclick="eyes('claver')"><i class="fa fa-eye-slash"></i></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Pregunta Secreta</label>
                                                        <input class="form-control" onkeypress="return keyLetter(event)" type="password" name="pregunta" id="pregunta" required="required">
                                                        <span class="eye" id="eyes-click-pregunta" onclick="eyes('pregunta')"><i class="fa fa-eye-slash"></i></span>
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Respuesta Secreta</label>
                                                        <input class="form-control" onkeypress="return keyLetter(event)" type="password" name="respuesta" id="respuesta" required="required">
                                                        <span class="eye" id="eyes-click-respuesta" onclick="eyes('respuesta')"><i class="fa fa-eye-slash"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="well">
                                                    <h5>La contrase&ntilde;a deber&iacute;a cumplir con los siguientes requerimientos:</h5>
                                                    <ul>
                                                        <li id="letter">Al menos deber&iacute;a tener <strong>una letra.</strong></li>
                                                        <li id="capital">Al menos deber&iacute;a tener <strong>una letra en may&uacute;sculas.</strong></li>
                                                        <li id="number">Al menos deber&iacute;a tener <strong>un n&uacute;mero.</strong></li>
                                                        <li id="length">Deber&iacute;a tener <strong>8 car&aacute;cteres</strong> como m&iacute;nimo.</li>
                                                        <li id="caracter">Deber&iacute;a tener <strong>1 car&aacute;cteres (solo se aceptan: .,%)</strong> como m&iacute;nimo.</li>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                            <!-- .panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="modulos">
                               <div class="row">
                                     <div class="col-lg-12">
                                        <br>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <i class="fa fa-cogs"></i> <b>Modulos de Acceso al Sistema</b>
                                                <label class="checkbox-inline pull-right">
                                                    <input type="checkbox" id="allCheckbox" onclick="allCheckbox()">Todos
                                                </label>
                                            </div>
                                            <div class="panel-body">
                                                <?php  foreach ($modulos as $modulo): ?>
                                                    <?php if ($modulo->estatus!=1): ?>
                                                        <div class="col-md-4">
                                                        <!-- /.panel-heading -->
                                                            <div class="form-group">
                                                                <label class="checkbox-inline">
                                                                <?php if ($tipo == "analista sistema"):?>
                                                                    <input type="checkbox" name="modulosUser[]" value="<?=$modulo->idmodulo?>" id="modulo<?=$modulo->idmodulo?>"><i class="fa fa-<?=$modulo->iconomodulo?>"></i> <?=$modulo->nombremodulo?>
                                                                <?php else:?>
                                                                    <fieldset disabled>
                                                                        <input type="checkbox" name="modulosUser[]" value="<?=$modulo->idmodulo?>" id="modulo<?=$modulo->idmodulo?>"><i class="fa fa-<?=$modulo->iconomodulo?>"></i> <?=$modulo->nombremodulo?>
                                                                    </fieldset>
                                                                <?php endif;?>
                                                                </label>
                                                            </div>
                                                         </div>
                                                    <?php endif;?>
                                                <?php  endforeach; ?>
                                                <!-- .panel-body -->
                                            </div>
                                            <!-- .panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                </div>
                            </div>

                         <hr>
                         <div class="col-lg-6 pull-right">
                            <input type="submit" name="btn-inicio" id="btn-inicio-agregar" class="btn btn-md btn-primary btn-block">

                        </div>
                        <div class="col-lg-6 pull-right">
                            <a href="javascript:;" class="btn btn-md btn-default btn-block" onclick="clearForm()"> Cancelar
                            </a>
                        </div>
                        </form>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal add usuario-->

     <!-- Modal usuario estatus-->
    <div class="modal fade" id="modal-form-estatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><br></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="idusuario" id="idusuario">¿Desea, Cambiar el Estatus de Usuario?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" onclick="updateUserEstatus()">Ok</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal add estatus-->
     <!-- /.row -->
    <div class="row">
        <!-- /.col-lg-6 -->
        <div class="col-lg-12" id="refreshUsers"></div>
        <!-- /.col-lg-6 -->
    </div>
    <!-- /.row -->