<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?=base_url('public/img/favicon.ico')?>" type="image/x-icon">
        <title><?=$title?></title>

        <!-- Bootstrap Core CSS -->
        <link href="<?=base_url('public/css/bootstrap.min.css')?>" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?=base_url('public/css/metisMenu.min.css')?>" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?=base_url('public/css/startmin.css')?>" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?=base_url('public/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
        
        <!-- Select2 plugins -->
        <link rel="stylesheet" type="text/css" href="<?=base_url('public/select/css/select2.css')?>">
        <link rel="stylesheet" type="text/css" href="<?=base_url('public/select/css/select2-bootstrap.css')?>">

        <!-- App style -->
        <link rel="stylesheet" type="text/css" href="<?=base_url('public/css/app.css')?>">

        <!-- HTML5 Shim and Respond.js')?> IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js')?> doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js')?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js')?>/1.4.2/respond.min.js')?>"></script>
        <![endif]-->
        <style type="text/css" media="screen">
            .btn-primary {
                color: #fff !important;
                background-color: #e61010 !important;
                border-color: #d20d0d !important;
            }

            .btn-primary:hover {
                color: #fff !important;
                background-color: #e61010 !important;
            }

            .btn-default {
                color: #333;
                background-color: #ddd;
                border-color: #ccc;
            }

            .eye-login-password {
                right: 0;
                position: absolute;
                margin-top: -28px;
                margin-right: -5px;
                font-size: large;
                cursor:pointer;
                color: #ccc;
            }

        </style>
    </head>
    <body>
        <div id="msj_exito"><div class="alert alert-success"></div></div>
        <div id="msj_error"><div class="alert alert-danger"></div></div>
        <div id="msj_advertencia"><div class="alert alert-warning"></div></div>
        <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url();?>">
        <div class="divCarga" id="div_loading">
            <div class="fontmensaje" style="color: #ffffff; margin-top:250px;" id="mensajetextoCarga"></div>
            <img id="imgtextocarga" style="margin-top:10px;  width: 30px;" src="<?=base_url('public/img/loading_trans.gif')?>">    
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading" align="center">
                            <br>
                        </div>
                        <div class="panel-body">
                            <img src="<?=base_url('public/img/banner.jpg')?>" class="img-responsive">
                             <div class="col-md-offset-1 col-md-10 ">
                                 <form role="form" method="post" action="" id="form-login-password" autocomplete="off" data-return="">
                                    <fieldset>
                                        <br>
                                        <div class="form-group">
                                            <div class="well">
                                                <h3 class="panel-title" align="center"><i class="fa fa-lock "></i> <b>Recuperar Clave de Acceso</b></h3>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Email Personal</label>
                                            <input class="form-control" placeholder="" name="email" type="email" autofocus id="email" required="required">
                                        </div>
                                        <div class="form-group">
                                            <label>Pregunta de Seguridad</label>
                                            <input class="form-control" onkeypress="return keyLetter(event)" placeholder="" name="pregunta" type="password" autofocus id="pregunta" required="required">
                                            <span class="eye-login-password" id="eyes-click-pregunta" onclick="eyesPassword('pregunta')"><i class="fa fa-eye-slash"></i></span> 
                                        </div>
                                        <div class="form-group">
                                            <label>Respuesta de Seguridad</label>
                                            <input class="form-control" onkeypress="return keyLetter(event)" placeholder="" name="respuesta" type="password" value="" id="respuesta" required="required">
                                            <span class="eye-login-password" id="eyes-click-respuesta" onclick="eyesPassword('respuesta')"><i class="fa fa-eye-slash"></i></span> 
                                        </div>
                                        <div class="form-group">
                                            <label>Nueva Clave de Seguridad</label>
                                            <input class="form-control" onkeypress="return keyPassword(event)" placeholder="" name="clave" type="password" value="" id="clave" required="required">
                                            <span class="eye-login-password" id="eyes-click-clave" onclick="eyesPassword('clave')"><i class="fa fa-eye-slash"></i></span> 
                                        </div>

                                        <br>
                                        <div class="well">
                                            <h5>La contrase&ntilde;a deber&iacute;a cumplir con los siguientes requerimientos:</h5>
                                               <ul>
                                                    <li id="letter">Al menos deber&iacute;a tener <strong>una letra.</strong></li>
                                                    <li id="capital">Al menos deber&iacute;a tener <strong>una letra en may&uacute;sculas.</strong></li>
                                                    <li id="number">Al menos deber&iacute;a tener <strong>un n&uacute;mero.</strong></li>
                                                    <li id="length">Deber&iacute;a tener <strong>8 car&aacute;cteres</strong> como m&iacute;nimo.</li>
                                                    <li id="caracter">Deber&iacute;a tener <strong>1 car&aacute;cteres (solo se aceptan: .,%)</strong> como m&iacute;nimo.</li>
                                                </ul>
                                        </div>
                                        <!-- Change this to a button or input when using this as a form -->
                                        <input type="submit" name="btn-inicio" id="btn-inicio-recuperar" class="btn btn-lg btn-primary btn-block" value="Recuperar">
                                        <br>
                                         <a href="javascript:;" id="btn-regresar" class="btn btn-lg btn-default btn-block">
                                            Regresar
                                        </a>

                                    </fieldset>
                                </form>
                                
                                <br>
                                    <div class="well">
                                        <h4><b>Importante!</b></h4>
                                        <p id="texto"><b>Si no puedes recuperar su clave para su usuario, pongase en contacto con el analista de sistema.</b></p>
                                    </div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery -->
        <script src="<?=base_url('public/js/jquery.min.js')?>"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?=base_url('public/js/bootstrap.min.js')?>"></script>

        <!-- Select2 Core JavaScript -->
        <script type="text/javascript" src="<?=base_url('public/select/js/select2.js')?>"></script>
        
        <!-- App Core JavaScript -->
        <script type="text/javascript" src="<?=base_url('public/js/app.js')?>"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?=base_url('public/js/metisMenu.min.js')?>"></script>

        <!-- Custom Theme JavaScript -->
        <script src="<?=base_url('public/js/startmin.js')?>"></script>
        <script type="text/javascript" charset="utf-8" async defer>
        $(document).ready(function(){

            $("#form-login-password").submit(function(){
                open_loading();
                    var pregunta = $("#pregunta").val();
                    var respuesta = $("#respuesta").val();
                    var clave = $("#clave").val();
                    var email = $("#email").val();
                    var url = $("#base_url").val();
               
                    $.post(base_url()+"/auth/sendpassword/", {"pregunta":pregunta, "respuesta":respuesta, "clave":clave, "email":email}, function(data){
                        var resp = data.split("::");
                        if(data == "exito"){
                            msj_exito("La Clave de Seguridad ha sido Recuperada Exitosamente", 3000);
                            setTimeout(function () {location.href = url}, 2000)
                        }else if(data == "error"){
                            msj_error("Error, Intente mas Tarde", 3000);
                        }else if(data == "info"){
                            msj_advertencia("La Informacion Suministrada es incorrecta", 3000); 
                        }
                        close_loading();
                    });
                    
                
                return false;
            });
        });

        function eyesPassword(o) {
            var email = $("#eyes-click-email").html();
            var user = $("#eyes-click-user").html();
            var c1 = $("#eyes-click-clave").html();
            var c2 = $("#eyes-click-claver").html();
            var p = $("#eyes-click-pregunta").html();
            var r = $("#eyes-click-respuesta").html();
            
            if(o=="email"){
                if(email=='<i class="fa fa-eye-slash"></i>'){
                    $("#eyes-click-email").html('<i class="fa fa-eye"></i>');
                    $("#email").removeAttr('type', 'password');
                    $("#email").attr('type', 'text');
                }else{
                    $("#eyes-click-email").html('<i class="fa fa-eye-slash"></i>');
                    $("#email").removeAttr('type', 'text');
                    $("#email").attr('type', 'password');
                }
            }

            if(o=="user"){
                if(user=='<i class="fa fa-eye-slash"></i>'){
                    $("#eyes-click-user").html('<i class="fa fa-eye"></i>');
                    $("#usuario").removeAttr('type', 'password');
                    $("#usuario").attr('type', 'text');
                }else{
                    $("#eyes-click-user").html('<i class="fa fa-eye-slash"></i>');
                    $("#usuario").removeAttr('type', 'text');
                    $("#usuario").attr('type', 'password');
                }
            }

            if(o=="clave"){
                if(c1=='<i class="fa fa-eye-slash"></i>'){
                    $("#eyes-click-clave").html('<i class="fa fa-eye"></i>');
                    $("#clave").removeAttr('type', 'password');
                    $("#clave").attr('type', 'text');
                }else{
                    $("#eyes-click-clave").html('<i class="fa fa-eye-slash"></i>');
                    $("#clave").removeAttr('type', 'text');
                    $("#clave").attr('type', 'password');
                }
            }

            if(o=="claver"){
                if(c2=='<i class="fa fa-eye-slash"></i>'){
                    $("#eyes-click-claver").html('<i class="fa fa-eye"></i>');
                    $("#claver").removeAttr('type', 'password');
                    $("#claver").attr('type', 'text');
                }else{
                    $("#eyes-click-claver").html('<i class="fa fa-eye-slash"></i>');
                    $("#claver").removeAttr('type', 'text');
                    $("#claver").attr('type', 'password');
                }
            }

            if(o=="pregunta"){
                if(p=='<i class="fa fa-eye-slash"></i>'){
                    $("#eyes-click-pregunta").html('<i class="fa fa-eye"></i>');
                    $("#pregunta").removeAttr('type', 'password');
                    $("#pregunta").attr('type', 'text');
                }else{
                    $("#eyes-click-pregunta").html('<i class="fa fa-eye-slash"></i>');
                    $("#pregunta").removeAttr('type', 'text');
                    $("#pregunta").attr('type', 'password');
                }
            }

            if(o=="respuesta"){
                if(r=='<i class="fa fa-eye-slash"></i>'){
                    $("#eyes-click-respuesta").html('<i class="fa fa-eye"></i>');
                    $("#respuesta").removeAttr('type', 'password');
                    $("#respuesta").attr('type', 'text');
                }else{
                    $("#eyes-click-respuesta").html('<i class="fa fa-eye-slash"></i>');
                    $("#respuesta").removeAttr('type', 'text');
                    $("#respuesta").attr('type', 'password');
                }
            }
        }
        </script>
    </body>
</html>