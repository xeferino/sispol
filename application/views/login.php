<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?=base_url('public/img/favicon.ico')?>" type="image/x-icon">
        <title><?=$title?></title>

        <!-- Bootstrap Core CSS -->
        <link href="<?=base_url('public/css/bootstrap.min.css')?>" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?=base_url('public/css/metisMenu.min.css')?>" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?=base_url('public/css/startmin.css')?>" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?=base_url('public/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
        
        <!-- Select2 plugins -->
        <link rel="stylesheet" type="text/css" href="<?=base_url('public/select/css/select2.css')?>">
        <link rel="stylesheet" type="text/css" href="<?=base_url('public/select/css/select2-bootstrap.css')?>">

        <!-- App style -->
        <link rel="stylesheet" type="text/css" href="<?=base_url('public/css/app.css')?>">

        <!-- HTML5 Shim and Respond.js')?> IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js')?> doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js')?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js')?>/1.4.2/respond.min.js')?>"></script>
        <![endif]-->
        <style type="text/css" media="screen">
            .btn-primary {
                color: #fff !important;
                background-color: #e61010 !important;
                border-color: #d20d0d !important;
            }

            .btn-primary:hover {
                color: #fff !important;
                background-color: #e61010 !important;
            }

            .btn-default {
                color: #333;
                background-color: #ddd;
                border-color: #ccc;
            }
            .eye-login {
                right: 0;
                position: absolute;
                margin-top: -28px;
                margin-right: -5px;
                font-size: large;
                cursor:pointer;
                color: #ccc;
            }
        </style>
    </head>
    <body>
        <div id="msj_exito"><div class="alert alert-success"></div></div>
        <div id="msj_error"><div class="alert alert-danger"></div></div>
        <div id="msj_advertencia"><div class="alert alert-warning"></div></div>
        <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url();?>">
        <div class="divCarga" id="div_loading">
            <div class="fontmensaje" style="margin-top:250px;"></div>
            <img id="imgtextocarga" style="margin-top:10px;  width: 100px;" src="<?=base_url('public/img/loading_trans.gif')?>">    
        </div>

        <div class="container">
            <div class="row" style="margin-top: -70px;">
                <div class="col-md-6 col-md-offset-3">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading" align="center">
                            <br>
                        </div>
                        <div class="panel-body">
                            <img src="<?=base_url('public/img/banner.jpg')?>" class="img-responsive">
                             <div class="col-md-offset-1 col-md-10 ">
                                <!-- <?=base_url('index.php/auth/login/')?> -->
                                 <form role="form" method="post" action="" id="form-login" autocomplete="off" data-return="">
                                    <fieldset>
                                        <br>
                                        <div class="form-group">
                                            <div class="well">
                                                <h3 class="panel-title" align="center"><i class="fa fa-lock "></i> <b>Ingrese sus Datos de Acceso</b></h3>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Usuario</label>
                                            <input class="form-control" placeholder="Usuario" name="usuario" type="text" autofocus id="usuario">
                                        </div>
                                        <div class="form-group">
                                            <label>Clave</label>
                                            <input class="form-control" placeholder="Password" name="clave" type="password" value="" id="clave">
                                            <span class="eye-login" id="eyes-click-clave" onclick="eyesLogin('clave')"><i class="fa fa-eye-slash"></i></span> 
                                        </div>
                                        <!-- Change this to a button or input when using this as a form -->
                                        <input type="submit" name="btn-inicio" id="btn-inicio" class="btn btn-lg btn-primary btn-block" value="Ingresar">

                                    </fieldset>
                                </form>
                             </div>
                        </div>
                            <div align="center">
                                 <a href="<?=base_url()?>auth/recuperarpassword">
                                   ¿Recuperar Contraseña, Pregunta de Seguridad?
                                </a>
                                <br><br>
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery -->
        <script src="<?=base_url('public/js/jquery.min.js')?>"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?=base_url('public/js/bootstrap.min.js')?>"></script>

        <!-- Select2 Core JavaScript -->
        <script type="text/javascript" src="<?=base_url('public/select/js/select2.js')?>"></script>
        
        <!-- App Core JavaScript -->
        <script type="text/javascript" src="<?=base_url('public/js/app.js')?>"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?=base_url('public/js/metisMenu.min.js')?>"></script>

        <!-- Custom Theme JavaScript -->
        <script src="<?=base_url('public/js/startmin.js')?>"></script>
        <script type="text/javascript" charset="utf-8" async defer>
        $(document).ready(function(){
            /**
             *  Cargar submit general para los formularios
             */
            $("#form-login").submit(function(){
                open_loading();
                    var usuario = $("#usuario").val();
                    var clave = $("#clave").val();
                    var url = $("#base_url").val();
               
                    if(usuario == '' || clave == '' ){
                        msj_advertencia("Disculpe, Usuario y Clave son Requeridos", 3000);  
                    }else{
                          $.post(base_url()+"/auth/login/", {"usuario":usuario, "clave":clave}, function(data){
                            var resp = data.split("::");
                            if(resp[0] == "exito"){
                                msj_exito("Sesión iniciada correctamente", 3000);
                                setTimeout(function () {location.href = url+""+resp[1]}, 3000);
                            }else if(resp[0] == "error"){
                                msj_error("Usuario o Contraseña incorrecta", 3000);
                            }else if(resp[0] == "bloqueado"){
                                msj_advertencia("Usuario Bloqueado", 3000); 
                            
                            }
                        });
                    }
                close_loading();
                return false;
            });
        });

        function eyesLogin(o) {
            var c1 = $("#eyes-click-clave").html();
            if(o=="clave"){
                if(c1=='<i class="fa fa-eye-slash"></i>'){
                    $("#eyes-click-clave").html('<i class="fa fa-eye"></i>');
                    $("#clave").removeAttr('type', 'password');
                    $("#clave").attr('type', 'text');
                }else{
                    $("#eyes-click-clave").html('<i class="fa fa-eye-slash"></i>');
                    $("#clave").removeAttr('type', 'text');
                    $("#clave").attr('type', 'password');
                }
            }
        }
        </script>
    </body>
</html>