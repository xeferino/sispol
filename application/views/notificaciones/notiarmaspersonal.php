<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list"></i> <b>Listado de Funcionario con Armamentos Pendientes por Entregar</b>
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
       <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover" id="dataTables-notificaciones-armas">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Asignaci&oacute;n</th>
                    <th>Devoluci&oacute;n</th>
                    <th>T. de Retraso</th>
                    <th>Armas</th>
                    <th>Funcionario</th>
                    <th>Estatus</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $ci =& get_instance();
                $ci->load->model(array("armas_model", "personal_model"));
                foreach ($movimientos as $data): 
                    $personal = $ci->personal_model->get($data->idpersonal);
                    if ($data->tiempoRetraso!="" and $data->tiempoRetraso>="0"){
                        $retraso = explode(":",$data->tiempoRetraso);
                        $tiempo = $retraso[0]." H ".$retraso[1]." M ".$retraso[2]." S"; 
                    }else{
                        $tiempo = "--------------";
                    }
                    
                                 
                ?>
                    <tr class="odd gradeX">
                        <td> <?= $data->idpersonalarmamento ?></td>
                        <td> <?= $data->fechaasignacion." ".$data->horaasignacion ?></td>
                        <td> <?= $data->fechadevolucion." ".$data->horadevolucion ?></td>
                        <td> <?= $tiempo ?></td>
                        <td>
                            <?php
                                  $armamentos = explode(',', $data->idarmas);
                                  foreach ($armamentos as $keya => $asignadas) {
                                    $arma = $ci->armas_model->get($asignadas);

                                        $cantArmas = explode(',', $data->cantarmas);
                                        foreach ($cantArmas as $keyc => $cantidad) {
                                            if($keya==$keyc){
                                                $calibre = "";
                                                if($arma->calibre!=""){
                                                    $calibre =" Calibre: ".$arma->calibre;
                                                }
                            ?>
                                <button class="btn btn-xs btn-default" title="<?=$arma->tipo."".$calibre." Cantidad: ".$cantidad?>"><?=$arma->codigo?></button><br>
                             <?php
                                            }
                                        }
                                    }
                             ?>
                        </td>
                        <td> <?= "(".$personal->documento."-".$personal->cedula.") ".$personal->nombres." ".$personal->apellidos." Rango: ".strtoupper($personal->rango)?></td>
                         <td> <?php if ($data->estatusarma == "Pendiente" or $data->estatusarma == "Vencido"):?>
                                        <a href="javascript:;" class="btn btn-primary btn-xs">
                                            <?=$data->estatusarma?>
                                        </a>
                                        <?php else:?>
                                            <a href="javascript:;" class="btn btn-success btn-xs">
                                               <?=$data->estatusarma?>
                                            </a>
                                        <?php endif;?>
                        </td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
