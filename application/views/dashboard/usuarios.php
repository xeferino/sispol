    <!-- Modal usuario estatus-->
    <div class="modal fade" id="modal-info-usuarios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><br></h4>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-list"></i> <b>Listado de Usuarios</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-usuarios">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombres y Apellidos</th>
                                            <th>Cedula</th>
                                            <th>Tipo</th>
                                            <th>Estatus</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php  foreach ($usuarios as $usuario): ?>
                                            <tr class="odd gradeX">
                                                <td><?=$usuario->idusuario?></td>
                                                <td><?=$usuario->nombres." ".$usuario->apellidos?></td>
                                                <td><?=$usuario->documento."-".$usuario->cedula?></td>
                                                <td><?=strtoupper($usuario->tipo)?></td>
                                                <td align="center">
                                                    <?php if ($usuario->estatususuario == "1"):?>
                                                        <a href="javascript:;" class="btn btn-primary btn-xs">
                                                            Bloqueado
                                                        </a>
                                                        <?php else:?>
                                                            <a href="javascript:;" class="btn btn-success btn-xs">
                                                                Activo
                                                            </a>
                                                        <?php endif;?>

                                                    </td>
                                            </tr>
                                            <?php  endforeach; ?> 
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal add estatus-->