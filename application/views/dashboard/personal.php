    <!-- Modal usuario estatus-->
    <div class="modal fade" id="modal-info-personal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><br></h4>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-list"></i> <b>Listado de Personal</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-personal">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombres y Apellidos</th>
                                            <th>Cedula</th>
                                            <th>Placa</th>
                                            <th>Rango</th>
                                            <th>Dirección</th>
                                            <th>Telefonos</th>
                                            <th>Estatus</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php  foreach ($personal as $persona): ?>
                                            <tr class="odd gradeX">
                                                <td><?=$persona->idpersonal?></td>
                                                <td><?=$persona->nombres." ".$persona->apellidos?></td>
                                                <td><?=$persona->documento."-".$persona->cedula?></td>
                                                <td><?=$persona->placa?></td>
                                                <td><?=strtoupper($persona->rango)?></td>
                                                <td><?=$persona->direccion?></td>
                                                <td><?=$persona->telefono_habitacion."<br>".$persona->telefono_celular?></td>
                                                <td align="center">
                                                    <?php if ($persona->estatus == "1"):?>
                                                        <a href="javascript:;" class="btn btn-primary btn-xs title="Bloqueado">
                                                            Bloqueado
                                                        </a>
                                                        <?php else:?>
                                                            <a href="javascript:;" class="btn btn-success btn-xs title="Activo">
                                                               Activo
                                                            </a>
                                                        <?php endif;?>
                                                    </td>
                                                </tr>
                                            <?php  endforeach; ?> 
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal add estatus-->