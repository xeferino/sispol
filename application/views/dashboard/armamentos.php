    <!-- Modal usuario estatus-->
    <div class="modal fade" id="modal-info-armamentos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><br></h4>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-list"></i> <b>Listado de Armas</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-armas">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Código</th>
                                            <th>Tipo</th>
                                            <th>Calibre</th>
                                            <th>Descripción</th>
                                            <th>Cantidad</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php  foreach ($armamentos as $arma): ?>
                                            <tr class="odd gradeX">
                                                <td><?=$arma->idarma?></td>
                                                <td><?=$arma->codigo?></td>
                                                <td><?=$arma->tipo?></td>
                                                <td><?=$arma->calibre?></td>
                                                <td><?=$arma->descripcion?></td>
                                                <td>
                                                    <a href="javascript:;" class="btn btn-default btn-xs">
                                                        <?=$arma->cantidad?>
                                                    </a>
                                                </tr>
                                            <?php  endforeach; ?> 
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal add estatus-->