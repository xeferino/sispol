
    <style>
        .charts_container{
            width: 900px;
            height: 420px;
            margin: 10px auto;
            border: #f1f1f1 1px solid;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Control de Armamentos en el Parqu&iacute;metro General
                </div>
                    <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="charts_container">
                        <canvas id="chartCanvas1" width="900" height="400">
                            Your web-browser does not support the HTML 5 canvas element.
                        </canvas>
                    </div>
                </div>
                    <!-- /.panel-body -->
            </div>
                <!-- /.panel -->
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Cantidad de Funcionarios 
                </div>
                    <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="charts_container">
                        <canvas id="chartCanvas6" width="900" height="400">
                            Your web-browser does not support the HTML 5 canvas element.
                        </canvas>
                    </div>
                </div>
                    <!-- /.panel-body -->
            </div>
                <!-- /.panel -->
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Control de Armamentos en el Parqu&iacute;metro Tipo de Arma (Pistolas)
                </div>
                    <!-- /.panel-heading -->
                <div class="panel-body">
                <div class="charts_container">
                        <canvas id="chartCanvas2" width="900" height="400">
                            Your web-browser does not support the HTML 5 canvas element.
                        </canvas>
                    </div>
                </div>
                    <!-- /.panel-body -->
            </div>
                <!-- /.panel -->
        </div>


        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Control de Armamentos en el Parqu&iacute;metro Tipo de Arma (Escopetas)
                </div>
                    <!-- /.panel-heading -->
                <div class="panel-body">
                <div class="charts_container">
                        <canvas id="chartCanvas3" width="900" height="400">
                            Your web-browser does not support the HTML 5 canvas element.
                        </canvas>
                    </div>
                </div>
                    <!-- /.panel-body -->
            </div>
                <!-- /.panel -->
        </div>


        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Control de Equipos Policiales en el Parqu&iacute;metro (Esposas)
                </div>
                    <!-- /.panel-heading -->
                <div class="panel-body">
                <div class="charts_container">
                        <canvas id="chartCanvas7" width="900" height="400">
                            Your web-browser does not support the HTML 5 canvas element.
                        </canvas>
                    </div>
                </div>
                    <!-- /.panel-body -->
            </div>
                <!-- /.panel -->
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Control de Equipos Policiales en el Parqu&iacute;metro (Chalecos)
                </div>
                    <!-- /.panel-heading -->
                <div class="panel-body">
                <div class="charts_container">
                        <canvas id="chartCanvas8" width="900" height="400">
                            Your web-browser does not support the HTML 5 canvas element.
                        </canvas>
                    </div>
                </div>
                    <!-- /.panel-body -->
            </div>
                <!-- /.panel -->
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Control de Equipos Policiales en el Parqu&iacute;metro (Municiones)
                </div>
                    <!-- /.panel-heading -->
                <div class="panel-body">
                <div class="charts_container">
                        <canvas id="chartCanvas9" width="900" height="400">
                            Your web-browser does not support the HTML 5 canvas element.
                        </canvas>
                    </div>
                </div>
                    <!-- /.panel-body -->
            </div>
                <!-- /.panel -->
        </div>


        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Control de Armamentos y Equipos Policiales General de Movimientos
                </div>
                    <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="charts_container">
                        <canvas id="chartCanvas4" width="900" height="400">
                            Your web-browser does not support the HTML 5 canvas element.
                        </canvas>
                    </div>
                </div>
                    <!-- /.panel-body -->
            </div>
                <!-- /.panel -->
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Control de Armamentos y Equipos Policiales General
                </div>
                    <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="charts_container">
                        <canvas id="chartCanvas5" width="900" height="400">
                            Your web-browser does not support the HTML 5 canvas element.
                        </canvas>
                    </div>

                </div>
                    <!-- /.panel-body -->
            </div>
                <!-- /.panel -->
        </div>
        
    </div>

     <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Movimientos de Salida de Armamentos TOP 10
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-dashboardSalida">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Asignación</th>
                                    <th>Devolución</th>
                                    <th>T. de Retraso</th>
                                    <th>Armas</th>
                                    <th>Funcionario</th>
                                    <th>Estatus</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $ci =& get_instance();
                                    $ci->load->model(array("armas_model", "personal_model"));
                                    foreach ($armaspersonaldev as $data): 
                                        //$armas = $ci->armas_model->get($data->idpersonal);
                                        $personal = $ci->personal_model->get($data->idpersonal);
                                        if ($data->tiempoRetraso!="" and $data->tiempoRetraso>="0"){
                                            $retraso = explode(":",$data->tiempoRetraso);
                                            $tiempo = $retraso[0]." H ".$retraso[1]." M ".$retraso[2]." S"; 
                                        }else{
                                            $tiempo = "--------------";
                                        }
                                ?>
                                <tr class="odd gradeX">
                                    <td> <?= $data->idpersonalarmamento ?></td>
                                    <td> <?= $data->fechaasignacion." ".$data->horaasignacion ?></td>
                                    <td> <?= $data->fechadevolucion." ".$data->horadevolucion ?></td>
                                    <td> <?= $tiempo ?></td>
                                    <td>
                                        <?php
                                              $armamentos = explode(',', $data->idarmas);
                                              foreach ($armamentos as $keya => $asignadas) {
                                                $arma = $ci->armas_model->get($asignadas);

                                                    $cantArmas = explode(',', $data->cantarmas);
                                                    foreach ($cantArmas as $keyc => $cantidad) {
                                                        if($keya==$keyc){
                                        ?>
                                            <button class="btn btn-xs btn-default" title="<?=$arma->tipo." Calibre: ".$arma->calibre." Cantidad: ".$cantidad?>"><?=$arma->codigo?></button><br>
                                         <?php
                                                        }
                                                    }
                                                }
                                         ?>
                                    </td>
                                    <td> <?= "(".$personal->documento."-".$personal->cedula.") ".$personal->nombres." ".$personal->apellidos." Rango: ".strtoupper($personal->rango)?></td>
                                   <td> <?php if($data->estatusarma=="Pendiente" or $data->estatusarma == "Vencido"):?>
                                            <a href="javascript:;" class="btn btn-primary btn-xs">
                                                <?=$data->estatusarma?>
                                            </a>
                                        <?php endif;?>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.tablse-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->

         <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Movimientos de Entrada de Armamentos TOP 10
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-dashboardEntrada">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Asignación</th>
                                    <th>Devolución</th>
                                    <th>T. de Retraso</th>
                                    <th>Armas</th>
                                    <th>Funcionario</th>
                                    <th>Estatus</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $ci =& get_instance();
                                    $ci->load->model(array("armas_model", "personal_model"));
                                    foreach ($armaspersonalent as $data): 
                                        //$armas = $ci->armas_model->get($data->idpersonal);
                                        $personal = $ci->personal_model->get($data->idpersonal);
                                        if ($data->tiempoRetraso!="" and $data->tiempoRetraso>="0"){
                                            $retraso = explode(":",$data->tiempoRetraso);
                                            $tiempo = $retraso[0]." H ".$retraso[1]." M ".$retraso[2]." S"; 
                                        }else{
                                            $tiempo = "--------------";
                                        }
                                ?>
                                <tr class="odd gradeX">
                                    <td> <?= $data->idpersonalarmamento ?></td>
                                    <td> <?= $data->fechaasignacion." ".$data->horaasignacion ?></td>
                                    <td> <?= $data->fechadevolucion." ".$data->horadevolucion ?></td>
                                    <td> <?= $tiempo ?></td>
                                    <td>
                                        <?php
                                              $armamentos = explode(',', $data->idarmas);
                                              foreach ($armamentos as $keya => $asignadas) {
                                                $arma = $ci->armas_model->get($asignadas);

                                                    $cantArmas = explode(',', $data->cantarmas);
                                                    foreach ($cantArmas as $keyc => $cantidad) {
                                                        if($keya==$keyc){
                                        ?>
                                            <button class="btn btn-xs btn-default" title="<?=$arma->tipo." Calibre: ".$arma->calibre." Cantidad: ".$cantidad?>"><?=$arma->codigo?></button><br>
                                         <?php
                                                        }
                                                    }
                                                }
                                         ?>
                                    </td>
                                    <td> <?= "(".$personal->documento."-".$personal->cedula.") ".$personal->nombres." ".$personal->apellidos." Rango: ".strtoupper($personal->rango)?></td>
                                   <td> <?php if($data->estatusarma == "Entregado"):?>
                                            <a href="javascript:;" class="btn btn-success btn-xs">
                                                <?=$data->estatusarma?>
                                            </a>
                                        <?php endif;?>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.tablse-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div id="getInfo"></div>

    <?php
        $entregado = 0;
        $pendiente = 0;
        $vencido = 0;
        if(count($armasMovimientos)>0){

            foreach ($armasMovimientos as $data){
                if ($data->estatusarma=="Entregado") {
                    $entregado++;
                }elseif ($data->estatusarma=="Pendiente") {
                    $pendiente++;
                }elseif ($data->estatusarma=="Vencido") {
                    $vencido++;
                }     
            }
        }

        $pistolasT  = 0;
        $pistolasA  = 0;
        $escopetasT = 0;
        $escopetasA = 0;
        $esposasT   = 0;
        $esposasA   = 0;
        $municionesT = 0;
        $municionesA = 0;
        $chalecosT = 0; 
        $chalecosA = 0;
        
        
        $pistolas9MMT  = 0;
        $pistolas9MMA  = 0;
        $pistolas12MMT  = 0;
        $pistolas12MMA  = 0;
        $escopetas9MMT = 0;
        $escopetas9MMA = 0;
        $escopetas12MMT = 0;
        $escopetas12MMA = 0;
        $municiones9MMT = 0;
        $municiones9MMA = 0;
        $municiones12MMT = 0;
        $municiones12MMA = 0;

        foreach ($armasEquipos as $key => $arma) {
            if($arma->tipo=="PISTOLA"){
                $pistolasT += $arma->cantidad;
                $pistolasA += $arma->cantidad_asignada;
            }elseif($arma->tipo=="ESCOPETA"){
                $escopetasT += $arma->cantidad;
                $escopetasA += $arma->cantidad_asignada;
            }elseif($arma->tipo=="ESPOSA"){
                $esposasT += $arma->cantidad;
                $esposasA += $arma->cantidad_asignada;
            }elseif($arma->tipo=="CHALECO"){
                $chalecosT += $arma->cantidad;
                $chalecosA += $arma->cantidad_asignada;
            }elseif($arma->tipo=="MUNICION"){
                $municionesT += $arma->cantidad;
                $municionesA += $arma->cantidad_asignada;
            }
            
            if($arma->tipo=="PISTOLA" and $arma->calibre=="9 MM"){
                $pistolas9MMT += $arma->cantidad;
                $pistolas9MMA += $arma->cantidad_asignada;
            }elseif($arma->tipo=="PISTOLA" and $arma->calibre=="12 MM"){
                $pistolas12MMT += $arma->cantidad;
                $pistolas12MMA += $arma->cantidad_asignada;
            }elseif($arma->tipo=="ESCOPETA" and $arma->calibre=="9 MM"){
                $escopetas9MMT += $arma->cantidad;
                $escopetas9MMA += $arma->cantidad_asignada;
            }elseif($arma->tipo=="ESCOPETA" and $arma->calibre=="12 MM"){
                $escopetas12MMT += $arma->cantidad;
                $escopetas12MMA += $arma->cantidad_asignada;
            }elseif($arma->tipo=="MUNICION" and $arma->calibre=="9 MM"){
                $municiones9MMT += $arma->cantidad;
                $municiones9MMA += $arma->cantidad_asignada;
            }elseif($arma->tipo=="MUNICION" and $arma->calibre=="12 MM"){
                $municiones12MMT += $arma->cantidad;
                $municiones12MMA += $arma->cantidad_asignada;
            }
        }


        $comjefe  = 0;
        $comagregado  = 0;
        $comisionado = 0;
        $supjefe = 0;
        $supagregado   = 0;
        $supervisor   = 0;
        $oficjefe = 0;
        $oficagregado = 0;
        $oficial = 0; 
        $permotorizado = 0;
        $jefeoperaciones = 0;
        $comsegundo = 0;
        $comandante = 0;
        foreach ($funcionarios as $key => $funcionario) {
            if($funcionario->rango=="comisionado jefe"){
                $comjefe++;
            }elseif($funcionario->rango=="comisionado agregado"){
                $comagregado++;
            }elseif($funcionario->rango=="comisionado"){
                $comisionado++;
            }elseif($funcionario->rango=="supervisor jefe"){
                $supjefe;
            }elseif($funcionario->rango=="supervisor agregado"){
                $supagregado++;
            }elseif($funcionario->rango=="supervisor"){
                $supervisor++;
            }elseif($funcionario->rango=="oficial jefe"){
                $oficjefe++;
            }elseif($funcionario->rango=="oficial agregado"){
                $oficagregado++;
            }elseif($funcionario->rango=="oficial"){
                $oficial++;
            }elseif($funcionario->rango=="personal motorizado"){
                $permotorizado++;
            }elseif($funcionario->rango=="jefe de operaciones"){
                $jefeoperaciones++;
            }elseif($funcionario->rango=="comandante segundo"){
                $comsegundo++;
            }elseif($funcionario->rango=="comandante"){
                $comandante++;
            }
        }
    ?>

<script type="application/javascript">
var chart1 = new AwesomeChart('chartCanvas1');
chart1.title = "Control de Armamentos General";
chart1.data = [<?=$pistolasT?>,<?=$pistolasA?>,<?=$escopetasT?>,<?=$escopetasA?>,<?=$municionesT?>,<?=$municionesA?>,<?=$esposasT?>,<?=$esposasA?>,<?=$chalecosT?>,<?=$chalecosA?>];
chart1.labels = ['Pistolas','Asignadas','Escopetas','Asignadas','Municiones','Asignadas','Esposas','Asignadas','Chalecos','Asignadas'];
//chart1.colors = ['#074dea', '#FF6600', '#074dea', '#FF6600', '#074dea', '#FF6600', '#074dea', '#FF6600', '#074dea', '#FF6600', ];
chart1.animate = true;
chart1.animationFrames = 30;
chart1.draw();

var chart1 = new AwesomeChart('chartCanvas7');
chart1.title = "Control de Equipos Policiales (Esposas)";
chart1.data = [<?=$esposasT?>,<?=$esposasA?>,<?=$esposasT-$esposasA?>];
chart1.labels = ['Esposas','Asignadas','Disponibles'];
//chart1.colors = ['#074dea', '#FF6600', '#074dea', '#FF6600', '#074dea', '#FF6600', '#074dea', '#FF6600', '#074dea', '#FF6600', ];
chart1.animate = true;
chart1.animationFrames = 30;
chart1.draw();

var chart1 = new AwesomeChart('chartCanvas8');
chart1.title = "Control de Equipos Policiales (Chalecos)";
chart1.data = [<?=$chalecosT?>,<?=$chalecosA?>,<?=$chalecosT-$chalecosA?>];
chart1.labels = ['Chalecos','Asignados','Disponibles'];
//chart1.colors = ['#074dea', '#FF6600', '#074dea', '#FF6600', '#074dea', '#FF6600', '#074dea', '#FF6600', '#074dea', '#FF6600', ];
chart1.animate = true;
chart1.animationFrames = 30;
chart1.draw();

var chart1 = new AwesomeChart('chartCanvas9');
chart1.title = "Control de Equipos Policiales (Municiones)";
chart1.data = [<?=$municiones9MMT?>,<?=$municiones9MMA?>,<?=$municiones9MMT-$municiones9MMA?>,<?=$municiones12MMT?>,<?=$municiones12MMA?>, <?=$municiones12MMT-$municiones12MMA?>];
chart1.labels = ['Municiones 9 MM','Asignadas  9 MM','Disponibles  9 MM','Municiones 12 MM','Asignadas  12 MM','Disponibles  12 MM'];
//chart1.colors = ['#074dea', '#FF6600', '#074dea', '#FF6600', '#074dea', '#FF6600', '#074dea', '#FF6600', '#074dea', '#FF6600', ];
chart1.animate = true;
chart1.animationFrames = 30;
chart1.draw();

var chart1 = new AwesomeChart('chartCanvas2');
chart1.title = "Control de Armamentos (Pistolas)";
chart1.data = [<?=$pistolas9MMT?>,<?=$pistolas9MMA?>,<?=$pistolas9MMT-$pistolas9MMA?>,<?=$pistolas12MMT?>,<?=$pistolas12MMA?>,<?=$pistolas12MMT-$pistolas12MMA?>];
chart1.labels = ['Pistolas 9 MM','Asignadas 9 MM', 'Disponibles 9 MM', 'Pistolas 12 MM','Asignadas 12 MM', 'Disponibles 12 MM'];
//chart1.colors = ['#074dea', '#FF6600','#34A038', '#074dea', '#FF6600', '#34A038'];
chart1.animate = true;
chart1.animationFrames = 30;
chart1.draw();

var chart1 = new AwesomeChart('chartCanvas3');
chart1.title = "Control de Armamentos (Escopetas)";
chart1.data = [<?=$escopetas9MMT?>,<?=$escopetas9MMA?>,<?=$escopetas9MMT-$escopetas9MMA?>,<?=$escopetas12MMT?>,<?=$escopetas12MMA?>, <?=$escopetas12MMT-$escopetas12MMA?>];
chart1.labels = ['Escopetas 9 MM','Asignadas 9 MM', 'Disponibles 9 MM', 'Escopetas 12 MM','Asignadas 12 MM', 'Disponibles 12 MM'];
//chart1.colors = ['#074dea', '#FF6600', '#34A038', '#074dea', '#FF6600', '#34A038'];
chart1.animate = true;
chart1.animationFrames = 30;
chart1.draw();

var chart1 = new AwesomeChart('chartCanvas4');
chart1.title = "Control General de Movimeintos de Armamentos y Equipos Policiales";
chart1.data = [<?=$entregado?>,<?=$pendiente?>,<?=$vencido?>];
chart1.labels = ['Entregado','Pendiente','Vencido'];
//chart1.colors = ['#34A038', '#FF6600'];
chart1.animate = true;
chart1.animationFrames = 30;
chart1.draw();

var chart13 = new AwesomeChart('chartCanvas5');
chart13.chartType = "pareto";
chart13.title = "Armamentos y Equipos Policiales General";
chart13.data = [<?=$pistolasT?>,<?=$escopetasT?>,<?=$municionesT?>,<?=$esposasT?>,<?=$chalecosT?>];
chart13.labels = ['Pistolas','Escopetas','Municiones','Esposas','Chalecos'];
//chart13.colors = ['#074dea', '#FF6600', '#074dea', '#FF6600', '#074dea'];
chart13.chartLineStrokeStyle = 'rgba(0, 0, 200, 0.5)';
chart13.chartPointFillStyle = 'rgb(0, 0, 200)';
chart13.draw();
var chart13 = new AwesomeChart('chartCanvas6');
chart13.chartType = "pareto";
chart13.title = "Cantidad General de Funcionarios Según su Rango";
chart13.data = [<?=$comjefe?>,<?=$comagregado?>,<?=$comisionado?>,<?=$supjefe?>,<?=$supagregado?>,<?=$supervisor?>,<?=$oficjefe?>,<?=$oficagregado?>,<?=$oficial?>,<?=$permotorizado?>, <?=$jefeoperaciones?>, <?=$comsegundo?>, <?=$comandante?>];
chart13.labels = ['Cdo. Jefe','Cdo. Agdo.','Comisionado','Sup. Jefe','Sup. Agdo.', 'Sup.', 'Ofic. Jefe', 'Ofic. Agdo.', 'Ofic.', 'P. Motdo.', 'Jefe de Ope.', 'Com. Seg.','Comandante'];                                                                                                      
//chart13.colors = ['#074dea', '#FF6600', '#074dea', '#FF6600', '#074dea'];
chart13.chartLineStrokeStyle = 'rgba(0, 0, 200, 0.5)';
chart13.chartPointFillStyle = 'rgb(0, 0, 200)';
chart13.draw();


</script>
          