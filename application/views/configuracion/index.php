       <!-- /.row -->
    <div class="row">
        <div class="col-lg-12" id="auditoria">
            <div class="well">
                <h4>Informaci&oacute;n</h4>
                <p> En este modulo se muestra la configuraci&oacute;n b&aacute;sica del sistema. Aqu&iacute; podr&aacute;s configurar si tu aplicaci&oacute;n posee biom&eacute;trio y el email receptor de las notificaciones.</p>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <br>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#configuracion" data-toggle="tab" aria-expanded="true">
                            <b>Configuraci&oacute;n del Sistema</b></a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="configuracion">
                            <div class="row">
                                <div class="col-md-12">
                                   <form method="post" name="form-configuracion" id="form-configuracion" class="form-horizontal" action="" target="">
                                    <br>
                                        <div class="col-md-3">
                                            <label>Email Emisor</label>
                                            <input type="email" name="emailEmisor" id="emailEmisor" class="form-control" placeholder="email@dominio.com" required="required" value="<?=$configuracion["emailEmisor"]?>" readonly="readonly">
                                        </div>

                                        <div class="col-md-3">
                                            <label>Email Receptor</label>
                                            <input type="email" name="emailReceptor" id="emailReceptor" class="form-control" value="<?=$configuracion["emailReceptor"]?>" placeholder="email@dominio.com" required="required">
                                        </div>

                                        <div class="col-md-3">
                                            <label>Biom&eacute;trico</label>
                                            <select class="form-control" name="biometrico" id="biometrico" required="required">

                                                <option 
                                                    <?php 
                                                        if($configuracion["biometrico"]=="si") { 
                                                            echo "selected='selected'";
                                                        }
                                                    ?>
                                                    value="si">Activado
                                                </option>
                                                <option 
                                                    <?php 
                                                        if($configuracion["biometrico"]=="no") { 
                                                            echo "selected='selected'";
                                                        }
                                                    ?>
                                                    value="no">Desactivado
                                                </option>
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                            <input type="submit" style="margin-top: 25px;" name="btn-inicio" id="btn-inicio-buscar" value="Configurar" class="btn btn-md btn-primary">
                                        </div>
                                    <br>
                                    </form>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            <div id="reportes-data"></div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->