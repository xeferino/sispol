
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-bomb"></i> Armas Registradas en el sistema<?php if ($validar!=""){echo ', filatrado por el rango de fecha desde '.$fechadesde.' hasta '.$fechahasta;} ?>
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
       <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover" id="dataTables-reportes-armas">
            <thead>
                <tr>
                    <th>#</th>
                    <th>C&oacute;digo</th>
                    <th>Tipo</th>
                    <th>Calibre</th>
                    <th>Fecha</th>
                    <th>Descripci&oacute;n</th>
                    <th>Cantidad</th>
                    <th>Cant. Asignada</th>
                    <th>Disponible</th>
                </tr>
            </thead>
            <tbody>
                <?php  
                if ($armamentos>0):
                    foreach ($armamentos as $armas): ?>
                        <tr class="odd gradeX">
                            <td><?=$armas->idarma?></td>
                            <td><?=$armas->codigo?></td>
                            <td><?=$armas->tipo?></td>
                            <td><?=$armas->calibre?></td>
                            <td><?=$armas->fecha?></td>
                            <td><?=$armas->descripcion?></td>
                            <td><?=$armas->cantidad?></td>
                            <td><?=$armas->cantidad_asignada?></td>
                            <td><?=$armas->disponible?></td>
                        </tr>
                    <?php  endforeach; ?>
                <?php endif; ?> 
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
        <?php if ($armamentos>0): ?>
            <div class="col-md-4">
                <form name="form-exportar-armas" target="_blank" method="post" action="reportes/armamentos" target="_blank">
                    <input type="hidden" name="desde" value="<?=$fechadesde?>">
                    <input type="hidden" name="hasta" value="<?=$fechahasta?>">
                    <br>
                    <button type="submit" class="btn btn-primary btn-md " id="btn-control-pdf" name="opc" value="pdf"><i class="fa fa-file-pdf-o"></i> PDF <span></span></button>
                    <button type="submit" class="btn btn-success btn-md " id="btn-control-excel" name="opc" value="excel"><i class="fa fa-file-excel-o"></i>  
                    Excel <span></span></button>
                </form>
            </div>
    <?php endif;?>
    </div>
    <!-- .panel-body -->
</div>