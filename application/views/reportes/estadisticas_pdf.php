<?php
require_once APPPATH.'\libraries\libchart\libchart\classes\libchart.php';
$ci =& get_instance();
$ci->load->model(array("armas_model", "personal_model"));
$pdf = new Pdf('H', 'mm', 'LEGAL', true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sispol');
$pdf->SetTitle("Reporte General de Estadísticas");
$pdf->SetSubject('PDF');
$pdf->SetKeywords('Reporte General de Estadísticas');
$cintillo = "banner_formatos.jpg";

#PDF_HEADER_LOGO
//$cintillo = "banner_formatos_valencia.jpg";
// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
$pdf->SetHeaderData($cintillo, "180%", "", "", array(0, 64, 255), array(0, 64, 128));
$pdf->setFooterData($tc = array(0, 64, 0), $lc = array(0, 64, 128));

// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//relación utilizada para ajustar la conversión de los píxeles
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


// ---------------------------------------------------------
// establecer el modo de fuente por defecto
$pdf->setFontSubsetting(true);

// Establecer el tipo de letra

//Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
// Helvetica para reducir el tamaño del archivo.
$pdf->SetFont('dejavusans', '', 8, '', true);

// Añadir una página
// Este método tiene varias opciones, consulta la documentación para más información.
$pdf->AddPage("P","A4");
if ($movimientos>0) {
    $persona="";
    $armas="";
    $ar ="";
    $fc  ="";
    if ($idpersonal!="todos" or $armamentos!="todos") {

        if ($idpersonal!="todos") {
            # code...
             $funcionario = $ci->personal_model->get($idpersonal);
             $persona = "(".$funcionario->documento."-".$funcionario->cedula.") ".$funcionario->nombres." ".$funcionario->apellidos." Placa: (".$funcionario->placa.") Rango: ".strtoupper($funcionario->rango);
             $fc ='
                    <tr align="center">
                        <th colspan="3"><b>'.$persona.'</b></th>
                    </tr>
                ';
        }
        if ($armamentos!="todos") {
            # code...
            $arma =  $ci->armas_model->get($armamentos);
            $armas = "Código (".$arma->codigo.") Tipo: ".$arma->tipo." Calibre: ".$arma->calibre;
            $ar ='
                    <tr align="center">
                        <th colspan="3"><b>'.$armas.'</b></th>
                    </tr>
                ';
        }
    }
    $txt ='
            <table style="width: 100% !important;" cellpadding="2">
                <thead>
                    <tr align="center">
                        <th></th>
                        <th></th>
                        <th><b>FECHA: '.date('d-m-Y').'</b></th>
                    </tr>
                    <tr align="center">
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr align="center">
                        <th colspan="3"><b>REPORTE GENERAL DE ESTADÍSTICAS</b></th>
                    </tr>
                    '.$fc.'
                    '.$ar.'
                    <tr align="center">
                        <th colspan="3"><b></b></th>
                    </tr>
                </thead>
    ';
    if ($validar == "true") {
        $txt .='
                <tr style="font-weight: bold;">
                    <th width="100%" colspan="3" align="center">Filtrado de para la Fecha: '.$fechadesde.' - '.$fechahasta.'</th>
                </tr>
        ';
    }
        
        if ($idpersonal!="todos" or $armamentos!="todos") {
            if ($armamentos!="todos") {
                $entregado = 0;
                $pendiente = 0;
                $vencido = 0;
                //var_dump($armamentos);
                foreach ($movimientos as $data){
                    $armas = explode(',', $data->idarmas);
                    foreach ($armas as $keya => $asignadas) {
                        $arma = $ci->armas_model->get($armamentos);
                        if ($armamentos==$asignadas) {
                            //echo $arma->codigo."<br>";
                             if ($data->estatusarma=="Entregado") {
                                $entregado++;
                            }elseif ($data->estatusarma=="Pendiente") {
                                $pendiente++;
                            }elseif ($data->estatusarma=="Vencido") {
                                 $vencido++;
                            }
                        }

                    }
                }

                $pieChart1 = new PieChart();
                $dataSetPie1 = new XYDataSet();

                $dataSetPie1->addPoint(new Point("Entregado (".$entregado.")", $entregado));
                $dataSetPie1->addPoint(new Point("Pendiente (".$pendiente.")", $pendiente));
                $dataSetPie1->addPoint(new Point("Vencido (".$vencido.")", $vencido));

                $pieChart1->setTitle("Gráfica N°1 Porcentaje de Movimientos de Arma Código (".$arma->codigo.") Tipo: ".$arma->tipo." Calibre: ".$arma->calibre);
                $pieChart1->setDataSet($dataSetPie1);
                $pieChart1->render('application/libraries/libchart/demo/generated/grafica1.png');

                /*Armas estatus*/
                $txt .='
                        <tr style="font-weight: bold;">
                            <th width="100%" colspan="3" align="center"><img alt="Pie chart"  src="application/libraries/libchart/demo/generated/grafica1.png" style="border: 1px solid gray;"/></th>
                        </tr>
                ';

                $chart = new HorizontalBarChart(600, 170);
                $dataSet = new XYDataSet();
                $idActual = "";
                $idAnterior = "";
                $cantidad = 0;
                //$armas = $ci->armas_model->get();

                foreach ($movimientos as $movi) {
                    $armas = explode(',', $movi->idarmas);
                    foreach ($armas as $keya => $asignadas) {
                        $arma = $ci->armas_model->get($armamentos);
                        $idActual = $asignadas;
                        if ($idActual!=$idAnterior) {
                            
                            if ($asignadas == $armamentos) {
                                $cantArmas = explode(',', $movi->cantarmas);
                                    foreach ($cantArmas as  $keyc => $cantidad) {
                                        if ($keya==$keyc) {
                                          $dataSet->addPoint(new Point($arma->tipo." ".$arma->calibre,  $cantidad++));
                                        }
                                    }
                               
                            }
                        }  
                    }
                    $idAnterior=$idActual;
                }

                $chart->setDataSet($dataSet);
                $chart->getPlot()->setGraphPadding(new Padding(5, 30, 20, 140));
                $chart->setTitle("Gráfica N°2 Porcentaje de Armamentos");
                $chart->render('application/libraries/libchart/demo/generated/grafica2.png');

                /*Armas todas*/
                $txt .='
                            <tr style="font-weight: bold;">
                                <th width="100%" colspan="3" align="center"><img alt="Pie chart"  src="application/libraries/libchart/demo/generated/grafica2.png" style="border: 1px solid gray;"/></th>
                            </tr>
                ';
            }

                if ($idpersonal!="todos") {
                    $chartf = new HorizontalBarChart(600, 170);
                    $dataSetf = new XYDataSet();
                    $idActual = "";
                    $idAnterior = "";
                    $cantidad = 0;
                    foreach ($movimientos as $movi) {
                        $armamentos = explode(',', $movi->idarmas);
                        foreach ($armamentos as $keya => $asignadas) {
                            $arma = $ci->armas_model->get($asignadas);
                            $idActual = $asignadas;
                            if ($idActual!=$idAnterior) {

                                if ($arma->idarma == $asignadas) {
                                    $cantArmas = explode(',', $movi->cantarmas);
                                    foreach ($cantArmas as  $keyc => $cantidad) {
                                        if ($keya==$keyc) {
                                          $dataSetf->addPoint(new Point($arma->tipo." ".$arma->calibre,  $cantidad++));
                                      }
                                  }

                              }
                          }  
                      }
                      $idAnterior=$idActual;
                  }
                    $chartf->setDataSet($dataSetf);
                    $chartf->getPlot()->setGraphPadding(new Padding(5, 30, 20, 140));
                    $chartf->setTitle("Gráfica N°2 Porcentaje de Armamentos Funcionario");
                    $chartf->render('application/libraries/libchart/demo/generated/grafica3.png');

                    /*Armas todas*/
                    $txt .='
                                <tr style="font-weight: bold;">
                                    <th width="100%" colspan="3" align="center"><img alt="Pie chart"  src="application/libraries/libchart/demo/generated/grafica3.png" style="border: 1px solid gray;"/></th>
                                </tr>
                    ';
              }

        }else{
            # code...
            $entregado = 0;
            $pendiente = 0;
            $vencido = 0;
            foreach ($movimientos as $data){
                if ($data->estatusarma=="Entregado") {
                    $entregado++;
                }elseif ($data->estatusarma=="Pendiente") {
                    $pendiente++;
                }elseif ($data->estatusarma=="Vencido") {
                     $vencido++;
                }
            }

            $pieCharta = new PieChart();
            $dataSetPiea = new XYDataSet();

            $dataSetPiea->addPoint(new Point("Entregado (".$entregado.")", $entregado));
            $dataSetPiea->addPoint(new Point("Pendiente (".$pendiente.")", $pendiente));
            $dataSetPiea->addPoint(new Point("Vencido   (".$vencido.")", $vencido));

            $pieCharta->setTitle("Gráfica N°1 Porcentaje de Movimientos de Armamentos");
            $pieCharta->setDataSet($dataSetPiea);
            $pieCharta->render('application/libraries/libchart/demo/generated/grafica.png');

            /*Armas todas*/
            $txt .='
                        <tr style="font-weight: bold;">
                            <th width="100%" colspan="3" align="center"><img alt="Pie chart"  src="application/libraries/libchart/demo/generated/grafica.png" style="border: 1px solid gray;"/></th>
                        </tr>
            ';
            
            if (count($movimientos)<=5){
                $chart = new HorizontalBarChart(600, 170);
                $dataSet = new XYDataSet();
                $idActual = "";
                $idAnterior = "";
                $cantidad = 0;
                $armas = $ci->armas_model->get();

                            foreach ($movimientos as $movi) {
                                $armamentos = explode(',', $movi->idarmas);
                                foreach ($armamentos as $keya => $asignadas) {
                                    $arma = $ci->armas_model->get($asignadas);
                                    $idActual = $asignadas;
                                    if ($idActual!=$idAnterior) {

                                        if ($arma->idarma == $asignadas) {
                                            $cantArmas = explode(',', $movi->cantarmas);
                                            foreach ($cantArmas as  $keyc => $cantidad) {
                                                if ($keya==$keyc) {
                                                $dataSet->addPoint(new Point($arma->tipo." ".$arma->calibre,  $cantidad++));
                                            }
                                        }

                                    }
                                }  
                            $idAnterior=$idActual;
                            }
                        }

                    $chart->setDataSet($dataSet);
                    $chart->getPlot()->setGraphPadding(new Padding(5, 30, 20, 140));
                    $chart->setTitle("Gráfica N°2 Porcentaje de Armamentos");
                    $chart->render('application/libraries/libchart/demo/generated/graficas.png');

                    /*Armas todas*/
                    $txt .='
                    <tr style="font-weight: bold;">
                    <th width="100%" colspan="3" align="center"><img alt="Pie chart"  src="application/libraries/libchart/demo/generated/graficas.png" style="border: 1px solid gray;"/></th>
                    </tr>
                    ';
            }else{
                $chart = new HorizontalBarChart(600, 170);
                $dataSet = new XYDataSet();
                $idActual = "";
                $idAnterior = "";
                $cantidad = 0;
                $armas = $ci->armas_model->get();

                            foreach ($movimientos as $movi) {
                                $armamentos = explode(',', $movi->idarmas);
                                foreach ($armamentos as $keya => $asignadas) {
                                    $arma = $ci->armas_model->get($asignadas);
                                    $idActual = $asignadas;
                                    if ($idActual!=$idAnterior) {

                                        if ($arma->idarma == $asignadas) {
                                            $cantArmas = explode(',', $movi->cantarmas);
                                            foreach ($cantArmas as  $keyc => $cantidad) {
                                                if ($keya==$keyc) {
                                                $dataSet->addPoint(new Point($arma->tipo." ".$arma->calibre,  $cantidad++));
                                            }
                                        }

                                    }
                                }  
                            $idAnterior=$idActual;
                            }
                        }

                    $chart->setDataSet($dataSet);
                    $chart->getPlot()->setGraphPadding(new Padding(5, 30, 20, 140));
                    $chart->setTitle("Gráfica N°2 Porcentaje de Armamentos");
                    $chart->render('application/libraries/libchart/demo/generated/graficas.png');

                    /*Armas todas*/
                    $txt .='
                    <tr style="font-weight: bold;">
                    <th width="100%" colspan="3" align="center"><img alt="Pie chart"  src="application/libraries/libchart/demo/generated/graficas.png" style="border: 1px solid gray;"/></th>
                    </tr>
                    ';
            }
            
        }
        
    }else{
        $txt='
                <p style="font-weight: bold; font-size:14px;" align="center">
                    No hay resultados<br>
                </p>
    ';
    }

    $pdf->writeHTML($txt, true, false, true, false, '');

    //Close and output PDF document
    $nombre_archivo = utf8_decode("Reporte General.pdf");
    $pdf->Output($nombre_archivo, 'I');