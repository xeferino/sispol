<?php
//$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf = new Pdf('H', 'mm', 'LEGAL', true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sispol');
$pdf->SetTitle("Reporte General de Usuarios");
$pdf->SetSubject('PDF');
$pdf->SetKeywords('Reporte General de Usuarios');
$cintillo = "banner_formatos.jpg";
    #PDF_HEADER_LOGO
    //$cintillo = "banner_formatos_valencia.jpg";
// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
$pdf->SetHeaderData($cintillo, "180%", "", "", array(0, 64, 255), array(0, 64, 128));
$pdf->setFooterData($tc = array(0, 64, 0), $lc = array(0, 64, 128));

// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//relación utilizada para ajustar la conversión de los píxeles
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


// ---------------------------------------------------------
// establecer el modo de fuente por defecto
$pdf->setFontSubsetting(true);

// Establecer el tipo de letra

//Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
// Helvetica para reducir el tamaño del archivo.
$pdf->SetFont('dejavusans', '', 8, '', true);

// Añadir una página
// Este método tiene varias opciones, consulta la documentación para más información.
$pdf->AddPage("P","A4");

$txt ='
        <table style="width: 100% !important;" cellpadding="2">
            <thead>
                <tr align="center">
                    <th></th>
                    <th></th>
                    <th><b>FECHA: '.date('d-m-Y').'</b></th>
                </tr>
                <tr align="center">
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <tr align="center">
                    <th colspan="3"><b>REPORTE GENERAL DE USUARIOS</b></th>
                </tr>
                <tr align="center">
                    <th colspan="3"><b></b></th>
                </tr>
            </thead>
';

$txt .='
            <tr style="font-weight: bold;">
                <th width="4%">#</th>
                <th width="15%" align="center">Nombres y Apellidos</th>
                <th width="12%" align="center">C&eacute;dula</th>
                <th width="14%" align="center">Correo</th>
                <th width="12%" align="center">Fecha</th>
                <th width="17%" align="center">Tipo</th>
                <th width="14%" align="center">Usuario</th>
                <th width="10%" align="center">Estatus</th>
            </tr>
            <hr>
';
    foreach ($usuarios as $usuario):

        $estilo = ($usuario->estatus == "Activo") ? 'style="color:#5cb85c; font-weight:bold;"' : 'style="color:#d43f3a; font-weight:bold;"';

    $txt .='
            <tr>
                <td width="4%">'.$usuario->idusuario.'</td>
                <td width="15%" align="center">'.utf8_decode($usuario->nombre).'</td>
                <td width="12%" align="center">'.utf8_decode($usuario->cedula).'</td>
                <td width="14%" align="center">'.utf8_decode($usuario->email).'</td>
                <td width="12%" align="center">'.utf8_decode($usuario->fecha).'</td>
                <td width="17%" align="center">'.utf8_decode($usuario->tipo).'</td>
                <td width="14%" align="center">'.utf8_decode($usuario->usuario).'</td>
                <td width="10%" align="center" '.$estilo.'>'.$usuario->estatus.'</td>
            </tr>
    ';
    endforeach;

    $pdf->writeHTML($txt, true, false, true, false, '');

    //Close and output PDF document
    $nombre_archivo = utf8_decode("Reporte General de Usuarios.pdf");
    $pdf->Output($nombre_archivo, 'I');