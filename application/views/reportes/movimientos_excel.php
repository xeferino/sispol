<?php
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Disposition: attachment; filename=movimientos.xls"); 
?>

<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-reportes-movimientos">
        <thead>
         <tr>
            <th colspan="9"><img width="80%" class="image-responsive" src="<?=base_url('public/img/banner_formatos.jpg')?>">
                <br><br><br>
            </th>
        </tr>
        <tr>
            <th colspan="9">MOVIMIENTOS DE ARMAMENTOS EN EL SISTEMA<?php if ($validar!=""){echo ', FILTRADO POR EL RANGO DE FECHA DESDE'.$fechadesde.' HASTA '.$fechahasta;} ?>
            <br><br><br>
        </th>
    </tr>
    <tr>
        <th>#</th>
        <th><?=utf8_decode('Asignación')?></th>
        <th><?=utf8_decode('Devolución')?></th>
        <th>T. de Retraso</th>
        <th>Armas</th>
        <th>Funcionario</th>
        <th>Estatus</th>
    </tr>
</thead>
<tbody>
    <?php 
    $ci =& get_instance();
    $ci->load->model(array("armas_model", "personal_model"));
    foreach ($movimientos as $data): 
        $personal = $ci->personal_model->get($data->idpersonal);
        if ($data->tiempoRetraso!="" and $data->tiempoRetraso>="0"){
            $retraso = explode(":",$data->tiempoRetraso);
            $tiempo = $retraso[0]." H ".$retraso[1]." M ".$retraso[2]." S"; 
        }else{
            $tiempo = "--------------";
        }
        $estilo = ($data->estatusarma == "Pendiente"  or $data->estatusarma == "Vencido") ? 'style="color:#d43f3a; font-weight:bold;"' : 'style="color:#5cb85c; font-weight:bold;"';
        ?>
        <tr class="odd gradeX">
            <td> <?= utf8_decode($data->idpersonalarmamento) ?></td>
            <td> <?= utf8_decode($data->fechaasignacion)." ".utf8_decode($data->horaasignacion) ?></td>
            <td> <?= utf8_decode($data->fechadevolucion)." ".utf8_decode($data->horadevolucion) ?></td>
            <td> <?= $tiempo ?></td>
            <td>
                <?php
                $armamentos = explode(',', $data->idarmas);
                foreach ($armamentos as $keya => $asignadas) {
                    $arma = $ci->armas_model->get($asignadas);

                    $cantArmas = explode(',', $data->cantarmas);
                    foreach ($cantArmas as $keyc => $cantidad) {
                        if($keya==$keyc){
                             $calibre="";
                             if ($arma->calibre!=""){$calibre="Calibre: ".$arma->calibre; }
                            ?>
                            <?="(".$arma->codigo.") ".$arma->tipo." ".$calibre." Cantidad: ".$cantidad?>
                            <br>
                            <?php
                        }
                    }
                }
                ?>
            </td>
            <td> <?= "(".utf8_decode($personal->documento."-".$personal->cedula.") ".$personal->nombres." ".$personal->apellidos." Rango: ".strtoupper($personal->rango))?></td>
            <td <?=$estilo?>> <?=$data->estatusarma?></td>
        </tr>
    <?php endforeach;?>
</tbody>
</table>
</div>
