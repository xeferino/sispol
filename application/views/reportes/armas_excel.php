<?php
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Disposition: attachment; filename=armas_inventario.xls"); 
?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-reportes-armas">
        <thead>
            <tr>
                <th colspan="9"><img width="80%" class="image-responsive" src="<?=base_url('public/img/banner_formatos.jpg')?>">
                    <br><br><br>
                </th>
            </tr>
            <tr>
                <th colspan="9">ARMAS REGISTRADAS EN EL SISTEMA<?php if ($validar!=""){echo ', FILTRADO POR EL RANGO DE FECHA DESDE'.$fechadesde.' HASTA '.$fechahasta;} ?>
                <br><br><br>
            </th>
        </tr>
        <tr>
            <th>#</th>
            <th><?=utf8_decode('C&oacute;digo')?></th>
            <th>Tipo</th>
            <th>Calibre</th>
            <th>Fecha</th>
            <th><?=utf8_decode('Descripci&oacute;n')?></th>
            <th>Cantidad</th>
            <th>Cant. Asignada</th>
            <th>Disponible</th>
        </tr>
    </thead>
    <tbody>
        <?php  foreach ($armamentos as $armas): ?>
            <tr class="odd gradeX">
                <td><?=utf8_decode($armas->idarma)?></td>
                <td><?=utf8_decode($armas->codigo)?></td>
                <td><?=utf8_decode($armas->tipo)?></td>
                <td><?=utf8_decode($armas->calibre)?></td>
                <td><?=utf8_decode($armas->fecha)?></td>
                <td><?=utf8_decode($armas->descripcion)?></td>
                <td><?=utf8_decode($armas->cantidad)?></td>
                <td><?=utf8_decode($armas->cantidad_asignada)?></td>
                <td><?=utf8_decode($armas->disponible)?></td>
            </tr>
        <?php  endforeach; ?> 
    </tbody>
    </table>
</div>
        <!-- /.table-responsive -->