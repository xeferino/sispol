<?php
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Disposition: attachment; filename=movimientos_armas_individuales.xls"); 
$ci =& get_instance();
$ci->load->model(array("armas_model", "personal_model"));
?>

<?php if($idarmamento=="todos"):?>
                    <?php  if ($movimientos!=0): ?>
                        <table class="table table-striped" id="dataTables-reportes-armas-movimientos">
                        <thead>
                            <tr>
                                <th colspan="7"><img width="80%" class="image-responsive" src="<?=base_url('public/img/banner_formatos.jpg')?>">
                                    <br><br><br>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="7">MOVIMIENTOS DE ARMAMENTOS INDIVIDUALES EN EL SISTEMA<?php if ($validar!=""){echo ', FILTRADO POR EL RANGO DE FECHA DESDE'.$fechadesde.' HASTA '.$fechahasta;} ?>
                                <br><br><br>
                            </th>
                        </tr>
                    </thead>
                            <tbody>
                                <?php foreach ($movimientos as $data): ?> 
                                <?php $estilo = ($data->estatusarma == "Pendiente"  or $data->estatusarma == "Vencido") ? 'style="color:#d43f3a; font-weight:bold;"' : 'style="color:#5cb85c; font-weight:bold;"';?>
                                <?php $personal = $ci->personal_model->get($data->idpersonal);?>
                                    <tr>
                                        <th colspan="7">Armas:</th>
                                    </tr>
                                    <?php $armamentos = explode(',', $data->idarmas);?>
                                    <?php foreach ($armamentos as $keya => $asignadas):?>
                                        <?php $arma = $ci->armas_model->get($asignadas);?>
                                        <?php $calibre = ""; ?>
                                        <?php if($arma->calibre!=""): ?>
                                            <?php  $calibre = " Calibre: ".$arma->calibre;?>
                                        <?php endif?> 
                                        <?php $cantArmas = explode(',', $data->cantarmas);?>
                                        <?php $i=0;?>
                                        <?php foreach ($cantArmas as $keyc => $cantidad):?>
                                        <?php $i++;  ?>
                                            <?php if($keya==$keyc):?>
                                                    <tr>
                                                        <td colspan="7">
                                                            <?=$i.") C&oacute;digo: ".$arma->codigo." ". $arma->tipo." ".$calibre." Cantidad: ".$cantidad?>
                                                        </td>
                                                    </tr>
                                            <?php endif?>
                                        <?php endforeach?>
                                    <?php endforeach?>
                                    <tr>
                                        <th colspan="7">Funcionario:</th>
                                    </tr>
                                    <tr>
                                        <td>#</td>
                                        <td>Nombre y Apellido</td>
                                        <td>C&eacute;dula</td>
                                        <td>Placa</td>
                                        <td>Rango</td>
                                        <td>Fec. Asig. / Fec. Dev.</td>
                                        <td>Estatus</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td><?= $personal->nombres." ".$personal->apellidos?></td>
                                        <td><?= $personal->documento."-".$personal->cedula?></td>
                                        <td><?= $personal->placa?></td>
                                        <td><?= $personal->rango?></td>
                                        <td><?= $data->fechaasignacion." ".$data->horaasignacion ?> / <?= $data->fechadevolucion." ".$data->horadevolucion ?></td>
                                        <td <?=$estilo?>> <?=$data->estatusarma?></td>
                                    </tr>
                                <?php endforeach?>
                            </tbody>
                        </table>
                    <?php else:?>
                        No hay resultados
                    <?php endif;?>
        <?php else:?>

            <?php $arma = $ci->armas_model->get($idarmamento);?>
            <?php $calibre = ""; ?>
            <?php if($arma->calibre!=""): ?>
                <?php  $calibre = " Calibre: ".$arma->calibre;?>
            <?php endif?> 

            <div class="table-responsive">
                <table class="table table-striped" id="dataTables-reportes-armas-movimientos">
                    <thead>
                        <tr>
                            <th colspan="7"><img width="80%" class="image-responsive" src="<?=base_url('public/img/banner_formatos.jpg')?>">
                                <br><br><br>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="7">MOVIMIENTOS DE ARMAMENTOS INDIVIDUALES EN EL SISTEMA<?php if ($validar!=""){echo ', FILTRADO POR EL RANGO DE FECHA DESDE'.$fechadesde.' HASTA '.$fechahasta;} ?>
                            <br><br><br>
                        </th>
                    </tr>
                </thead>

                    <tbody>
                     <?php if($movimientos>0):?>
                        <?php foreach ($movimientos as $data):?>
                            <?php $estilo = ($data->estatusarma == "Pendiente"  or $data->estatusarma == "Vencido") ? 'style="color:#d43f3a; font-weight:bold;"' : 'style="color:#5cb85c; font-weight:bold;"';?>
                            <?php $armamentos = explode(',', $data->idarmas);?>
                            <?php $validar = []; ?>
                            <?php foreach ($armamentos as $a):?>
                                <?php if($idarmamento==$a):?>
                                    <?php $validar [] = $a;?>
                                <?php endif?>
                            <?php endforeach ?>
                        <?php endforeach?>
                    <?php endif ?>
                    <?php if(count($validar)>0):?>
                            <tr>
                                <th colspan="7">
                                    <?="Arma: C&oacute;digo: (".$arma->codigo.") Tipo: ". $arma->tipo." ".$calibre?>
                                </th>
                            </tr>
                            <?php foreach ($movimientos as $data): ?>
                                <?php $armamentos = explode(',', $data->idarmas);?>
                                <?php $personal = $ci->personal_model->get($data->idpersonal);?>
                                <?php foreach ($armamentos as $a):?>
                                    <?php if($idarmamento==$a): ?>
                                            <tr style="font-weight:bold;">
                                                <td>#</td>
                                                <td>Nombre y Apellido</td>
                                                <td>C&eacute;dula</td>
                                                <td>Placa</td>
                                                <td>Rango</td>
                                                <td>Fec. Asig. / Fec. Dev.</td>
                                                <td>Estatus</td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td><?= $personal->nombres." ".$personal->apellidos?></td>
                                                <td><?= $personal->documento."-".$personal->cedula?></td>
                                                <td><?= $personal->placa?></td>
                                                <td><?= $personal->rango?></td>
                                                <td><?= $data->fechaasignacion." ".$data->horaasignacion ?> / <?= $data->fechadevolucion." ".$data->horadevolucion ?></td>
                                                <td <?=$estilo?>> <?=$data->estatusarma?></td>
                                            </tr>
                                    <?php endif?>
                                <?php endforeach?>
                            <?php endforeach?>
                                    <?php else:?>
                                        No hay resultados
                                <?php endif?>
                    </tbody>
                </table>
            </div>
        <?php endif?>