<?php
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Disposition: attachment; filename=usuarios_registrados.xls"); 
?>

<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-reportes-usuarios">
        <thead>
            <tr>
                <th colspan="9"><img width="80%" class="image-responsive" src="<?=base_url('public/img/banner_formatos.jpg')?>">
                    <br><br><br>
                </th>
            </tr>
            <tr>
                <th colspan="9">USUARIOS REGISTRADOS EN EL SISTEMA<?php if ($validar!=""){echo ', FILTRADO POR EL RANGO DE FECHA DESDE'.$fechadesde.' HASTA '.$fechahasta;} ?>
                <br><br><br>
            </th>
        </tr>
        <tr>
            <th>#</th>
            <th>Nombres y Apellidos</th>
            <th>C&eacute;dula</th>
            <th>Correo</th>
            <th>Fecha</th>
            <th>Tipo</th>
            <th>Usuario</th>
            <th>Estatus</th>
        </tr>
    </thead>
    <tbody>
        <?php  foreach ($usuarios as $usuario):
            $estilo = ($usuario->estatus == "Activo") ? 'style="color:#5cb85c; font-weight:bold;"' : 'style="color:#d43f3a; font-weight:bold;"';
            ?>
            <tr class="odd gradeX">
                <td><?=utf8_decode($usuario->idusuario)?></td>
                <td><?=utf8_decode($usuario->nombre)?></td>
                <td><?=utf8_decode($usuario->cedula)?></td>
                <td><?=utf8_decode($usuario->email)?></td>
                <td><?=utf8_decode($usuario->fecha)?></td>
                <td><?=utf8_decode($usuario->tipo)?></td>
                <td><?=utf8_decode($usuario->usuario)?></td>
                <td align="center" <?=$estilo?>><?=$usuario->estatus?></td>
            </tr>
        <?php  endforeach; ?> 
    </tbody>
</table>
</div>
