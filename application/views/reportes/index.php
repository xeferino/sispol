       <!-- /.row -->
    <div class="row">
        <div class="col-lg-12" id="auditoria">
            <div class="well">
                <h4>Informaci&oacute;n</h4>
                <p> En este modulo se muestra los diferentes reportes que soporta el 
                    sistema.
                </p>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Tipos de Reportes
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#usuarios" data-toggle="tab" aria-expanded="true" onclick="clearForm();"><b>Usuarios en el Sistema</b></a>
                        </li>
                        <li class=""><a href="#personal" data-toggle="tab" aria-expanded="false" onclick="clearForm();"><b>Personal en la Instituci&oacute;n</b></a>
                        </li>
                        <li class=""><a href="#armamentos" data-toggle="tab" aria-expanded="false" onclick="clearForm();"><b>Armamentos Inventario</b></a>
                        </li>
                        <li class=""><a href="#movimientos" data-toggle="tab" aria-expanded="false" onclick="clearForm();"><b>Movimientos de E/S de Armamentos Funcionarios</b></a>
                        </li>
                        <li class=""><a href="#movimientos_armas" data-toggle="tab" aria-expanded="false" onclick="clearForm();"><b>Movimientos de E/S de Armamentos Individual</b></a>
                        </li>
                        <li class=""><a href="#estadisticas" data-toggle="tab" aria-expanded="false" onclick="clearForm();"><b>Estad&iacute;sticas de E/S de Armamentos</b></a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="usuarios">
                            <div class="row">
                                <div class="col-md-12">
                                   <form method="post" name="form-usuarios-reportes" id="form-usuarios-reportes" class="form-horizontal" action="" target="">
                                    <input type="hidden" name="opc" value="html">
                                    <br>
                                        <div class="col-md-3">
                                            <label>Fecha Desde</label>
                                            <input type="text" name="desde" id="desde_u" class="form-control datepicker" placeholder="0000-00-00">
                                        </div>

                                        <div class="col-md-3">
                                            <label>Fecha Hasta</label>
                                            <input type="text" name="hasta" id="hasta_u" class="form-control datepicker" placeholder="0000-00-00">
                                        </div>

                                        <div class="col-md-3">
                                            <label>Estatus</label>
                                            <select class="form-control" name="estatus" id="estatus">
                                                <option value="todos">.::Todos::.</option>
                                                <option value="0">Activos</option>
                                                <option value="1">Bloqueados</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                            <input type="submit" style="margin-top: 25px;" name="btn-inicio" id="btn-inicio-buscar" value="Buscar" class="btn btn-md btn-primary">
                                        </div>
                                    <br>
                                    </form>
                                </div>
                            </div>
                            <hr>
                        </div>

                        <div class="tab-pane fade" id="personal">
                           <div class="row">
                                <div class="col-md-12">
                                   <form method="post" name="form-personal-reportes" id="form-personal-reportes" class="form-horizontal" action="" target="">
                                    <input type="hidden" name="opc" value="html">
                                    <br>
                                        <div class="col-md-3">
                                            <label>Fecha Desde</label>
                                            <input type="text" name="desde" id="desde_p" class="form-control datepicker" placeholder="0000-00-00">
                                        </div>

                                        <div class="col-md-3">
                                            <label>Fecha Hasta</label>
                                            <input type="text" name="hasta" id="hasta_p" class="form-control datepicker" placeholder="0000-00-00">
                                        </div>

                                         <div class="col-md-3">
                                            <label>Rango</label>
                                            <select class="form-control" name="rango" id="rango">
                                                <option value="todos">.::Todos::.</option>
                                                <option value="comandante">Comandante</option>
                                                <option value="comandante segundo">Comandante Segundo</option>
                                                <option value="jefe de operaciones">Jefe de Operaciones</option>
                                                <option value="personal motorizado">Personal Motorizado</option>
                                                <option value="oficial">Oficial</option>
                                                <option value="oficial agregado">Oficial Agregado</option>
                                                <option value="oficial jefe">Oficial Jefe</option>
                                                <option value="supervisor">Supervisor</option>
                                                <option value="supervisor agregado">Supervisor Agregado</option>
                                                <option value="supervisor jefe">Supervisor Jefe</option>
                                                <option value="comisionado">Comisionado</option>
                                                <option value="comisionado agregado">Comisionado Agregado</option>
                                                <option value="comisionado jefe">Comisionado Jefe</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                            <label>Estatus</label>
                                            <select class="form-control" name="estatus" id="estatus">
                                                <option value="todos">.::Todos::.</option>
                                                <option value="0">Activos</option>
                                                <option value="1">Bloqueados</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                            <input type="submit" style="margin-top: 25px;" name="btn-inicio" id="btn-inicio-buscar" value="Buscar" class="btn btn-md btn-primary">
                                        </div>
                                    <br>
                                    </form>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="tab-pane fade" id="armamentos">
                            <div class="row">
                                <div class="col-md-12">
                                   <form method="post" name="form-armas-reportes" id="form-armas-reportes" class="form-horizontal" action="" target="">
                                    <input type="hidden" name="opc" value="html">
                                    <br>
                                        <div class="col-md-3">
                                            <label>Fecha Desde</label>
                                            <input type="text" name="desde" id="desde_a" class="form-control datepicker" placeholder="0000-00-00">
                                        </div>

                                        <div class="col-md-3">
                                            <label>Fecha Hasta</label>
                                            <input type="text" name="hasta" id="hasta_a" class="form-control datepicker" placeholder="0000-00-00">
                                        </div>

                                        <div class="col-md-3">
                                            <input type="submit" style="margin-top: 25px;" name="btn-inicio" id="btn-inicio-buscar" value="Buscar" class="btn btn-md btn-primary">
                                        </div>
                                    <br>
                                    </form>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="tab-pane fade" id="movimientos">
                            <div class="row">
                                <div class="col-md-12">
                                   <form method="post" name="form-movimientos-reportes" id="form-movimientos-reportes" class="form-horizontal" action="" target="">
                                    <input type="hidden" name="opc" value="html">
                                    <br>
                                        <div class="col-md-4">
                                            <label>Fecha Desde</label>
                                            <input type="text" name="desde" id="desde_m" class="form-control datepicker" placeholder="0000-00-00">
                                        </div>

                                        <div class="col-md-4">
                                            <label>Fecha Hasta</label>
                                            <input type="text" name="hasta" id="hasta_m" class="form-control datepicker" placeholder="0000-00-00">
                                        </div>

                                        <div class="col-md-4">
                                            <label>Funcionarios</label>
                                            <select class="form-control" name="idpersonal" id="idpersonal">
                                                <option value="todos">.::Todos::.</option>
                                                <?php foreach ($personal as $persona): ?>
                                                            <option value="<?=$persona->idpersonal?>">
                                                                <?="(".$persona->documento."-".$persona->cedula.") ".$persona->nombres." ".$persona->apellidos." Placa: ".$persona->placa?>
                                                            </option>
                                                <?php endforeach;?>
                                                </select>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <br>
                                            <label>Estatus</label>
                                            <select class="form-control" name="estatus" id="estatus">
                                                <option value="todos">.::Todos::.</option>
                                                <option value="Pendiente">Pendientes</option>
                                                <option value="Entregado">Entregados</option>
                                                <option value="Vencido">Vencidos</option>
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                             <br>
                                            <input type="submit" style="margin-top: 25px;" name="btn-inicio" id="btn-inicio-buscar" value="Buscar" class="btn btn-md btn-primary">
                                        </div>
                                    <br>
                                    </form>
                                </div>
                            </div>
                            <hr>
                        </div>

                        <div class="tab-pane fade" id="movimientos_armas">
                            <div class="row">
                                <div class="col-md-12">
                                   <form name="form-exportar-armas-movimientos" target="_blank" method="post" action="reportes/movimientosArmas" target="_blank"> 
                                    <br>
                                        <div class="col-md-4">
                                            <label>Fecha Desde</label>
                                            <input type="text" name="desde" id="desde_ma" class="form-control datepicker" placeholder="0000-00-00">
                                        </div>

                                        <div class="col-md-4">
                                            <label>Fecha Hasta</label>
                                            <input type="text" name="hasta" id="hasta_ma" class="form-control datepicker" placeholder="0000-00-00">
                                        </div>

                                        <div class="col-md-4">
                                            <label>Armamentos</label>
                                            <select class="form-control" name="armamentos" id="idarmas">
                                                <option value="todos">.::Todas::.</option>
                                                <?php foreach ($armas as $arma): ?>
                                                        <option value="<?=$arma->idarma?>"><?="(".$arma->codigo.") Tipo: ".$arma->tipo; if($arma->calibre!="") echo " Calibre: ".$arma->calibre;?>
                                                        </option>
                                                <?php endforeach;?>
                                                
                                            </select>
                                        </div>
                                        
                                        <!-- <div class="col-md-4">
                                            <br>
                                            <label>Estatus</label>
                                            <select class="form-control" name="estatus" id="estatus">
                                                <option value="todos">.::Todos::.</option>
                                                <option value="Pendiente">Pendientes</option>
                                                <option value="Entregado">Entregados</option>
                                                <option value="Vencido">Vencidos</option>
                                            </select>
                                        </div> -->

                                        <div class="col-md-4">
                                             <br>
                                             <button type="submit" style="margin-top: 25px;" class="btn btn-primary btn-md " id="btn-control-pdf" name="opc" value="pdf">
                                                <i class="fa fa-file-pdf-o"></i> PDF <span></span>
                                            </button>
        
                                             <button type="submit"  style="margin-top: 25px;" class="btn btn-success btn-md " id="btn-control-excel" name="opc" value="excel">
                                                <i class="fa fa-file-excel-o"></i>Excel <span></span>
                                            </button>
                                        </div>
                                    <br>
                                    </form>
                                </div>
                            </div>
                            <hr>
                        </div>

                        <div class="tab-pane fade" id="estadisticas">
                            <div class="row">
                                <div class="col-md-12">
                                   <form method="post" name="form-estadisticas-reportes" id="form-estadisticas-reportes" class="form-horizontal" action="<?=base_url()?>reportes/estadisticas" target="_blank">
                                    <input type="hidden" name="opc" value="html">
                                    <br>
                                        <div class="col-md-4">
                                            <label>Fecha Desde</label>
                                            <input type="text" name="desde" id="desde_e" class="form-control datepicker" placeholder="0000-00-00">
                                        </div>

                                        <div class="col-md-4">
                                            <label>Fecha Hasta</label>
                                            <input type="text" name="hasta" id="hasta_e" class="form-control datepicker" placeholder="0000-00-00">
                                        </div>

                                        <div class="col-md-4">
                                            <label>Funcionarios</label>
                                            <select class="form-control" name="idpersonal" id="idpersonal">
                                                <option value="todos">.::Todos::.</option>
                                                <?php foreach ($personal as $persona): ?>
                                                            <option value="<?=$persona->idpersonal?>">
                                                                <?="(".$persona->documento."-".$persona->cedula.") ".$persona->nombres." ".$persona->apellidos." Placa: ".$persona->placa?>
                                                            </option>
                                                <?php endforeach;?>
                                                </select>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <br>
                                            <label>Armamentos</label>
                                            <select class="form-control" name="armamentos" id="armamentos">
                                                <option value="todos">.::Todos::.</option>
                                                <?php foreach ($armas as $arma): ?>
                                                        <option value="<?=$arma->idarma?>"><?="(".$arma->codigo.") Tipo: ".$arma->tipo; if($arma->calibre!="") echo " Calibre: ".$arma->calibre;?>
                                                        </option>
                                                <?php endforeach;?>
                                                
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <br>
                                            <label>Estatus</label>
                                            <select class="form-control" name="estatus" id="estatus">
                                                <option value="todos">.::Todos::.</option>
                                                <option value="Pendiente">Pendientes</option>
                                                <option value="Entregado">Entregados</option>
                                                <option value="Vencido">Vencidos</option>
                                                <!-- <option value="Asignado">Mas Asignados</option> -->
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <br>
                                            <button type="submit" style="margin-top: 25px;" class="btn btn-primary btn-md " id="btn-control-pdf" name="opc" value="pdf"><i class="fa fa-file-pdf-o"></i> PDF <span></span></button>
                                           <!--  <button type="submit" style="margin-top: 25px;" class="btn btn-success btn-md " id="btn-control-excel" name="opc" value="excel"><i class="fa fa-file-excel-o"></i>  
                                                Excel <span></span></button> -->
                                        </div>
                                        <br>
                                    </form>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            <div id="reportes-data"></div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->