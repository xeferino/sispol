
<?php
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Disposition: attachment; filename=auditoria.xls"); 
?>   <!-- /.row -->
    <div class="row">
        <div class="col-lg-12" id="auditoria">
            <div class="panel panel-default">
                <div class="panel-heading">
                   <i class="fa fa-list"></i> Auditoria del Sistema
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-auditoria">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?=utf8_decode('Acción')?></th>
                                    <th>Tabla BD</th>
                                    <th>Fecha</th>
                                    <th><?=utf8_decode('Información Actual')?></th>
                                    <th><?=utf8_decode('Información Anterior')?></th>
                                    <th>Usuario</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($auditoria as $data): ?>
                                    <tr class="odd gradeX">
                                        <td><?=utf8_decode($data->id)?></td>
                                        <td><?=utf8_decode($data->accion)?></td>
                                        <td><?=utf8_decode($data->tabla)?></td>
                                        <td><?=utf8_decode($data->fechahora)?></td>
                                        <td><?=utf8_decode($data->informacion_new)?></td>
                                        <td><?=utf8_decode($data->informacion_old)?></td>
                                        <td>
                                            <?php 
                                                foreach ($usuarios as $usuario):
                                                    if ($usuario->idusuario==$data->idusuario)
                                                    {
                                                       echo utf8_decode($usuario->documento)."-".utf8_decode($usuario->cedula)."<br>".
                                                            utf8_decode($usuario->nombres)." ".utf8_decode($usuario->apellidos)."<br>".
                                                            utf8_decode($usuario->tipo);
                                                            
                                                    }
                                                endforeach; 
                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->