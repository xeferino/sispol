<?php 
$ci =& get_instance();
$ci->load->model(array("armas_model", "personal_model"));
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list"></i> <b>Listado de Movimientos de Entrada y Salida de Armamentos Individual <?php if ($validar!=""){echo ', filatrado por el rango de fecha desde '.$fechadesde.' hasta '.$fechahasta;} ?></b>
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <?php if($idarmamento=="todos"):?>
                <div class="table-responsive">
                    <?php  if ($movimientos>0): ?>
                        <table class="table table-striped" id="dataTables-reportes-armas-movimientos">
                            <thead>
                            </thead>
                            <tbody>
                                <?php foreach ($movimientos as $data): ?> 
                                <?php $personal = $ci->personal_model->get($data->idpersonal);?>
                                    <tr>
                                        <th colspan="7">Armas:</th>
                                    </tr>
                                    <?php $armamentos = explode(',', $data->idarmas);?>
                                    <?php foreach ($armamentos as $keya => $asignadas):?>
                                        <?php $arma = $ci->armas_model->get($asignadas);?>
                                        <?php $calibre = ""; ?>
                                        <?php if($arma->calibre!=""): ?>
                                            <?php  $calibre = " Calibre: ".$arma->calibre;?>
                                        <?php endif?> 
                                        <?php $cantArmas = explode(',', $data->cantarmas);?>
                                        <?php $i=0;?>
                                        <?php foreach ($cantArmas as $keyc => $cantidad):?>
                                        <?php $i++;  ?>
                                            <?php if($keya==$keyc):?>
                                                    <tr>
                                                        <td colspan="7">
                                                            <?=$i.") Código: ".$arma->codigo." ". $arma->tipo." ".$calibre." Cantidad: ".$cantidad?>
                                                        </td>
                                                    </tr>
                                            <?php endif?>
                                        <?php endforeach?>
                                    <?php endforeach?>
                                    <tr>
                                        <th colspan="7">Funcionario:</th>
                                    </tr>
                                    <tr>
                                        <td>#</td>
                                        <td>Nombre y Apellido</td>
                                        <td>C&eacute;dula</td>
                                        <td>Placa</td>
                                        <td>Rango</td>
                                        <td>Fec. Asig. / Fec. Dev.</td>
                                        <td>Estatus</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td><?= $personal->nombres." ".$personal->apellidos?></td>
                                        <td><?= $personal->documento."-".$personal->cedula?></td>
                                        <td><?= $personal->placa?></td>
                                        <td><?= $personal->rango?></td>
                                        <td><?= $data->fechaasignacion." ".$data->horaasignacion ?> / <?= $data->fechadevolucion." ".$data->horadevolucion ?></td>
                                        <td>
                                            <?php if ($data->estatusarma == "Pendiente" or $data->estatusarma == "Vencido"):?>
                                                <a href="javascript:;" class="btn btn-danger btn-xs">
                                                    <?=$data->estatusarma?>
                                                </a>
                                            <?php else:?>
                                                    <a href="javascript:;" class="btn btn-success btn-xs">
                                                        <?=$data->estatusarma?>
                                                    </a>
                                            <?php endif;?></td>
                                    </tr>
                                <?php endforeach?>
                            </tbody>
                        </table>
                <?php endif?>
                </div>
        <?php else:?>

            <?php $arma = $ci->armas_model->get($idarmamento);?>
            <?php $calibre = ""; ?>
            <?php if($arma->calibre!=""): ?>
                <?php  $calibre = " Calibre: ".$arma->calibre;?>
            <?php endif?> 

            <div class="table-responsive">
                <table class="table table-striped" id="dataTables-reportes-armas-movimientos">
                    <thead>
                    </thead>

                    <tbody>
                     <?php if($movimientos>0):?>
                        <?php foreach ($movimientos as $data):?>
                            <?php $armamentos = explode(',', $data->idarmas);?>
                            <?php $validar = []; ?>
                            <?php foreach ($armamentos as $a):?>
                                <?php if($idarmamento==$a):?>
                                    <?php $validar [] = $a;?>
                                <?php endif?>
                            <?php endforeach ?>
                        <?php endforeach?>
                    <?php endif ?>
                    <?php if(count($validar)>0):?>
                            <tr>
                                <th colspan="7">
                                    <?="Arma: Código: (".$arma->codigo.") Tipo: ". $arma->tipo." ".$calibre?>
                                </th>
                            </tr>
                            <?php foreach ($movimientos as $data): ?>
                                <?php $armamentos = explode(',', $data->idarmas);?>
                                <?php $personal = $ci->personal_model->get($data->idpersonal);?>
                                <?php foreach ($armamentos as $a):?>
                                    <?php if($idarmamento==$a): ?>
                                            <tr style="font-weight:bold;">
                                                <td>#</td>
                                                <td>Nombre y Apellido</td>
                                                <td>C&eacute;dula</td>
                                                <td>Placa</td>
                                                <td>Rango</td>
                                                <td>Fec. Asig. / Fec. Dev.</td>
                                                <td>Estatus</td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td><?= $personal->nombres." ".$personal->apellidos?></td>
                                                <td><?= $personal->documento."-".$personal->cedula?></td>
                                                <td><?= $personal->placa?></td>
                                                <td><?= $personal->rango?></td>
                                                <td><?= $data->fechaasignacion." ".$data->horaasignacion ?> / <?= $data->fechadevolucion." ".$data->horadevolucion ?></td>
                                                <td><?php if ($data->estatusarma == "Pendiente" or $data->estatusarma == "Vencido"):?>
                                                        <a href="javascript:;" class="btn btn-danger btn-xs">
                                                            <?=$data->estatusarma?>
                                                </a>
                                                <?php else:?>
                                                    <a href="javascript:;" class="btn btn-success btn-xs">
                                                        <?=$data->estatusarma?>
                                                    </a>
                                                <?php endif;?></td>
                                            </tr>
                                    <?php endif?>
                                <?php endforeach?>
                            <?php endforeach?>
                    <?php endif?>
                    </tbody>
                </table>
            </div>
        <?php endif?>
    <!-- /.panel-body -->
    </div>
<!-- /.panel -->
</div>