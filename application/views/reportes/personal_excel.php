<?php
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Disposition: attachment; filename=personal_registrado.xls"); 
?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-reportes-personal">
        <thead>
            <tr>
                <th colspan="9"><img width="80%" class="image-responsive" src="<?=base_url('public/img/banner_formatos.jpg')?>">
                    <br><br><br>
                </th>
            </tr>
            <tr>
                <th colspan="9">PERSONAL REGISTRADO EN EL SISTEMA<?php if ($validar!=""){echo ', FILTRADO POR EL RANGO DE FECHA DESDE'.$fechadesde.' HASTA '.$fechahasta;} ?>
                <br><br><br>
            </th>
        </tr>
        <tr>
            <th>#</th>
            <th>Nombres y Apellidos</th>
            <th>C&eacute;dula</th>
            <th>Placa</th>
            <th>Fecha</th>
            <th>Rango</th>
            <th>Tel&eacute;fonos</th>
            <th><?=utf8_decode('Direcci&oacute;n')?></th>
            <th>Estatus</th>
        </tr>
    </thead>
    <tbody>
        <?php  foreach ($personas as $personal):
            $estilo = ($personal->estatus == "Activo") ? 'style="color:#5cb85c; font-weight:bold;"' : 'style="color:#d43f3a; font-weight:bold;"'; 

            ?>
            <tr class="odd gradeX">
                <td><?=utf8_decode($personal->idpersonal)?></td>
                <td><?=utf8_decode($personal->nombre)?></td>
                <td><?=utf8_decode($personal->cedula)?></td>
                <td><?=utf8_decode($personal->placa)?></td>
                <td><?=utf8_decode($personal->fecha)?></td>
                <td><?=utf8_decode($personal->tipo)?></td>
                <td><?=utf8_decode($personal->tlf)?></td>
                <td><?=utf8_decode($personal->direccion)?></td>
                <td align="center" <?=$estilo?>><?=$personal->estatus?></td>
            </tr>
        <?php  endforeach; ?> 
    </tbody>
</table>
</div>