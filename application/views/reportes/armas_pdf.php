<?php
//$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf = new Pdf('H', 'mm', 'LEGAL', true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sispol');
$pdf->SetTitle("Reporte General de Armamentos");
$pdf->SetSubject('PDF');
$pdf->SetKeywords('Reporte General de Armamentos');
$cintillo = "banner_formatos.jpg";
    #PDF_HEADER_LOGO
    //$cintillo = "banner_formatos_valencia.jpg";
// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
$pdf->SetHeaderData($cintillo, "180%", "", "", array(0, 64, 255), array(0, 64, 128));
$pdf->setFooterData($tc = array(0, 64, 0), $lc = array(0, 64, 128));

// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//relación utilizada para ajustar la conversión de los píxeles
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


// ---------------------------------------------------------
// establecer el modo de fuente por defecto
$pdf->setFontSubsetting(true);

// Establecer el tipo de letra

//Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
// Helvetica para reducir el tamaño del archivo.
$pdf->SetFont('dejavusans', '', 8, '', true);

// Añadir una página
// Este método tiene varias opciones, consulta la documentación para más información.
$pdf->AddPage("P","A4");
$txt ='
        <table style="width: 100% !important;" cellpadding="2">
            <thead>
                <tr align="center">
                    <th></th>
                    <th></th>
                    <th><b>FECHA: '.date('d-m-Y').'</b></th>
                </tr>
                <tr align="center">
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <tr align="center">
                    <th colspan="3"><b>REPORTE GENERAL DE ARMAMENTOS</b></th>
                </tr>
                <tr align="center">
                    <th colspan="3"><b></b></th>
                </tr>
            </thead>
';

$txt .='
            <tr style="font-weight: bold;">
                <th width="6%">#</th>
                <th width="12%" align="center">C&oacute;digo</th>
                <th width="13%" align="center">Tipo</th>
                <th width="12%" align="center">Calibre</th>
                <th width="12%" align="center">Fecha</th>
                <th width="18%" align="center">Descripci&oacute;n</th>
                <th width="10%" align="center">Cant.</th>
                <th width="8%" align="center">Cant. Asig.</th>
                <th width="8%" align="center">Disp.</th>
            </tr>
            <hr>
';

    foreach ($armamentos as $armas):

    $txt .='
            <tr>
                <td width="6%">'.$armas->idarma.'</td>
                <td width="12%" align="center">'.utf8_decode($armas->codigo).'</td>
                <td width="13%" align="center">'.utf8_decode($armas->tipo).'</td>
                <td width="12%" align="center">'.utf8_decode($armas->calibre).'</td>
                <td width="12%" align="center">'.utf8_decode($armas->fecha).'</td>
                <td width="18%" align="center">'.utf8_decode($armas->descripcion).'</td>
                <td width="10%" align="center">'.utf8_decode($armas->cantidad).'</td>
                <td width="8%" align="center">'.utf8_decode($armas->cantidad_asignada).'</td>
                <td width="8%" align="center">'.utf8_decode($armas->disponible).'</td>
            </tr>
    ';
    endforeach;

    $pdf->writeHTML($txt, true, false, true, false, '');

    //Close and output PDF document
    $nombre_archivo = utf8_decode("Reporte General de Armamentos.pdf");
    $pdf->Output($nombre_archivo, 'I');