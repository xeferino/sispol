<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list"></i> <b>Listado de Movimientos de Entrada y Salida de Armamentos<?php if ($validar!=""){echo ', filatrado por el rango de fecha desde '.$fechadesde.' hasta '.$fechahasta;} ?></b>
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
       <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover" id="dataTables-reportes-movimientos">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Asignación</th>
                    <th>Devolución</th>
                    <th>T. de Retraso</th>
                    <th>Armas</th>
                    <th>Funcionario</th>
                    <th>Estatus</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $ci =& get_instance();
                $ci->load->model(array("armas_model", "personal_model"));
                if ($movimientos>0):
                foreach ($movimientos as $data): 
                    $personal = $ci->personal_model->get($data->idpersonal);
                    if ($data->tiempoRetraso!="" and $data->tiempoRetraso>="0"){
                        $retraso = explode(":",$data->tiempoRetraso);
                        $tiempo = $retraso[0]." H ".$retraso[1]." M ".$retraso[2]." S"; 
                    }else{
                        $tiempo = "--------------";
                    }
                ?>
                    <tr class="odd gradeX">
                        <td> <?= $data->idpersonalarmamento ?></td>
                        <td> <?= $data->fechaasignacion." ".$data->horaasignacion ?></td>
                        <td> <?= $data->fechadevolucion." ".$data->horadevolucion ?></td>
                        <td> <?= $tiempo ?></td>
                        <td>
                            <?php
                                  $armamentos = explode(',', $data->idarmas);
                                  foreach ($armamentos as $keya => $asignadas) {
                                    $arma = $ci->armas_model->get($asignadas);

                                        $cantArmas = explode(',', $data->cantarmas);
                                        foreach ($cantArmas as $keyc => $cantidad) {
                                            if($keya==$keyc){
                            ?>
                                <button class="btn btn-xs btn-default" title="<?php if($arma->calibre!=""){ echo "Calibre: " .$arma->calibre; } echo "Tipo: ".$arma->tipo." Cantidad: ".$cantidad;?>"><?=$arma->codigo?></button><br>
                             <?php
                                            }
                                        }
                                    }
                             ?>
                        </td>
                        <td> <?= "(".$personal->documento."-".$personal->cedula.") ".$personal->nombres." ".$personal->apellidos." Rango: ".strtoupper($personal->rango)?></td>
                        <td> 
                            <?php if ($data->estatusarma == "Pendiente" or $data->estatusarma == "Vencido"):?>
                                <a href="javascript:;" class="btn btn-danger btn-xs">
                                    <?=$data->estatusarma?>
                                </a>
                                <?php else:?>
                                    <a href="javascript:;" class="btn btn-success btn-xs">
                                        <?=$data->estatusarma?>
                                    </a>
                                <?php endif;?>
                        </td>
                    </tr>
                <?php endforeach;?>
            <?php endif;?>
            </tbody>
        </table>
    </div>
    <?php if ($movimientos>0): ?>
     <div class="col-md-4">
            <form name="form-exportar-armas" target="_blank" method="post" action="reportes/movimientos" target="_blank">
                <input type="hidden" name="desde" value="<?=$fechadesde?>">
                <input type="hidden" name="hasta" value="<?=$fechahasta?>">
                <input type="hidden" name="idpersonal" value="<?=$idpersonal?>">
                <input type="hidden" name="estatus" value="<?=$estatus?>">
                <br>
                <button type="submit" class="btn btn-primary btn-md " id="btn-control-pdf" name="opc" value="pdf"><i class="fa fa-file-pdf-o"></i> PDF <span></span></button>
                <button type="submit" class="btn btn-success btn-md " id="btn-control-excel" name="opc" value="excel"><i class="fa fa-file-excel-o"></i>  
                Excel <span></span></button>
            </form>
        </div>
    <?php endif; ?>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>