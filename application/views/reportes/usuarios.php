
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-users"></i> Usuarios Registrados en el sistema<?php if ($validar!=""){echo ', filatrado por el rango de fecha desde '.$fechadesde.' hasta '.$fechahasta;} ?>
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
       <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover" id="dataTables-reportes-usuarios">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nombres y Apellidos</th>
                    <th>C&eacute;dula</th>
                    <th>Correo</th>
                    <th>Fecha</th>
                    <th>Tipo</th>
                    <th>Usuario</th>
                    <th>Estatus</th>
                </tr>
            </thead>
            <tbody>
                <?php  
                if ($usuarios>0):
                    foreach ($usuarios as $usuario): ?>
                        <tr class="odd gradeX">
                            <td><?=$usuario->idusuario?></td>
                            <td><?=$usuario->nombre?></td>
                            <td><?=$usuario->cedula?></td>
                            <td><?=$usuario->email?></td>
                            <td><?=$usuario->fecha?></td>
                            <td><?=$usuario->tipo?></td>
                            <td><?=$usuario->usuario?></td>
                            <td align="center">
                                <?php if ($usuario->estatus == "Bloqueado"):?>
                                    <a href="javascript:;" class="btn btn-danger btn-xs">
                                        Bloqueado
                                    </a>
                                    <?php else:?>
                                        <a href="javascript:;" class="btn btn-success btn-xs">
                                            Activo
                                        </a>
                                    <?php endif;?>

                                </td>
                            </tr>
                        <?php  endforeach; ?> 
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
        <?php if ($usuarios>0): ?>
            <div class="col-md-4">
                <form name="form-exportar-usuarios" target="_blank" method="post" action="reportes/usuarios" target="_blank">
                    <input type="hidden" name="desde" value="<?=$fechadesde?>">
                    <input type="hidden" name="hasta" value="<?=$fechahasta?>">
                    <input type="hidden" name="estatus" value="<?=$estatus?>">
                    <br>
                    <button type="submit" class="btn btn-primary btn-md " id="btn-control-pdf" name="opc" value="pdf"><i class="fa fa-file-pdf-o"></i> PDF <span></span></button>
                    <button type="submit" class="btn btn-success btn-md " id="btn-control-excel" name="opc" value="excel"><i class="fa fa-file-excel-o"></i>  
                    Excel <span></span></button>
                </form>
            </div>
        <?php endif; ?>
    </div>
    <!-- .panel-body -->
</div>