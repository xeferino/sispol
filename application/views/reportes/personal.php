
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-user"></i> Personal Registrado en el sistema<?php if ($validar!=""){echo ', filatrado por el rango de fecha desde '.$fechadesde.' hasta '.$fechahasta;} ?>
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
       <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover" id="dataTables-reportes-personal">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nombres y Apellidos</th>
                    <th>C&eacute;dula</th>
                    <th>Placa</th>
                    <th>Fecha</th>
                    <th>Rango</th>
                    <th>Tel&eacute;fonos</th>
                    <th>Direcci&oacute;n</th>
                    <th>Estatus</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                if ($personas>0): 
                    foreach ($personas as $personal): ?>
                        <tr class="odd gradeX">
                            <td><?=$personal->idpersonal?></td>
                            <td><?=$personal->nombre?></td>
                            <td><?=$personal->cedula?></td>
                            <td><?=$personal->placa?></td>
                            <td><?=$personal->fecha?></td>
                            <td><?=$personal->tipo?></td>
                            <td><?=$personal->tlf?></td>
                            <td><?=$personal->direccion?></td>
                            <td align="center">
                                <?php if ($personal->estatus == "Bloqueado"):?>
                                    <a href="javascript:;" class="btn btn-danger btn-xs">
                                        Bloqueado
                                    </a>
                                    <?php else:?>
                                        <a href="javascript:;" class="btn btn-success btn-xs">
                                            Activo
                                        </a>
                                    <?php endif;?>

                                </td>
                            </tr>
                        <?php  endforeach; ?> 
                    <?php endif;?>
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
        <?php if ($personas>0): ?>
            <div class="col-md-4">
                <form name="form-exportar-personal" target="_blank" method="post" action="reportes/personal" target="_blank">
                    <input type="hidden" name="desde" value="<?=$fechadesde?>">
                    <input type="hidden" name="hasta" value="<?=$fechahasta?>">
                    <input type="hidden" name="estatus" value="<?=$estatus?>">
                    <input type="hidden" name="rango" value="<?=$rango?>">
                    <br>
                    <button type="submit" class="btn btn-primary btn-md " id="btn-control-pdf" name="opc" value="pdf"><i class="fa fa-file-pdf-o"></i> PDF <span></span></button>
                    <button type="submit" class="btn btn-success btn-md " id="btn-control-excel" name="opc" value="excel"><i class="fa fa-file-excel-o"></i>  
                    Excel <span></span></button>
                </form>
            </div>
        <?php endif; ?>
    </div>
    <!-- .panel-body -->
</div>