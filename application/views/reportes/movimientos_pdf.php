<?php
//$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$ci =& get_instance();
$ci->load->model(array("armas_model", "personal_model"));
$pdf = new Pdf('H', 'mm', 'LEGAL', true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sispol');
$pdf->SetTitle("Reporte General de Movimientos de Armamentos");
$pdf->SetSubject('PDF');
$pdf->SetKeywords('Reporte General de Movimientos de Armamentos');
$cintillo = "banner_formatos.jpg";
    #PDF_HEADER_LOGO
    //$cintillo = "banner_formatos_valencia.jpg";
// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
$pdf->SetHeaderData($cintillo, "180%", "", "", array(0, 64, 255), array(0, 64, 128));
$pdf->setFooterData($tc = array(0, 64, 0), $lc = array(0, 64, 128));

// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//relación utilizada para ajustar la conversión de los píxeles
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


// ---------------------------------------------------------
// establecer el modo de fuente por defecto
$pdf->setFontSubsetting(true);

// Establecer el tipo de letra

//Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
// Helvetica para reducir el tamaño del archivo.
$pdf->SetFont('dejavusans', '', 8, '', true);

// Añadir una página
// Este método tiene varias opciones, consulta la documentación para más información.
$pdf->AddPage("P","A4");
$txt ='
        <table style="width: 100% !important;" cellpadding="2">
            <thead>
                <tr align="center">
                    <th></th>
                    <th colspan="2"></th>
                    <th><b>FECHA: '.date('d-m-Y').'</b></th>
                </tr>
                <tr align="center">
                    <th></th>
                    <th></th>
                    <th colspan="2"></th>
                </tr>
                <tr align="center">
                    <th colspan="4"><b>REPORTE GENERAL DE MOVIMIENTOS DE ARMAMENTOS</b></th>
                </tr>
                <tr align="center">
                    <th colspan="4"><b></b></th>
                </tr>
                <hr>
            </thead>
';
    foreach ($movimientos as $info):
        $estilo = ($info->estatusarma == "Pendiente"  or $info->estatusarma == "Vencido") ? 'style="color:#d43f3a; font-weight:bold;"' : 'style="color:#5cb85c; font-weight:bold;"';
        if ($info->tiempoRetraso!="" and $info->tiempoRetraso>="0"){
            $retraso = explode(":",$info->tiempoRetraso);
            $tiempo = $retraso[0]." H ".$retraso[1]." M ".$retraso[2]." S"; 
        }else{
            $tiempo = "--------------";
        }
        $txt .='
                <tr>
                    <td colspan="4" style="font-weight:bold;">'.
                        "(".$info->documento."-".$info->cedula.") ".$info->nombres." ".$info->apellidos." Rango: ".strtoupper($info->rango)." Placa: ".$info->placa.'
                    </td>
                </tr>
                <br>
                <tr style="font-weight:bold;">
                    <th  align="center">Asignación</th>
                    <th  align="center">Devolución</th>
                    <th  align="center">T. Retraso</th>
                    <th  align="right">Estatus</th>
                </tr>
                <tr>
                    <td  align="center">'.$info->fechaasignacion." ".$info->horaasignacion.'</td>
                    <td  align="center">'.$info->fechadevolucion." ".$info->horadevolucion.'</td>
                    <td  align="center">'.$tiempo.'</td>
                    <td  align="right" '.$estilo.'>'.$info->estatusarma.'</td>
                </tr>
                <br>
                <tr style="font-weight:bold;">
                    <td colspan="3">ARMAS ASIGNADAS</td>
                    <td align="right">Cantidad</td>
                </tr>
                <br>';
                $i = 0;
                $armamentos = explode(',', $info->idarmas);
                foreach ($armamentos as $keya => $asignadas) {
                    $i++;
                    $arma = $ci->armas_model->get($asignadas);
                    $calibre = "";
                    if($arma->calibre!="") {$calibre ="Calibre: ".$arma->calibre;}

                    $armas = "Código (".$arma->codigo.") Tipo: ".$arma->tipo." ".$calibre;
            $txt .='<tr>
                        <td colspan="3">
                            <b>'.$i.')</b> '.$armas.'
                        </td>
                        <td align="right" style="font-weight:bold;">';
                            $cantArmas = explode(',', $info->cantarmas);
                            foreach ($cantArmas as  $keyc => $cantidad) {
                                if ($keya==$keyc) {
                                 $txt.= "".$cantidad."<br>";
                                }
                            }
            $txt .='    </td>
                    </tr>';
                }
        endforeach;
     $txt .='</table>';

    $pdf->writeHTML($txt, true, false, true, false, '');

    //Close and output PDF document
    $nombre_archivo = utf8_decode("Reporte General de Movimientos de Armamentos.pdf");
    $pdf->Output($nombre_archivo, 'I');