<?php
//$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$ci =& get_instance();
$ci->load->model(array("armas_model", "personal_model"));
$pdf = new Pdf('H', 'mm', 'LEGAL', true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sispol');
$pdf->SetTitle("Reporte General de Movimientos de Armamentos");
$pdf->SetSubject('PDF');
$pdf->SetKeywords('Reporte General de Movimientos de Armamentos');
$cintillo = "banner_formatos.jpg";
    #PDF_HEADER_LOGO
    //$cintillo = "banner_formatos_valencia.jpg";
// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
$pdf->SetHeaderData($cintillo, "180%", "", "", array(0, 64, 255), array(0, 64, 128));
$pdf->setFooterData($tc = array(0, 64, 0), $lc = array(0, 64, 128));

// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//relación utilizada para ajustar la conversión de los píxeles
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


// ---------------------------------------------------------
// establecer el modo de fuente por defecto
$pdf->setFontSubsetting(true);

// Establecer el tipo de letra

//Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
// Helvetica para reducir el tamaño del archivo.
$pdf->SetFont('dejavusans', '', 8, '', true);

// Añadir una página
// Este método tiene varias opciones, consulta la documentación para más información.
$pdf->AddPage("P","A4");
//var_dump($movimientos);die();

if($idarmamento=="todos"):
       if ($movimientos!=0):
            $txt ='<table style="width: 100% !important;" cellpadding="2">
                        <thead>
                            <tr align="center">
                                <th colspan="6"></th>
                                <th><b>FECHA: '.date('d-m-Y').'</b></th>
                            </tr>
                            <tr align="center">
                                <th colspan="7"></th>
                            </tr>
                            <tr align="center">
                                <th colspan="7"><b>REPORTE GENERAL DE MOVIMIENTOS DE ARMAMENTOS</b></th>
                            </tr>
                            <tr align="center">
                                <th colspan="7"><b></b></th>
                            </tr>
                    </thead>
                    <tbody>';
                        foreach ($movimientos as $data):  
                        $personal = $ci->personal_model->get($data->idpersonal);
                $txt .='    <tr style="font-weight:bold;">
                                <th colspan="7">Armas:</th>
                            </tr>';
                            $armamentos = explode(',', $data->idarmas);
                            $i=0;
                            foreach ($armamentos as $keya => $asignadas):
                                $i++;
                                $estilo = ($data->estatusarma == "Pendiente"  or $data->estatusarma == "Vencido") ? 'style="color:#d43f3a; font-weight:bold;"' : 'style="color:#5cb85c; font-weight:bold;"';
                                $arma = $ci->armas_model->get($asignadas);
                                $armas = "Código (".$arma->codigo.") Tipo: ".$arma->tipo." Calibre: ".$arma->calibre;
                                $calibre = ""; 
                                if($arma->calibre!=""): 
                                    $calibre = " Calibre: ".$arma->calibre;
                                endif;
                                $cantArmas = explode(',', $data->cantarmas);
                                $i=0;
                                foreach ($cantArmas as $keyc => $cantidad):
                                $i++;  
                                    if($keya==$keyc):
                            $txt .='       <tr style="font-weight:bold;">
                                                <td colspan="7">
                                                    <b>'.$i.')</b> '.$armas.'
                                                </td>
                                            </tr>';
                                    endif;
                                endforeach;
                            endforeach;
                $txt .='     <tr style="font-weight:bold;">
                                <th colspan="7">Funcionario:</th>
                            </tr>
                            <tr style="font-weight:bold;">
                                <td>#</td>
                                <td>Nombre y Apellido</td>
                                <td>C&eacute;dula</td>
                                <td>Placa</td>
                                <td>Rango</td>
                                <td>Fec. Asig. / Fec. Dev.</td>
                                <td>Estatus</td>
                            </tr>
                            <tr>
                                <td>1</td>';
                        $txt .='<td>'.$personal->nombres." ".$personal->apellidos.'</td>
                                <td>'.$personal->documento."-".$personal->cedula.'</td>
                                <td>'.$personal->placa.'</td>
                                <td>'.$personal->rango.'</td>
                                <td>'.$data->fechaasignacion." ".$data->horaasignacion." / ".$data->fechadevolucion." ".$data->horadevolucion.'</td>
                                <td '.$estilo.'>'.$data->estatusarma.'</td>
                            </tr>';
                        endforeach;
            $txt .='</tbody>
                </table>';
            $pdf->writeHTML($txt, true, false, true, false, '');
        else:
            $txt='
                <p style="font-weight: bold; font-size:14px;" align="center">
                    No hay resultados<br>
                </p>';
            $pdf->writeHTML($txt, true, false, true, false, '');
        endif;

else:
     $arma = $ci->armas_model->get($idarmamento);
     $calibre = ""; 
     if($arma->calibre!=""): 
          $calibre = " Calibre: ".$arma->calibre;
     endif; 
     $armasMovi = "Código (".$arma->codigo.") Tipo: ".$arma->tipo." ".$calibre;
$txt ='<table style="width: 100% !important;" cellpadding="2">
        <thead>
        </thead>
        <tbody>';
          if($movimientos>0):
             foreach ($movimientos as $data):
                 $armamentos = explode(',', $data->idarmas);
                 $validar = []; 
                 foreach ($armamentos as $a):
                     if($idarmamento==$a):
                         $validar [] = $a;
                     endif;
                endforeach; 
            endforeach;
        endif; 
         if(count($validar)>0):
        $txt .='<tr>
                    <th colspan="7">
                        Arma: '.$armasMovi.'
                    </th>
                </tr>';
                 foreach ($movimientos as $data): 
                     $armamentos = explode(',', $data->idarmas);
                     $personal = $ci->personal_model->get($data->idpersonal);
                     $i=0;
                     foreach ($armamentos as $a):
                            $estilo = ($data->estatusarma == "Pendiente"  or $data->estatusarma == "Vencido") ? 'style="color:#d43f3a; font-weight:bold;"' : 'style="color:#5cb85c; font-weight:bold;"';
                         if($idarmamento==$a): 
                            $i++;
                        $txt .='<tr style="font-weight:bold;">
                                    <td>#</td>
                                    <td>Nombre y Apellido</td>
                                    <td>C&eacute;dula</td>
                                    <td>Placa</td>
                                    <td>Rango</td>
                                    <td>Fec. Asig. / Fec. Dev.</td>
                                    <td>Estatus</td>
                                </tr>
                                <tr>
                                    <td>1</td>';
                        $txt .='    <td>'.$personal->nombres." ".$personal->apellidos.'</td>
                                    <td>'.$personal->documento."-".$personal->cedula.'</td>
                                    <td>'.$personal->placa.'</td>
                                    <td>'.$personal->rango.'</td>
                                    <td>'.$data->fechaasignacion." ".$data->horaasignacion." / ".$data->fechadevolucion." ".$data->horadevolucion.'</td>
                                    <td '.$estilo.'>'.$data->estatusarma.'</td>
                                </tr>';
                        endif;
                    endforeach;
                endforeach;
            endif;
$txt .=' </tbody>
    </table>';
    if(count($validar)>0):
        $pdf->writeHTML($txt, true, false, true, false, '');
    else:
        $txt='
                <p style="font-weight: bold; font-size:14px;" align="center">
                    No hay resultados<br>
                </p>';
            $pdf->writeHTML($txt, true, false, true, false, '');
    endif;
endif;
    

    //Close and output PDF document
    $nombre_archivo = utf8_decode("Reporte General de Movimientos de Armamentos.pdf");
    $pdf->Output($nombre_archivo, 'I');