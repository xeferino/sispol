       <!-- /.row -->
    <div class="row">
        <div class="col-lg-12" id="auditoria">
            <div class="panel panel-default">
                <div class="panel-heading">
                   <i class="fa fa-list"></i> <b>Auditoria del Sistema</b>
                   <a href="reportes/auditoria" target="_blank" class="btn btn-default btn-xs pull-right"> <i class="fa fa-file-excel-o"></i> Exportar Excel</a>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-auditoria">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Acci&oacute;n</th>
                                    <th>Tabla BD</th>
                                    <th>Fecha</th>
                                    <th>Informaci&oacute;n Actual</th>
                                    <th>Informaci&oacute;n Anterior</th>
                                    <th>Usuario</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($auditoria as $data): ?>
                                    <tr class="odd gradeX">
                                        <td><?=$data->id?></td>
                                        <td><?=$data->accion?></td>
                                        <td><?=$data->tabla?></td>
                                        <td><?=$data->fechahora?></td>
                                        <td><?=$data->informacion_new?></td>
                                        <td><?=$data->informacion_old?></td>
                                        <td>
                                            <a href="javascript:;" title="Usuario" class="btn btn-primary btn-xs btn-circle" onclick="getUser('<?=$data->idusuario?>')">
                                            <i class="fa fa-user"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <br>
                    <div class="well">
                        <h4>Informaci&oacute;n</h4>
                        <p>Aqui se muestra los movimientos de cada usuario en el sistema, segun la acci&oacute;n realizada en la aplicaci&oacute;n. </p>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    
      <!-- Modal Auditoria -->
    <div class="modal fade" id="modal-form-auditoria" tabindex="-1"
         role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div id="usuario" style="text-transform: uppercase;">
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal Auditoria-->