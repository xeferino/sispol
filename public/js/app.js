$(document).ready(function() {
    inicializar();
    relojTime();
    getProfile();

    var longitud = false,
    minuscula = false,
    numero = false,
    mayuscula = false,
    caracter = false;
    $('#clave').keyup(function() {
        var clave = $(this).val();

        if (clave.length < 8) {
            $('#length').removeClass('valid').addClass('invalid');
            longitud = false;
        } else {
            $('#length').removeClass('invalid').addClass('valid');
            longitud = true;
        }

        //validate letter
        if (clave.match(/[A-z]/)) {
            $('#letter').removeClass('invalid').addClass('valid');
            minuscula = true;
        } else {
            $('#letter').removeClass('valid').addClass('invalid');
            minuscula = false;
        }

        //validate caracter
        if (clave.match(/[\.\,\%]/)) {
            $('#caracter').removeClass('invalid').addClass('valid');
            caracter = true;
        } else {
            $('#caracter').removeClass('valid').addClass('invalid');
            caracter = false;
        }

        //validate capital letter
        if (clave.match(/[A-Z]/)) {
            $('#capital').removeClass('invalid').addClass('valid');
            mayuscula = true;
        } else {
            $('#capital').removeClass('valid').addClass('invalid');
            mayuscula = false;
        }

        //validate number
        if (clave.match(/\d/)) {
            $('#number').removeClass('invalid').addClass('valid');
            numero = true;
        } else {
            $('#number').removeClass('valid').addClass('invalid');
            numero = false;
        }

        if(longitud == true && minuscula == true && mayuscula == true && numero == true  && caracter == true){
            $('#btn-inicio-agregar').attr('disabled', false);
            $('#btn-inicio-recuperar').attr('disabled', false);
            $('#btn-inicio-perfil').attr('disabled', false);
            
        }else{
            $('#btn-inicio-agregar').attr('disabled', true);
            $('#btn-inicio-recuperar').attr('disabled', true);
            $('#btn-inicio-perfil').attr('disabled', true);
        }
  });

  $('#claver').keyup(function() {
    var claver = $(this).val();

    if (claver.length < 8) {
        $('#length').removeClass('valid').addClass('invalid');
        longitud = false;
    } else {
        $('#length').removeClass('invalid').addClass('valid');
        longitud = true;
    }

    //validate letter
    if (claver.match(/[A-z]/)) {
        $('#letter').removeClass('invalid').addClass('valid');
        minuscula = true;
    } else {
        $('#letter').removeClass('valid').addClass('invalid');
        minuscula = false;
    }

    //validate caracter
    if (claver.match(/[\.\,\%]/)) {
        $('#caracter').removeClass('invalid').addClass('valid');
        caracter = true;
    } else {
        $('#caracter').removeClass('valid').addClass('invalid');
        caracter = false;
    }

    //validate capital letter
    if (claver.match(/[A-Z]/)) {
        $('#capital').removeClass('invalid').addClass('valid');
        mayuscula = true;
    } else {
        $('#capital').removeClass('valid').addClass('invalid');
        mayuscula = false;
    }

    //validate number
    if (claver.match(/\d/)) {
        $('#number').removeClass('invalid').addClass('valid');
        numero = true;
    } else {
        $('#number').removeClass('valid').addClass('invalid');
        numero = false;
    }

    if(longitud == true && minuscula == true && mayuscula == true && numero == true && caracter == true){
        $('#btn-inicio-agregar').attr('disabled', false);
        $('#btn-inicio-recuperar').attr('disabled', false);
        $('#btn-inicio-perfil').attr('disabled', false);
    }else{
        $('#btn-inicio-agregar').attr('disabled', true);
        $('#btn-inicio-recuperar').attr('disabled', true);
        $('#btn-inicio-perfil').attr('disabled', true);
    }
});

    /**
     *  Ajax Setup
     */
    $.ajaxSetup({
        error: function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                //msj_error('Not connected: Verify Network. Please try again later', 3000);
            } else if (jqXHR.status == 404) {
                msj_error('Requested page not found [404]. Please try again later', 3000);
            } else if (jqXHR.status == 500) {
                msj_error('Internal Server Error [500]. Please try again later', 3000);
            } else if (textStatus === 'parsererror') {
                msj_error('Requested parse failed. Please try again later', 3000);
            } else if (textStatus === 'timeout') {
                msj_error('Time out error. Please try again later', 3000);
            } else if (textStatus === 'abort') {
                msj_error('Ajax request aborted. Please try again later', 3000);
            } else {
                msj_error('Uncaught Error: ' + jqXHR.responseText + '. Please Try again Later', 3000);
           }
           close_loading();
        }
    });

    $(".numeric").keypress(function(e) {
            var key = window.Event ? e.which : e.keyCode
                return ((key >= 48 && key <= 57) || (key==8) || (key==46)) 
    });

    $("input[type='text'], input[type='email'], textarea").change(function(){
      var valor = $(this).val();
      if($(this).attr("name") != "clave" && $(this).attr("name") != "claver"){
        $(this).val(valor.toUpperCase());
      }
    });

    $("#btn-regresar").click(function(e) {
        window.history.back();
    });
});

/**
 *  Inicializar plugins
 *
 *  @return {void}
 */
function inicializar() {
    if (typeof jQuery.fn.datepicker == 'function') {
        $('input.datepicker').datepicker({changeYear:true});
    }
    if (typeof jQuery.fn.mask == 'function') {
        $.mask.definitions['~']='[+-]';
        $('input.rif').mask("a-99999999-9");
        $('input.letter').mask("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        $('input.phone').mask('(9999) 999-9999');
        $("input.placa").mask("99999999");
        $("input.codigo").mask("9999999999");
    }
    if (typeof jQuery.fn.timeEntry == 'function') {
        $('.time').timeEntry({
            show24Hours:true,
            timeSteps:[1,1,1]
        });
    }
    if (typeof jQuery.fn.select2 == 'function') {
        $("select").select2();
    }
}

/*function de salir del sistema*/
function logout() {
    open_loading();
    $.post(base_url()+"/auth/logout/", function(data){
        $('#logout').modal('hide');
        setTimeout(function () {location.href = $("#base_url").val() }, 1000);
   });
    close_loading();
}

/*function de update perfil*/
    $("#form-perfil-user").submit(function(){
        var  clave  = $("#clave_perfil").val();
        var  claver = $("#claver_perfil").val();
        var  idusuario = $("#idusuario_perfil").val();
        open_loading();
        if (clave != claver) {
            msj_advertencia("las claves no coinciden", 5000);
        }else{
            if (idusuario) {
                $.post(base_url()+"usuario/updatePerfil", $(this).serialize(), function(data){
                    var resp = data.split("::");
                    if(resp[0] == "exito"){
                        msj_exito("Perfil actualizado correctamente", 5000);
                        clearFormPerfil();
                        $('#perfil').modal('hide');
                    }else if(resp[0] == "cedula"){
                        msj_advertencia("la cédula ("+resp[2]+"-"+resp[1]+") ingresada ya existe!", 5000);  
                    }else if(resp[0] == "email"){
                        msj_advertencia("el email ("+resp[1]+") ingresado ya existe!", 5000);   
                    }else if(resp[0] == "nombreusuario"){
                        msj_advertencia("el usuario ("+resp[1]+") ingresado ya existe!", 5000); 
                    }else{
                        msj_error("Error, Intente mas tarde", 5000);
                    }
                });
            }
        }
        close_loading();
        return false;
    });

    
    /*function limpiar form*/
    function clearFormPerfil(){
        /*Form Perfil*/
        $("#idusuario_perfil").val("");
        $("#nombre_perfil").val("");
        $("#cedula_perfil").val("");
        $("#usuario_perfil").val("");
        $("#clave_perfil").val("");
        $("#pregunta_perfil").val("");
        $("#apellido_perfil").val("");
        $("#email_perfil").val("");
        $("#tipo_perfil").select2("val", "");
        $("#documento_perfil").select2("val", "");
        $("#claver_perfil").val("");
        $("#respuesta_perfil").val("");
        $('#perfil').modal('hide');

    }


        /*function DataTableUser list*/
    function perfil(idusuario){
        open_loading();
            $.post(base_url()+"usuario/getUser/"+idusuario, {}, function(data){
                var resp = data.split("::");
                //modal-perfil
                $("#idusuario_perfil").val(resp[0]);
                $("#nombre_perfil").val(resp[1]);
                $("#cedula_perfil").val(resp[3]);
                $("#usuario_perfil").val(resp[5]);
                $("#pregunta_perfil").val(resp[8]);
                $("#apellido_perfil").val(resp[2]);
                $("#email_perfil").val(resp[4]);
                $("#tipo_perfil").select2("val", resp[7]);
                $("#respuesta_perfil").val(resp[9]);
                $("#documento_perfil").select2("val", resp[10]);

                $("#btn-inicio-perfil").val("Editar");
                close_loading();
                $('#perfil').modal({backdrop: 'static', keyboard: false});
            });
    }

    function getProfile() {
        errorData = $("#perfilError").val();
        if(errorData == "exito"){
            msj_exito("Perfil actualizado correctamente", 1000);
            setTimeout(function () {location.href = $("#base_url").val()+"profile" }, 2000);
        }else if(errorData=="clave"){
            msj_advertencia("las claves no coinciden", 5000);
        }else if(errorData == "cedula"){
            msj_advertencia("la cédula ingresada ya existe!", 5000);  
        }else if(errorData == "email"){
            msj_advertencia("el email ingresado ya existe!", 5000);   
        }else if(errorData == "correo"){
            msj_advertencia("el email ingresado no es valido, verifique!", 5000);   
        }else if(errorData == "usuario"){
            msj_advertencia("el usuario ingresado ya existe!", 5000); 
        }else if(errorData == "error"){
            msj_error("Error, Intente mas tarde", 5000);
        }
    }

/**
 *  Formatear un número
 *
 *  @param {decimal}    Número a formatear
 *  @param {integer}    Cantidad de decimales
 *  @param {string}     Signo de decimales
 *  @param {string}     Signo separador de miles
 */
function number_format(number, decimals, decPoint, thousandsSep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
    var n = !isFinite(+number) ? 0 : +number
    var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
    var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
    var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
    var s = ''
    var toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec)
        return '' + (Math.round(n * k) / k)
        .toFixed(prec)
    }
    // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
    }
    return s.join(dec)
}

/**
 *  Url base del sistema
 *
 *  @return {string}
 */
function base_url(){
    var base_url = $("#base_url").val()+"index.php/";

    return base_url;    
}

/**
 *  Url base del sistema
 *
 *  @return {string}
 */
function msj_exito(mensaje, tiempo){
    $("#msj_exito div").html('<i class="fa fa-check-circle fa-2x"></i> &nbsp;'+mensaje);
    $("#msj_exito").fadeIn(500);
    setTimeout("close_msj('exito')",tiempo);    
}

/**
 *  Url base del sistema
 *
 *  @return {string}
 */
function msj_error(mensaje, tiempo){
    $("#msj_error div").html('<i class="fa fa-times-circle fa-2x"></i> &nbsp;'+mensaje);
    $("#msj_error").fadeIn(500);
    setTimeout("close_msj('error')",tiempo);    
}

/**
 *  Url base del sistema
 *
 *  @return {string}
 */
function msj_advertencia(mensaje, tiempo){
    $("#msj_advertencia div").html('<i class="fa fa-minus-circle fa-2x"></i> &nbsp;'+mensaje);
    $("#msj_advertencia").fadeIn(500);
    setTimeout("close_msj('advertencia')",tiempo);  
}

/**
 *  Url base del sistema
 *
 *  @return {string}
 */
function close_msj(tipo){
    $("#msj_"+tipo).fadeOut(500);   
}

/**
 *  Url base del sistema
 *
 *  @return {string}
 */
function redirect(url, tiempo){
    setTimeout("location.href='"+base_url()+url+"'",tiempo);    
}

/**
 *  Url base del sistema
 *
 *  @return {string}
 */
function open_loading(){
    $("#div_loading").fadeIn(500);  
}

/**
 *  Url base del sistema
 *
 *  @return {string}
 */
function close_loading(){
    $("#div_loading").fadeOut(1000);    
}


function relojTime(){
    var fecha = new Date()
    var hora = fecha.getHours()
    var minuto = fecha.getMinutes()
    var segundo = fecha.getSeconds()
    if (hora < 10) {hora = "0" + hora}
    if (minuto < 10) {minuto = "0" + minuto}
    if (segundo < 10) {segundo = "0" + segundo}
    var reloj = hora + ":" + minuto + ":" + segundo
    $("#hora").html(reloj) 
    tiempo = setTimeout('relojTime()',1000)
}

function keyletterNumber(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz0123456789";
    especiales = "8-37-39-46";

    tecla_especial = false
    for(var i in especiales){
         if(key == especiales[i]){
             tecla_especial = true;
             break;
         }
     }

     if(letras.indexOf(tecla)==-1 && !tecla_especial){
         return false;
     }
 }

 function keyLetter(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = "8-37-39-46";

    tecla_especial = false
    for(var i in especiales){
         if(key == especiales[i]){
             tecla_especial = true;
             break;
         }
     }

     if(letras.indexOf(tecla)==-1 && !tecla_especial){
         return false;
     }
 }

 function keyPassword(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ0123456789.,%";
    especiales = "8-37-39-46";

    tecla_especial = false
    for(var i in especiales){
         if(key == especiales[i]){
             tecla_especial = true;
             break;
         }
     }

     if(letras.indexOf(tecla)==-1 && !tecla_especial){
         return false;
     }
 }

 function eyesPerfil(o) {
	var email = $("#eyes-click-email").html();
	var user = $("#eyes-click-user").html();
	var c1 = $("#eyes-click-clave").html();
	var c2 = $("#eyes-click-claver").html();
	var p = $("#eyes-click-pregunta").html();
	var r = $("#eyes-click-respuesta").html();
	
	if(o=="email"){
		if(email=='<i class="fa fa-eye-slash"></i>'){
			$("#eyes-click-email").html('<i class="fa fa-eye"></i>');
			$("#email").removeAttr('type', 'password');
			$("#email").attr('type', 'text');
		}else{
			$("#eyes-click-email").html('<i class="fa fa-eye-slash"></i>');
			$("#email").removeAttr('type', 'text');
			$("#email").attr('type', 'password');
		}
	}

	if(o=="user"){
		if(user=='<i class="fa fa-eye-slash"></i>'){
			$("#eyes-click-user").html('<i class="fa fa-eye"></i>');
			$("#usuario").removeAttr('type', 'password');
			$("#usuario").attr('type', 'text');
		}else{
			$("#eyes-click-user").html('<i class="fa fa-eye-slash"></i>');
			$("#usuario").removeAttr('type', 'text');
			$("#usuario").attr('type', 'password');
		}
	}

	if(o=="clave"){
		if(c1=='<i class="fa fa-eye-slash"></i>'){
			$("#eyes-click-clave").html('<i class="fa fa-eye"></i>');
			$("#clave").removeAttr('type', 'password');
			$("#clave").attr('type', 'text');
		}else{
			$("#eyes-click-clave").html('<i class="fa fa-eye-slash"></i>');
			$("#clave").removeAttr('type', 'text');
			$("#clave").attr('type', 'password');
		}
	}

	if(o=="claver"){
		if(c2=='<i class="fa fa-eye-slash"></i>'){
			$("#eyes-click-claver").html('<i class="fa fa-eye"></i>');
			$("#claver").removeAttr('type', 'password');
			$("#claver").attr('type', 'text');
		}else{
			$("#eyes-click-claver").html('<i class="fa fa-eye-slash"></i>');
			$("#claver").removeAttr('type', 'text');
			$("#claver").attr('type', 'password');
		}
	}

	if(o=="pregunta"){
		if(p=='<i class="fa fa-eye-slash"></i>'){
			$("#eyes-click-pregunta").html('<i class="fa fa-eye"></i>');
			$("#pregunta").removeAttr('type', 'password');
			$("#pregunta").attr('type', 'text');
		}else{
			$("#eyes-click-pregunta").html('<i class="fa fa-eye-slash"></i>');
			$("#pregunta").removeAttr('type', 'text');
			$("#pregunta").attr('type', 'password');
		}
	}

	if(o=="respuesta"){
		if(r=='<i class="fa fa-eye-slash"></i>'){
			$("#eyes-click-respuesta").html('<i class="fa fa-eye"></i>');
			$("#respuesta").removeAttr('type', 'password');
			$("#respuesta").attr('type', 'text');
		}else{
			$("#eyes-click-respuesta").html('<i class="fa fa-eye-slash"></i>');
			$("#respuesta").removeAttr('type', 'text');
			$("#respuesta").attr('type', 'password');
		}
	}
}
