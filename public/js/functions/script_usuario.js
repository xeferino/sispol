$(document).ready(function() {
	/*dataTables list User*/
	DataTableUser();
	$("#btn-inicio-agregar").val("Agregar");

	/*function de add/update user*/
	var url = $("#base_url").val();
    $("#form-add-user").submit(function(){
	    var  clave  = $("#clave").val();
	    var  claver = $("#claver").val();
		var  idusuario = $("#idusuario").val();
		var correo = $("#email").val();
	    open_loading();
	    if (clave != claver) {
	    	msj_advertencia("las claves no coinciden", 5000);
	    }else if (correo.match(/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i)) {
	    	if (idusuario) {
	    		$.post(base_url()+"usuario/updateUser", $(this).serialize(), function(data){
			    	var resp = data.split("::");
			    	if(resp[0] == "exito"){
			    		clearForm();
			    		msj_exito("Usuario actualizado correctamente", 5000);
			    		DataTableUser();
			    	}else if(resp[0] == "cedula"){
			    		msj_advertencia("la cédula ("+resp[2]+"-"+resp[1]+") ingresada ya existe!", 5000);	
			    	}else if(resp[0] == "email"){
			    		msj_advertencia("el email ("+resp[1]+") ingresado ya existe!", 5000);	
			    	}else if(resp[0] == "nombreusuario"){
			    		msj_advertencia("el usuario ("+resp[1]+") ingresado ya existe!", 5000);	
			    	}else if(resp[0] == "modulo"){
			    		msj_advertencia("Disculpe, debe seleccionar al menos un modulo!", 5000);	
			    	}else{
			    		msj_error("Error, Intente mas tarde", 5000);
			    	}
			    });
	    	}else{
			    $.post(base_url()+"usuario/registerUser", $(this).serialize(), function(data){
			    	var resp = data.split("::");
			    	if(resp[0] == "exito"){
			    		clearForm();
			    		msj_exito("Usuario Registrado correctamente", 5000);
			    		DataTableUser();
			    	}else if(resp[0] == "cedula"){
			    		msj_advertencia("la cédula ("+resp[2]+"-"+resp[1]+") ingresada ya existe!", 5000);	
			    	}else if(resp[0] == "email"){
			    		msj_advertencia("el email ("+resp[1]+") ingresado ya existe!", 5000);	
			    	}else if(resp[0] == "nombreusuario"){
			    		msj_advertencia("el usuario ("+resp[1]+") ingresado ya existe!", 5000);	
			    	}else if(resp[0] == "modulo"){
			    		msj_advertencia("Disculpe, debe seleccionar al menos un modulo!", 5000);	
			    	}else{
			    		msj_error("Error, Intente mas tarde", 5000);
			    	}
			    });
	    	}
		}else{
			msj_advertencia("El correo ingresado es incorreto, verifique!", 5000);
	    }
	    close_loading();
	    return false;
    });

    /*function seleccionar all Checkbox*/
    $("#allCheckbox").change(function (){
       	$("input:checkbox").prop('checked', $(this).prop("checked"));
	});
	
});

function eyes(o) {
	var email = $("#eyes-click-email").html();
	var user = $("#eyes-click-user").html();
	var c1 = $("#eyes-click-clave").html();
	var c2 = $("#eyes-click-claver").html();
	var p = $("#eyes-click-pregunta").html();
	var r = $("#eyes-click-respuesta").html();
	
	if(o=="email"){
		if(email=='<i class="fa fa-eye-slash"></i>'){
			$("#eyes-click-email").html('<i class="fa fa-eye"></i>');
			$("#email").removeAttr('type', 'password');
			$("#email").attr('type', 'text');
		}else{
			$("#eyes-click-email").html('<i class="fa fa-eye-slash"></i>');
			$("#email").removeAttr('type', 'text');
			$("#email").attr('type', 'password');
		}
	}

	if(o=="user"){
		if(user=='<i class="fa fa-eye-slash"></i>'){
			$("#eyes-click-user").html('<i class="fa fa-eye"></i>');
			$("#usuario").removeAttr('type', 'password');
			$("#usuario").attr('type', 'text');
		}else{
			$("#eyes-click-user").html('<i class="fa fa-eye-slash"></i>');
			$("#usuario").removeAttr('type', 'text');
			$("#usuario").attr('type', 'password');
		}
	}

	if(o=="clave"){
		if(c1=='<i class="fa fa-eye-slash"></i>'){
			$("#eyes-click-clave").html('<i class="fa fa-eye"></i>');
			$("#clave").removeAttr('type', 'password');
			$("#clave").attr('type', 'text');
		}else{
			$("#eyes-click-clave").html('<i class="fa fa-eye-slash"></i>');
			$("#clave").removeAttr('type', 'text');
			$("#clave").attr('type', 'password');
		}
	}

	if(o=="claver"){
		if(c2=='<i class="fa fa-eye-slash"></i>'){
			$("#eyes-click-claver").html('<i class="fa fa-eye"></i>');
			$("#claver").removeAttr('type', 'password');
			$("#claver").attr('type', 'text');
		}else{
			$("#eyes-click-claver").html('<i class="fa fa-eye-slash"></i>');
			$("#claver").removeAttr('type', 'text');
			$("#claver").attr('type', 'password');
		}
	}

	if(o=="pregunta"){
		if(p=='<i class="fa fa-eye-slash"></i>'){
			$("#eyes-click-pregunta").html('<i class="fa fa-eye"></i>');
			$("#pregunta").removeAttr('type', 'password');
			$("#pregunta").attr('type', 'text');
		}else{
			$("#eyes-click-pregunta").html('<i class="fa fa-eye-slash"></i>');
			$("#pregunta").removeAttr('type', 'text');
			$("#pregunta").attr('type', 'password');
		}
	}

	if(o=="respuesta"){
		if(r=='<i class="fa fa-eye-slash"></i>'){
			$("#eyes-click-respuesta").html('<i class="fa fa-eye"></i>');
			$("#respuesta").removeAttr('type', 'password');
			$("#respuesta").attr('type', 'text');
		}else{
			$("#eyes-click-respuesta").html('<i class="fa fa-eye-slash"></i>');
			$("#respuesta").removeAttr('type', 'text');
			$("#respuesta").attr('type', 'password');
		}
	}
}

	/*function limpiar form*/
    function clearForm(){
    	$('#modal-form-add').modal('hide');
    	$("#btn-inicio-agregar").val("Agregar");
    	$("#idusuario").val("");
		$("#nombre").val("");
		$("#cedula").val("");
		$("#usuario").val("");
		$("#clave").val("");
		$("#pregunta").val("");
		$("#apellido").val("");
		$("#email").val("");
		$("#tipo").select2("val", "");
		$("#documento").select2("val", "");
		$("#claver").val("");
		$("#respuesta").val("");
		$("input:checkbox").prop('checked', false);
	}

	/*function DataTableUser list*/
	function DataTableUser(){
		$("#refreshUsers").html('<div align="center" style="color: #d20d0d; font-weight: bold;"><i class="fa fa-spinner fa-pulse fa-2x"></i> <b style="font-size: 15px;">Cargando!  por favor espere...</b></div>');
    	$.post(base_url()+"usuario/listDataTableUser", $(this).serialize(), function(data){
    		$("#refreshUsers").html(data);
    		$('#dataTables-usuarios').DataTable({
	    		responsive: true,
	    		language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
			        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Registros",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
    		});
		});
	}

	/*function DataTableUser list*/
	function getUser(idusuario){
		open_loading();
	    	$.post(base_url()+"usuario/getUser/"+idusuario, {}, function(data){
	    		var resp = data.split("::");
	    		$("#idusuario").val(resp[0]);
	    		$("#nombre").val(resp[1]);
				$("#cedula").val(resp[3]);
				$("#usuario").val(resp[5]);
				$("#pregunta").val(resp[8]);
				$("#apellido").val(resp[2]);
				$("#email").val(resp[4]);
				$("#tipo").select2("val", resp[7]);
				$("#respuesta").val(resp[9]);
				$("#documento").select2("val", resp[11]);

				dataModulos = JSON.parse(resp[10]);
				dataModulos = dataModulos['dataModulos'];

			    for (var key in dataModulos){
			    	$('#modulo' + key).prop('checked',true);
			    }

				$("#btn-inicio-agregar").val("Editar");
				$("#clave").attr("disabled");
				$("#claver").attr("disabled");
				$('#modal-form-add').modal({backdrop: 'static', keyboard: false});
				$("#texto").html("Aqui puedes editar la información del usuario seleccionado!");
	    		close_loading();
			});
	}

	/*function getUserEstatus list*/
	function getUserEstatus(idusuario){
		$.post(base_url()+"usuario/getUser/"+idusuario, {}, function(data){
			var resp = data.split("::");
			$("#idusuario").val(resp[0]);
			$("#btn-inicio-estatus").val("Ok");
			$('#modal-form-estatus').modal({backdrop: 'static', keyboard: false});
		});
	}

	/*function updateUserEstatus list*/
	function updateUserEstatus(){
		idusuario = $("#idusuario").val();
		open_loading();
    	$.post(base_url()+"usuario/updateUserEstatus/"+idusuario, {}, function(data){
			if(data == "exito"){
			    msj_exito("Estatus Actualizado correctamente", 3000);
			    $('#modal-form-estatus').modal('hide');
			    DataTableUser();
			}else{
				msj_error("Error, Intente mas tarde", 3000);
			}
    		close_loading();
		});
	}