$(document).ready(function() {
	/*dataTables list Personal*/
	DataTablePersonal();
	$("#btn-inicio-agregar").val("Agregar");

	/*function de add/update user*/
	var url = $("#base_url").val()+"index.php/";
    $("#form-personal").submit(function(){
	    var  idpersonal = $("#idpersonal").val();
	    open_loading();
    	if (idpersonal){
    		$.post(base_url()+"personal/updatePersonal", $(this).serialize(), function(data){
		    	var resp = data.split("::");
		    	if(resp[0] == "exito"){
		    		clearForm();
		    		msj_exito("Personal Actualizado correctamente", 5000);
		    		DataTablePersonal();
		    	}else if(resp[0] == "cedula"){
		    		msj_advertencia("la cédula ("+resp[1]+"-"+resp[2]+") ingresada ya existe!", 5000);	
		    	}else if(resp[0] == "placa"){
		    		msj_advertencia("la placa ("+resp[1]+") ingresado ya existe!", 5000);	
		    	}else{
		    		msj_error("Error, Intente mas tarde", 5000);
		    	}
		    });
    	}else{
		    $.post(base_url()+"personal/registerPersonal", $(this).serialize(), function(data){
		    	var resp = data.split("::");
		    	if(resp[0] == "exito"){
		    		clearForm();
		    		msj_exito("Personal Registrado correctamente", 5000);
		    		DataTablePersonal();
		    	}else if(resp[0] == "cedula"){
		    		msj_advertencia("la cédula ("+resp[1]+"-"+resp[2]+") ingresada ya existe!", 5000);	
		    	}else if(resp[0] == "placa"){
		    		msj_advertencia("la placa ("+resp[1]+") ingresada ya existe!", 5000);	
		    	}else{
		    		msj_error("Error, Intente mas tarde", 5000);
		    	}
		    });
    	}
	    close_loading();
	    return false;
    });
});

	/*function limpiar form*/
    function clearForm(){
    	$('#modal-form-personal').modal('hide');		
    	$("#btn-inicio-agregar").val("Agregar");
    	$("#idpersonal").val("");
		$("#nombre").val("");
		$("#apellido").val("");
		$("#documento").select2("val", "");
		$("#cedula").val("");
		$("#placa").val("");
		$("#direccion").val("");
		$("#telefono_habitacion").val("");
		$("#telefono_celular").val("");
		$("#rango").select2("val", "");
		$("#sexo").select2("val", "");
	}

	/*function DataTablePersonal list*/
	function DataTablePersonal(){
		$("#personal").html('<div align="center" style="color: #d20d0d; font-weight: bold;"><i class="fa fa-spinner fa-pulse fa-2x"></i> <b style="font-size: 15px;">Cargando!  por favor espere...</b></div>');
    	$.post(base_url()+"personal/listDataTablePersonal", $(this).serialize(), function(data){
    		$("#personal").html(data);
    		$('#dataTables-personal').DataTable({
	    		responsive: true,
	    		language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
			        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Registros",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
    		});
		});
	}


	/*function DataTablePersonal list*/
	function getPersonal(idpersonal){
		open_loading();
	    	$.post(base_url()+"personal/getPersonal/"+idpersonal, {}, function(data){
	    		var resp = data.split("::");
	    		$("#idpersonal").val(resp[0]);
	    		$("#nombre").val(resp[1]);
	    		$("#apellido").val(resp[2]);
	    		$("#documento").select2("val", resp[3]);
				$("#cedula").val(resp[4]);
				$("#placa").val(resp[5]);
				$("#direccion").val(resp[6]);
				$("#telefono_habitacion").val(resp[7]);
				$("#telefono_celular").val(resp[8]);
				$("#rango").select2("val", resp[9]);
				$("#sexo").select2("val", resp[10]);
				
				$("#btn-inicio-agregar").val("Editar");
				$('#modal-form-personal').modal({backdrop: 'static', keyboard: false});
				$("#texto").html("Aqui puedes editar la información del personal seleccionado!");
	    		close_loading();
			});
	}

	/*function getPersonalEstatus list*/
	function getPersonalEstatus(idpersonal){
		$.post(base_url()+"personal/getPersonal/"+idpersonal, {}, function(data){
			var resp = data.split("::");
			$("#idpersonal").val(resp[0]);
			$("#btn-inicio-estatus").val("Ok");
			$('#modal-form-estatus-personal').modal({backdrop: 'static', keyboard: false});
		});
	}

	/*function updatePersonalEstatus list*/
	function updatePersonalEstatus(){
		idpersonal = $("#idpersonal").val();
		open_loading();
    	$.post(base_url()+"personal/updatePersonalEstatus/"+idpersonal, {}, function(data){
			$('#modal-form-estatus-personal').modal('hide');
			if(data == "exito"){
			    msj_exito("Estatus Actualizado correctamente", 3000);
			    DataTablePersonal();
			}else if(data == "asignadas"){
				$('#modal-form-estatus-personal').modal('hide');
				msj_error("El Funcionario, que intenta bloquear tiene armamentos asignados", 5000);
			}else{
				msj_error("Error, Intente mas tarde", 3000);
			}
    		close_loading();
		});
	}


	function getPersonalDelete(idpersonal){
		$('#modal-form-eliminar-personal').modal({backdrop: 'static', keyboard: false});
		$('#modal-form-eliminar-personal').show();
		$('#ok').click(function (e) { 
			e.preventDefault();
			open_loading();
	    	$.post(base_url()+"personal/getDelete/"+idpersonal, {}, function(data){
	    		if (data=="exito") {
					msj_exito("El Funcionario fue Eliminado Exitosamente", 3000);
					setTimeout(function () {location.href = $("#base_url").val()+"personal"}, 3000);
				} else if (data=="validar") {
					msj_exito("El Funcionario Seleccionado, tiene armas pendientes por entregar", 3000);
				}else {
					msj_error("Error, Intente mas tarde", 5000);
				} 
				$('#modal-form-eliminar-personal').hide();
				close_loading();
			});
		});
		
	}