$(document).ready(function() {
	/*dataTables list Arma*/
	DataTableArmas();
    $('#dataTables-movimientos-armas').DataTable({
	    responsive: true,
	    "order": [[ 6, "desc" ]],
	    language: {
	        "decimal": "",
	        "emptyTable": "No hay información",
	        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
	        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
	        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
	        "infoPostFix": "",
	        "thousands": ",",
	        "lengthMenu": "Mostrar _MENU_ Registros",
	        "loadingRecords": "Cargando...",
	        "processing": "Procesando...",
	        "search": "Buscar:",
	        "zeroRecords": "Sin resultados encontrados",
	        "paginate": {
	            "first": "Primero",
	            "last": "Ultimo",
	            "next": "Siguiente",
	            "previous": "Anterior"
	        }
	    },
	});

	$("#btn-inicio-agregar-a").val("Agregar");
	$("#btn-inicio-agregar-e").val("Agregar");
	$("#desde").datepicker({changeYear:true, minDate: '0', maxDate: '0'});
	$("#hasta").datepicker({changeYear:true, minDate: '0'});

	$('#h_asignacion').timepicker({
		minuteStep: 1,
		showSeconds: true,
		showMeridian: false,
		disableFocus: true,
		icons: {
			up: 'fa fa-chevron-up',
			down: 'fa fa-chevron-down'
		}
	}).on('focus', function() {
		$('#h_asignacion').timepicker('showWidget');
	});

	$('#h_devolucion').timepicker({
		minuteStep: 1,
		showSeconds: true,
		showMeridian: false,
		disableFocus: true,
		icons: {
			up: 'fa fa-chevron-up',
			down: 'fa fa-chevron-down'
		}
	}).on('focus', function() {
		$('#h_devolucion').timepicker('showWidget');
	});

	var url = $("#base_url").val();

	$("#form-asignar-armamentos").submit(function(){
		var hoy = new Date();
		var  personal = $("#personal").val();
		var  armas = $("#armamentos").val();
		var desde = $("#desde").val().split("-");
		var hasta = $("#hasta").val().split("-");
		var h_a = $("#h_asignacion").val().split(":");
		var h_d = $("#h_devolucion").val().split(":");

		var d = new Date(parseInt(desde[0]), parseInt(desde[1]-1), parseInt(desde[2]), parseInt(h_a[0]), parseInt(h_a[1]), parseInt(h_a[2]), 0);
		var h = new Date(parseInt(hasta[0]), parseInt(hasta[1]-1), parseInt(hasta[2]), parseInt(h_d[0]), parseInt(h_d[1]), parseInt(h_d[2]), 0);
		
		var diff = (h-d)/1000/60/60;

		if (personal=="0") {
			msj_advertencia("Por favor! debe seleccionar un Funcionario de la lista", 3000);
		}else if (armas==null){
			msj_advertencia("Por favor! debe seleccionar al menos un arma de la lista", 3000);
		}/* else if (diff<0 || diff==0 || diff<=23 || diff>24) {
			msj_error("Atencion! debe existir un intervalo de diferencia de al menos 24 horas entre la (fecha/hora) de asignacion y devolucion. Verifique!", 6000);	
		} */else{
			$.post(base_url()+"armas/ConfirmarArmasPersonal",  $(this).serialize(), function(data){
				$('#getInfo').html(data);
				$("#fecha_a").datepicker({changeYear:true, minDate: '0', maxDate: '0'});
				$("#fecha_d").datepicker({changeYear:true, minDate: '0'});
				$('#hora_a').timepicker({
					minuteStep: 1,
					showSeconds: true,
					showMeridian: false,
					disableFocus: true,
					icons: {
						up: 'fa fa-chevron-up',
						down: 'fa fa-chevron-down'
					}
				}).on('focus', function() {
					$('#hora_a').timepicker('showWidget');
				});

				$('#hora_d').timepicker({
					minuteStep: 1,
					showSeconds: true,
					showMeridian: false,
					disableFocus: true,
					icons: {
						up: 'fa fa-chevron-up',
						down: 'fa fa-chevron-down'
					}
				}).on('focus', function() {
					$('#hora_d').timepicker('showWidget');
				});

				$('.cantidad').each(function() {
					$('.numeric').keyup(function (){
						this.value = this.value.replace(/[^0-9]/g,'');
						this.value = this.value.replace(/^0+/, '');
					});
				});

				$('#modal-confimar-armamentos').modal({backdrop: 'static', keyboard: false});
				close_loading();
			});
		}
		
	    return false;
    });

	/*function de add/update armas*/
    $("#form-add-arma").submit(function(){
	    var  idarma = $("#idarma").val();
	    open_loading();
    	if (idarma){
    		$.post(base_url()+"armas/updateArma", $(this).serialize(), function(data){
		    	var resp = data.split("::");
		    	if(resp[0] == "exito"){
		    		msj_exito("Arma Actualizada Correctamente", 5000);
		    		/* clearForm();
					DataTableArmas(); */
				    setTimeout(function () {location.href = $("#base_url").val()+"armas"}, 3000);
		    	}else if(resp[0] == "codigo"){
		    		msj_advertencia("El Código ("+resp[1]+") ingresado ya existe!", 5000);	
		    	}else{
		    		msj_error("Error, Intente mas tarde", 5000);
		    	}
		    });
    	}else{
		    $.post(base_url()+"armas/registerArma", $(this).serialize(), function(data){
		    	var resp = data.split("::");
		    	if(resp[0] == "exito"){
					msj_exito("Arma Registrada correctamente", 5000);
				    setTimeout(function () {location.href = $("#base_url").val()+"armas"}, 3000);
		    		/* clearForm();
		    		DataTableArmas(); */
		    	}else if(resp[0] == "codigo"){
		    		msj_advertencia("El Código ("+resp[1]+") ingresado ya existe!", 5000);	
		    	}else{
		    		msj_error("Error, Intente mas tarde", 5000);
		    	}
		    });
    	}
	    close_loading();
	    return false;
	});
	
	/*function de add/update armas equipos*/
    $("#form-add-arma-equipos").submit(function(){
		var  idarma = $("#idarma_e").val();
	    open_loading();
    	if (idarma){
    		$.post(base_url()+"armas/updateArma", $(this).serialize(), function(data){
		    	var resp = data.split("::");
		    	if(resp[0] == "exito"){
		    		msj_exito("Equipo Policial Actualizado Correctamente", 5000);
				    setTimeout(function () {location.href = $("#base_url").val()+"armas"}, 3000);
					/* clearFormE();
		    		DataTableArmas(); */
		    	}else if(resp[0] == "codigo"){
		    		msj_advertencia("El Código ("+resp[1]+") ingresado ya existe!", 5000);	
		    	}else{
		    		msj_error("Error, Intente mas tarde", 5000);
		    	}
		    });
    	}else{
		    $.post(base_url()+"armas/registerArma", $(this).serialize(), function(data){
		    	var resp = data.split("::");
		    	if(resp[0] == "exito"){
					msj_exito("Equipo Policial Registrado Correctamente", 5000);
				    setTimeout(function () {location.href = $("#base_url").val()+"armas"}, 3000);
		    		/* clearFormE();
		    		DataTableArmas(); */
		    	}else if(resp[0] == "codigo"){
		    		msj_advertencia("El Código ("+resp[1]+") ingresado ya existe!", 5000);	
		    	}else{
		    		msj_error("Error, Intente mas tarde", 5000);
		    	}
		    });
    	}
	    close_loading();
	    return false;
    });

    /*function seleccionar all Checkbox*/
    $("#personal-armas").change(function (){
    	open_loading();
	    	var idpersonal = $("#personal-armas").val();
	    	if (idpersonal == "0") {
	    		msj_advertencia("Por favor, debe seleccionar un funcionario. Verifique!", 3000);
	    	}else {
		    	$("#getInfoArmas").html('<div align="center"><i class="fa fa-spinner"></i> Cargando, por favor espere!...</div>');
		       	$.post(base_url()+"/armas/getInfoArmasPersonal/", {"idpersonal":idpersonal}, function(data){
		       		$("#getInfoArmas").html(data);
		       		$("#personal-armas").select2("val", "0");
		       	});
		       	close_loading();
	    	}
	});

	$("#tipo_e").change(function (){
		var tipo = $("#tipo_e").val();
		
		if(tipo=="MUNICION"){
			$("#calibre_e").attr('disabled', false);
		}else{
			$("#calibre_e").attr('disabled', true);
		}
	});

});

	/*function limpiar form*/
    function clearForm(){
    	$("#btn-inicio-agregar").val("Agregar");
    	$("#idarma").val("");
		$("#codigo").val("");
		$("#tipo").select2("val", "");
		$("#calibre").select2("val", "");
		$("#descripcion").val("");
		$("#cantidad").val("1");
		$('#modal-form-armas').modal('hide');
		setTimeout(function () {location.href = $("#base_url").val()+"armas"}, 1000);		
	}

	function clearFormE(){
    	$("#btn-inicio-agregar").val("Agregar");
    	$("#idarma_e").val("");
		$("#codigo_e").val("");
		$("#tipo_e").select2("val", "");
		$("#calibre_e").select2("val", "");
		$("#descripcion_e").val("");
		$("#cantidad_e").val("");
		$('#modal-form-armas').modal('hide');
		setTimeout(function () {location.href = $("#base_url").val()+"armas"}, 1000);

	}

	
	/*function limpiar form*/
    function finalizarAsignacionArmas(){
    	var hoy = new Date();
       	var cantArmas = {};

       	var fecha_a = $("#fecha_a").val();
		var fecha_d = $("#fecha_d").val();
		var hora_a = $("#hora_a").val();
		var hora_d = $("#hora_d").val();
		var funcionario = $("#funcionario").val();
		var armamentos = $("#armamentos").val();


		var desde = $("#fecha_a").val().split("-");
		var hasta = $("#fecha_d").val().split("-");
		var h_a = $("#hora_a").val().split(":");
		var h_d = $("#hora_d").val().split(":");

		var d = new Date(parseInt(desde[0]), parseInt(desde[1]-1), parseInt(desde[2]), parseInt(h_a[0]), parseInt(h_a[1]), parseInt(h_a[2]), 0);
		var h = new Date(parseInt(hasta[0]), parseInt(hasta[1]-1), parseInt(hasta[2]), parseInt(h_d[0]), parseInt(h_d[1]), parseInt(h_d[2]), 0);
		
		var diff = (h-d)/1000/60/60;

		//alert("fecha_a: "+fecha_a+" hora_a: "+hora_a+" fecha_d: "+fecha_d+" hora_d: "+hora_d+" funcionario: "+funcionario+" armamentos: "+armamentos);

    	$('.cantidad').each(function() {
    		cantArmas[$(this).attr('id')] = $(this).val();
    	});
    	//console.log(cantArmas);
    	/* if (diff<0 || diff==0 || diff<=23 || diff>24) {
			msj_error("Atencion! debe existir un intervalo de diferencia de al menos 24 horas entre la (fecha/hora) de asignacion y devolucion. Verifique!", 6000);	
		} else{*/
	    	$.post(base_url()+"/armas/addArmasPersonal/", {"fecha_a":fecha_a, "hora_a":hora_a, "fecha_d":fecha_d, "hora_d":hora_d, "funcionario":funcionario, "armamentos":armamentos, "cantArmas":cantArmas, "diff":diff}, function(data){
	    		if (data=="exito") {
	    			msj_exito("Armamentos Asignados Correctamente", 3000);
				    $('#modal-confimar-armamentos').modal('hide');
				    setTimeout(function () {location.href = $("#base_url").val()+"armas/asignarArmas"}, 3000);
	    		}else if(data == "cantidad"){
				    msj_advertencia("La cantidad de una de la(s) arma(s), es mayor a la disponible!", 5000);
				}else if(data == "campo"){
				    msj_advertencia("Verifique! la cantidad ingresada esta en cero (0) ó en blanco", 5000);
				}else{
					msj_error("Error, Intente mas tarde", 3000);
				}
	    	});
		/* } */
	}

	function finalizarDevolverArmas(id) {
		open_loading();
			$.post(base_url()+"/armas/getDevolverArmas/", {"id":id}, function(data){
				if (data=="exito") {
					msj_exito("Armamentos Devueltos Correctamente", 3000);
					setTimeout(function () {location.href = $("#base_url").val()+"armas/devolverArmas"}, 3000);
				}else{
					msj_error("Error, Intente mas tarde", 3000);
				}
			});
		close_loading();
	}

	/*function DataTableArmas list*/
	function DataTableArmas(){
		$("#armas").html('<div align="center" style="color: #d20d0d; font-weight: bold;"><i class="fa fa-spinner fa-pulse fa-2x"></i> <b style="font-size: 15px;">Cargando!  por favor espere...</b></div>');
    	$.post(base_url()+"armas/listDataTableArmas", $(this).serialize(), function(data){
			$("#armas").html(data);
			$("#equipo_policiales").show();
			$("#equipo_armas").show();
			$("#btn-inicio-agregar-a").val("Agregar");
			$("#btn-inicio-agregar-e").val("Agregar");
    		$('#dataTables-armas').DataTable({
	    		responsive: true,
	    		language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
			        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Registros",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
    		});
		});
	}


	function getArma(idarma){
		open_loading();
	    	$.post(base_url()+"armas/getArma/"+idarma, {}, function(data){
				var resp = data.split("::"); 
				var tipo = resp[2];
				if(tipo=="PISTOLA" || tipo=="ESCOPETA") {
					$("#idarma").val(resp[0]);
					$("#codigo").val(resp[1]);
					$("#tipo").select2("val", resp[2]);
					$("#calibre").select2("val", resp[3]);
					$("#descripcion").val(resp[4]);
					$("#cantidad").val(resp[5]);

					$("#btn-inicio-agregar-a").val("Editar");
					$('#modal-form-armas').modal({backdrop: 'static', keyboard: false});
					$("#texto").html("Aqui puedes editar la información del amarmaneto seleccionado!");
					$("#equipo_policiales").hide();
				 }else {
					$("#idarma_e").val(resp[0]);
					$("#codigo_e").val(resp[1]);
					$("#tipo_e").select2("val", resp[2]);
					if(tipo=="MUNICION") {$("#calibre_e").attr('disabled', false);}else{$("#calibre_e").attr('disabled', true);}
					$("#calibre_e").select2("val", resp[3]);
					$("#descripcion_e").val(resp[4]);
					$("#cantidad_e").val(resp[5]);

					$("#btn-inicio-agregar-e").val("Editar");
					$('#modal-form-armas').modal({backdrop: 'static', keyboard: false});
					$("#texto").html("Aqui puedes editar la información del amarmaneto seleccionado!");
					$("#equipo_policiales").show();
					$("#equipo_armas").hide();
					$("#equipo_armas").removeClass("active");
					$("#equipo_policiales").addClass("active");
					$("#armamentos").removeClass("active in");
					$("#equipos").addClass("active in");
				}
	    		
	    		close_loading();
			});
	}

	/*function getArmaEstatus list*/
	function getArmaEstatus(idarma){
		$.post(base_url()+"armas/getArma/"+idarma, {}, function(data){
			var resp = data.split("::");
			$("#idarma").val(resp[0]);
			$("#btn-inicio-estatus").val("Ok");
			$('#modal-form-estatus-armas').modal({backdrop: 'static', keyboard: false});
		});
	}

	/*function biometrico*/
	function getPersonalBiometrico(proceso){
		//alert(proceso);
        open_loading();
            $.post(base_url()+"armas/getPersonalHuellas/",{"proceso":proceso}, function(data){ 
				$("#getInfoPersonal").html(data);
				$('#modal-personal-huellas').modal({backdrop: 'static', keyboard: false});
				$('#dataTables-personal-huellas').DataTable({
					responsive: true,
					language: {
						"decimal": "",
						"emptyTable": "No hay información",
						"info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
						"infoEmpty": "Mostrando 0 to 0 of 0 Registros",
						"infoFiltered": "(Filtrado de _MAX_ total Registros)",
						"infoPostFix": "",
						"thousands": ",",
						"lengthMenu": "Mostrar _MENU_ Registros",
						"loadingRecords": "Cargando...",
						"processing": "Procesando...",
						"search": "Buscar:",
						"zeroRecords": "Sin resultados encontrados",
						"paginate": {
							"first": "Primero",
							"last": "Ultimo",
							"next": "Siguiente",
							"previous": "Anterior"
						}
					},
				});
			});
        close_loading();
	}
	
	/*function DataTablePersonal list*/
	function getHuella(idpersonal, movimiento){
		open_loading();
			$.post(base_url()+"huellas/getHuellasPersonalArmamentos/"+idpersonal, {}, function(data){
				if (data=="error") {
					msj_error("Error, Intente mas tarde", 5000);
				} else if(data == "0"){
					msj_error("Error, Intente mas tarde", 5000);	
				}else{ 
					setTimeout('procesoBiometricoTemp("'+movimiento+'")',20000);
				}
				close_loading();
			});
			$('#modal-personal-huellas').hide();
	}

	function procesoBiometricoTempEliminar(){
		$.post(base_url()+"huellas/procesoBiometricoTempEliminar/", {}, function(data){
		});
	}

	function getProcesoDevolver(idpersonal){
		
		$.post(base_url()+"/armas/getInfoArmasPersonal/", {"idpersonal":idpersonal}, function(data){
			$("#getInfoArmas").html(data);
			finalizarDevolverArmas(idpersonal);
		});
	}

	function procesoBiometricoTemp(movimiento){
		$.post(base_url()+"huellas/getPersonalHuellasCapturarArmamentos/", {}, function(data){
			var resp = data.split("::");
			if (resp[0]=="exito") {
				msj_exito("Por favor, continue con el proceso", 3000);
				open_loading();
				if(movimiento=="asignar"){
					$("#personal").select2("val", resp[1]);
				}else if (movimiento=="devolver"){
					$("#personal-armas").select2("val", resp[1]);
					getProcesoDevolver(resp[1]);
					//finalizarDevolverArmas(resp[1]);
					$("#btn-devolver").hide();
				}
				close_loading();
				procesoBiometricoTempEliminar();
			}
		});
		tiempo = setTimeout('procesoBiometricoTemp("'+movimiento+'")',20000);
	}