$(document).ready(function() {

	/*function de reportes de usuarios en el sistemass*/
	$("#hasta_m").datepicker();
	$("#hasta_ma").datepicker();
	$("#hasta_a").datepicker();
	$("#hasta_u").datepicker();
	$("#hasta_p").datepicker();

	$("#desde_m").datepicker();
	$("#desde_ma").datepicker();
	$("#desde_a").datepicker();
	$("#desde_u").datepicker();
	$("#desde_p").datepicker();

	$("#desde_e").datepicker();
	$("#desde_e").datepicker();

	var url = $("#base_url").val();
    $("#form-usuarios-reportes").submit(function(){
    	open_loading();
    	$("#reportes-data").html('<div align="center" style="color: #d20d0d; font-weight: bold;"><i class="fa fa-spinner fa-pulse fa-2x"></i> <b style="font-size: 15px;">Cargando!  por favor espere...</b></div>');
    	$.post(base_url()+"reportes/usuarios", $(this).serialize(), function(data){
			$("#reportes-data").html(data);
			$('#dataTables-reportes-usuarios').DataTable({
	    		responsive: true,
	    		language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
			        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Registros",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
    		});
		});
	    close_loading();
	    return false;
    });

    $("#form-personal-reportes").submit(function(){
    	open_loading();
    	$("#reportes-data").html('<div align="center" style="color: #d20d0d; font-weight: bold;"><i class="fa fa-spinner fa-pulse fa-2x"></i> <b style="font-size: 15px;">Cargando!  por favor espere...</b></div>');
    	$.post(base_url()+"reportes/personal", $(this).serialize(), function(data){
			$("#reportes-data").html(data);
			$('#dataTables-reportes-personal').DataTable({
	    		responsive: true,
	    		language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
			        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Registros",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
    		});
		});
	    close_loading();
	    return false;
    });

    $("#form-armas-reportes").submit(function(){
    	open_loading();
    	$("#reportes-data").html('<div align="center" style="color: #d20d0d; font-weight: bold;"><i class="fa fa-spinner fa-pulse fa-2x"></i> <b style="font-size: 15px;">Cargando!  por favor espere...</b></div>');
    	$.post(base_url()+"reportes/armamentos", $(this).serialize(), function(data){
			$("#reportes-data").html(data);
			$('#dataTables-reportes-armas').DataTable({
	    		responsive: true,
	    		language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
			        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Registros",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
    		});
		});
	    close_loading();
	    return false;
    });

    $("#form-movimientos-reportes").submit(function(){
    	open_loading();
    	$("#reportes-data").html('<div align="center" style="color: #d20d0d; font-weight: bold;"><i class="fa fa-spinner fa-pulse fa-2x"></i> <b style="font-size: 15px;">Cargando!  por favor espere...</b></div>');
    	$.post(base_url()+"reportes/movimientos", $(this).serialize(), function(data){
			$("#reportes-data").html(data);
			$('#dataTables-reportes-movimientos').DataTable({
	    		responsive: true,
	    		language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
			        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Registros",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
    		});
		});
	    close_loading();
	    return false;
	});
	
	$("#form-movimientos-armas-reportes").submit(function(){
		var arma = $("#idarmas").val();
		open_loading();
		/* if(arma=="0"){
			msj_advertencia("Por favor! debe seleccionar un Arma de la lista", 3000);
		}else{ */
			$("#reportes-data").html('<div align="center" style="color: #d20d0d; font-weight: bold;"><i class="fa fa-spinner fa-pulse fa-2x"></i> <b style="font-size: 15px;">Cargando!  por favor espere...</b></div>');
			$.post(base_url()+"reportes/movimientosArmas", $(this).serialize(), function(data){
				$("#reportes-data").html(data);
				$('#dataTables-reportes-armas-movimientos').DataTable({
					responsive: true,
					language: {
						"decimal": "",
						"emptyTable": "No hay información",
						"info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
						"infoEmpty": "Mostrando 0 to 0 of 0 Registros",
						"infoFiltered": "(Filtrado de _MAX_ total Registros)",
						"infoPostFix": "",
						"thousands": ",",
						"lengthMenu": "Mostrar _MENU_ Registros",
						"loadingRecords": "Cargando...",
						"processing": "Procesando...",
						"search": "Buscar:",
						"zeroRecords": "Sin resultados encontrados",
						"paginate": {
							"first": "Primero",
							"last": "Ultimo",
							"next": "Siguiente",
							"previous": "Anterior"
						}
					},
				});
			});
		/* } */
	    close_loading();
	    return false;
    });
});

	/*function limpiar form*/
    function clearForm(){
    	$("input.form-control").val("");
		$("select.form-control").select2("val", "todos");
	}