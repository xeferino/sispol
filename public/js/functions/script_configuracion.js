$(document).ready(function() {
	/*function de add/update config*/
    $("#form-configuracion").submit(function(){
	    open_loading();
		    $.post(base_url()+"configuracion/insertConfiguracion", $(this).serialize(), function(data){
		    	if(data == "exito"){
		    		msj_exito("Configuración agregada exitosamente", 3000);
				    setTimeout(function () {location.href = $("#base_url").val()+"configuracion"}, 3000);
		    	}else{
		    		msj_error("Error, Intente mas tarde", 5000);
		    	}
		    });
    	close_loading();
	    return false;
    });
});