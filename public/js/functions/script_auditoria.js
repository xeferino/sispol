$(document).ready(function() {
	DataTableAuditoria();
});

	/*function DataTableAuditoria list*/
	function DataTableAuditoria(){
		$("#auditoria").html('<div align="center" style="color: #d20d0d; font-weight: bold;"><i class="fa fa-spinner fa-pulse fa-2x"></i> <b style="font-size: 15px;">Cargando!  por favor espere...</b></div>');
    	$.post(base_url()+"auditoria/getAuditoria", $(this).serialize(), function(data){
    		$("#auditoria").html(data);
    		$('#dataTables-auditoria').DataTable({
	    		responsive: true,
	    		language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
			        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Registros",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
    		});
		});
	}

	/*function DataTableUser list*/
	function getUser(idusuario){
		open_loading();
	    	$.post(base_url()+"usuario/getUser/"+idusuario, {}, function(data){
	    		var resp = data.split("::");
	    		
				$("#usuario").html(
									'<div class="row"'+
										'<div class="col-sm-12">'+
	    									'<div class="hero-widget well well-sm">'+
										        '<div class="icon">'+
										           '<i class="glyphicon glyphicon-user"></i>'+
										        '</div>'+
										        '<div class="">'+
										            '<label class="text-muted">'+resp[11]+'-'+resp[3]+'</label><br>'+
										            '<label class="text-muted">'+resp[1]+' '+resp[2]+'</label><br>'+
										            '<label class="text-muted">'+resp[7]+'</label><br>'+
										        '</div>'+
	        
										   '</div>'+
										'</div>'+
									'</div>'
								);
				$('#modal-form-auditoria').modal('show');
			});
			close_loading();
	}
