$(document).ready(function() {
	/*dataTables*/
	$('#dataTables-dashboardSalida').DataTable({
    	responsive: true,
    	language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
			        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Registros",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
    });
    $('#dataTables-dashboardEntrada').DataTable({
    	responsive: true,
    	language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
			        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Registros",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
    });

     $('#dataTables-movimientos').DataTable({
    	responsive: true,
    	"order": [[ 6, "desc" ]],
    	language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
			        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Registros",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
    });
	var url = $("#base_url").val();    
});

/*function DataTableUser list*/
	function getInfo(opc) {
		open_loading();
			if (opc=="armamentos") {
				$.post(base_url()+"dashboard/armamentos", function(data){
					$('#getInfo').html(data);
					$('#modal-info-armamentos').modal('show');
					$('#dataTables-armas').DataTable({
						responsive: true,
						language: {
					        "decimal": "",
					        "emptyTable": "No hay información",
					        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
					        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
					        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
					        "infoPostFix": "",
					        "thousands": ",",
					        "lengthMenu": "Mostrar _MENU_ Registros",
					        "loadingRecords": "Cargando...",
					        "processing": "Procesando...",
					        "search": "Buscar:",
					        "zeroRecords": "Sin resultados encontrados",
					        "paginate": {
					            "first": "Primero",
					            "last": "Ultimo",
					            "next": "Siguiente",
					            "previous": "Anterior"
					        }
					    },
					});
					close_loading();
				});
			}else if  (opc=="personal"){
				$.post(base_url()+"dashboard/personal", function(data){
					$('#getInfo').html(data);
					$('#modal-info-personal').modal('show');
					$('#dataTables-personal').DataTable({
						responsive: true,
						language: {
					        "decimal": "",
					        "emptyTable": "No hay información",
					        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
					        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
					        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
					        "infoPostFix": "",
					        "thousands": ",",
					        "lengthMenu": "Mostrar _MENU_ Registros",
					        "loadingRecords": "Cargando...",
					        "processing": "Procesando...",
					        "search": "Buscar:",
					        "zeroRecords": "Sin resultados encontrados",
					        "paginate": {
					            "first": "Primero",
					            "last": "Ultimo",
					            "next": "Siguiente",
					            "previous": "Anterior"
					        }
					    },
					});
					close_loading();
				});
			}else if  (opc=="usuarios"){
				$.post(base_url()+"dashboard/usuarios", function(data){
					$('#getInfo').html(data);
					$('#modal-info-usuarios').modal('show');
					$('#dataTables-usuarios').DataTable({
						responsive: true,
						language: {
					        "decimal": "",
					        "emptyTable": "No hay información",
					        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
					        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
					        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
					        "infoPostFix": "",
					        "thousands": ",",
					        "lengthMenu": "Mostrar _MENU_ Registros",
					        "loadingRecords": "Cargando...",
					        "processing": "Procesando...",
					        "search": "Buscar:",
					        "zeroRecords": "Sin resultados encontrados",
					        "paginate": {
					            "first": "Primero",
					            "last": "Ultimo",
					            "next": "Siguiente",
					            "previous": "Anterior"
					        }
					    },
					});
					close_loading();
				});
			}else{
				$.post(base_url()+"dashboard/movimientos", function(data){
					$('#getInfo').html(data);
					$('#modal-info-movimientos').modal('show');
					$('#dataTables-movimientos').DataTable({
						responsive: true,
						"order": [[ 6, "desc" ]],
						language: {
					        "decimal": "",
					        "emptyTable": "No hay información",
					        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
					        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
					        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
					        "infoPostFix": "",
					        "thousands": ",",
					        "lengthMenu": "Mostrar _MENU_ Registros",
					        "loadingRecords": "Cargando...",
					        "processing": "Procesando...",
					        "search": "Buscar:",
					        "zeroRecords": "Sin resultados encontrados",
					        "paginate": {
					            "first": "Primero",
					            "last": "Ultimo",
					            "next": "Siguiente",
					            "previous": "Anterior"
					        }
					    },
					});
					close_loading();
				});
			}
	}
