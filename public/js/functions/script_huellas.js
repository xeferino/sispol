$(document).ready(function() {
	//procesoBiometricoTemp();
	$('#dataTables-personal').DataTable({
	   responsive: true,
	   language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
			        "infoFiltered": "(Filtrado de _MAX_ total Registros)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Registros",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
    });
});

	/*function DataTablePersonal list*/
	function getPersonal(idpersonal){
		open_loading();
	    	$.post(base_url()+"huellas/getPersonalHuellas/"+idpersonal, {}, function(data){
	    		if (data=="error") {
					msj_error("Error, Intente mas tarde", 5000);
				} else if(data == "0"){
		    		msj_error("Error, Intente mas tarde", 5000);	
		    	}else{
					procesoBiometricoTemp();
				}
				close_loading();
			});
	}

	function getPersonalDeleteHuella(idpersonal){
		$('#modal-form-eliminar-huella').modal({backdrop: 'static', keyboard: false});
		$('#ok').click(function (e) { 
			e.preventDefault();
			open_loading();
	    	$.post(base_url()+"huellas/getDeleteHuella/"+idpersonal, {}, function(data){
	    		if (data=="error") {
					msj_error("Error, Intente mas tarde", 5000);
				} else{
					msj_exito("Huella Eliminda Exitosamente", 3000);
					setTimeout(function () {location.href = $("#base_url").val()+"huellas"}, 3000);
				}
				$('#modal-form-eliminar-huella').hide();
				close_loading();
			});
			
		});
		
	}

	function procesoBiometricoTemp(){
		$.post(base_url()+"huellas/getPersonalHuellasCapturar/", {}, function(data){
			if (data=="error") {
				msj_error("Error, Intente mas tarde", 2000);
			} else if(data == "0"){
				msj_error("Error, Intente mas tarde", 2000);	
			}else if(data == "exito"){
				msj_exito("Huella Agregada Exitosamente", 2000);
				setTimeout(function () {location.href = $("#base_url").val()+"huellas"}, 2000);	
			}
		});
		tiempo = setTimeout('procesoBiometricoTemp()',10000);
	};